---
title: "HIFIS im EGI Newsletter"
title_image: default
date: 2020-06-08
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
lang: de
lang_ref: 2020-06-19-hifis-in-the-egi-newsletter
---

_Helmholtz Federated IT Services - Promoting IT based science at all levels_,
so lautet der Titel unseres Artikels, der im Newsletter unserer Freunde von der
EGI-Foundation erschienen ist.
In dem EGI-Newsletter geben wir einen Überblick über die neue HIFIS-Plattform.
<!--more-->

<a type="button" class="btn btn-outline-primary btn-lg" href="https://preview.mailerlite.com/s9b7l1/1435456442688607486/y2j8/">
  <i class="fas fa-external-link-alt"></i> Weiterlesen
</a> {% comment %} The link is only to the newsletter, the more detailed article has gone offline, as well as all the articles from older issues. Reported bug to EGI.eu (Sophie). {% endcomment %}
