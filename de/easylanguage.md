---
title: HIFIS in leichter Sprache  
title_image: default
layout: default
excerpt:
    Informationen in leichter Sprache über HIFIS
lang_ref: easylanguage
---

Willkommen auf der Website von HIFIS.  
Hier gibt es Informationen in leichter Sprache.

## Was ist HIFIS?

{:.treat-as-figure}
{:.float-right}
![Werkzeuge]({% link assets/img/hifisfor/poster-1.svg %})
Lorna Schütte, CC-BY-NC-SA

HIFIS ist eine digitale Platfform für Wissenschaftlerinnen und Wissenschaftler.  
Wir helfen ihnen, indem wir ihnen Zugang zu Computern und Werkzeugen geben.  
Das sind zum Beispiel:
- Programme, 
- Orte, wo man Datein speichern kann,
- und andere Sachen,die man für die Forschung braucht.  

HIFIS biete auch Beratung und Hilfe an.  
Wir helfen besonders dabei, gute Programme für die Forschung zu machen.

## Die Helmholtz-Cloud

HIFIS unterstützt auch die Helmholtz-Cloud.  
Das ist ein Ort im Internet, wo man viele Dienste findet.  
Früher waren diese Dienste getrennt.  
Jetzt sind sie alle in der Helmholtz-Cloud zusammen.

Es gibt dort mehr als 30 Dienste für:
- Wissenschaft, 
- Zusammenarbeit, 
- Technologie, 
- Rechnen und ganz schnellen Computern, 
- und Speicherung von Daten. 

Die Nutzer können diese Dienste benutzen, wenn sie sich mit ihrem Helmholtz-Konto anmelden.

{:.treat-as-figure}
{:.float-right}
![Datenhoheit]({% link assets/img/hifisfor/poster-2b.svg %})
Lorna Schütte, CC-BY-NC-SA

## Daten sicher und verfügbar halten

HIFIS sorgt dafür, dass sensible Daten sicher sind.  
Wir speichern und arbeiten mit den Daten in eigenen Rechenzentren.  
So bleiben die Daten in Deutschland und auch in Zukunft verfügbar.  

## Unterstützung der Entwicklung von Forschungssoftware

HIFIS hilft auch bei der Entwicklung von guten Programmen für die Forschung.  
Wir bieten Schulungen für alle an, die mehr über Programmieren lernen wollen.  
Es gibt Kurse für Anfänger und Fortgeschrittene.  
Außerdem bieten wir kostenlose Beratung für Forschende an, die Hilfe bei Software-Projekten brauchen.
