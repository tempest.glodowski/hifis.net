Dear HIFIS community and friends,

As the festive season approaches, we would like to wish you and your loved ones a peaceful end to 2023 and a happy start to the new year. We are looking forward to a temporary decrease in the number of logins during this joyful season, just this once.

We are proud to look back on a year in which we achieved many successes together.
The Helmholtz Assembly of Members decided to continue funding the HIFIS platform.
Helmholtz ID is becoming an indispensable part of the IT infrastructure at Helmholtz and beyond.
The Helmholtz Cloud is growing and the foundations for deeper integration have been laid, while important steps were taken for a better resilience.
Our service providers have implemented new services and are ensuring stable operations as usage numbers increase.
Training, consulting and community activities are creating impact, as highlighted by the Research Software Directory and the Helmholtz Incubator Software Award.
And these are just a few of the highlights of 2023.
We would like to thank all those involved for their hard work and valuable contribution.

Let us carry the team's enthusiasm from the recent all-hands meeting we had in Dresden - where the vegan schnitzel was only for the bravest - into 2024. Picture a future where different services are embedded in a seamless workflow, where the trust in the federation allows centres to rely on HIFIS services, reaching a perfect balance between redundancy and efficiency at the Helmholtz level, and where researcher's first instinct when looking for a new digital service is to check out what HIFIS has to offer. 

We are looking forward to 2024 to work towards this future together and continue impressing people with what we, a bunch of very motivated nerds, can offer.

The HIFIS Team