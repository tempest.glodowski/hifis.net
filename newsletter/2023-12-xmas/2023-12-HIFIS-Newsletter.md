---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #00285a}
</style>
<title>HIFIS News December 2023</title>

{% include_relative html_bits/page_intro.htm %}

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
