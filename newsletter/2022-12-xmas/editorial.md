Dear HIFIS community and friends, 

As the year comes to an end, we want to use this opportunity to take a look back and say thank you!
It has been a challenging year in many ways, but we have overcome a lot of obstacles by focusing on our strengths and joining forces wherever possible.
In terms of IT services for science, we showed how this can be done with our platform HIFIS, to which the centres of the Helmholtz Association contribute their best solutions and competencies.

Let's remember some highlights of 2022 but keep in mind that this list is by far not complete:
While we had about 6,000 users authorized in our Helmholtz Login at the end of 2021, now more than 13,000 users from [all Helmholtz centres and beyond]({% post_url 2022/2022-08-26-aai %}) can log in with their home institution's account to all our services.
Once logged in, they can benefit from our support offerings and [Helmholtz Cloud](https://helmholtz.cloud/) services.
The latter have been increased from 20 to 25 software services during the last 12 months.
Our website got a [makeover]({% post_url 2022/2022-04-06-hifis-net-relaunch %}) as well as the [Helmholtz Cloud]({% post_url 2022/2022-08-01-update-cloudportal %}).
We expanded our collections of [use cases]({% link usecases.md %}) and [software spotlights]({% link spotlights.md %}) from the HIFIS community.
Plony, the service database, became the [central interface for service providers]({% post_url 2022/2022-11-04-cloud-plony-central-interface %}) to automate and facilitate processes.
HIFIS Education did not only teach a lot of people but also started publishing their [workshop materials]({% link services/software/training.md %}).
HIFIS Consulting did likewise with the publication of the [Consulting Handbook]({% post_url 2022/2022-10-28-consulting-handbook %}) and an [Awesome list of useful links]({% post_url 2022/2022-09-19-awesome-lists %}) for software development.
And last but not least, we created the HIFIS newsletter to keep you informed about the world of HIFIS.

The [very positive evaluation]({% post_url 2022/2022-12-12-evaluation-update %}) in the fall also showed that we have taken the right path together.
HIFIS is a visible landmark of sustainable, federated services in science.

We would like to thank all our users, partners and friends for a great cooperation over the last year. 
We are looking forward to set new milestones and master new challenges together.
But for now, we wish you all happy holidays and all the best to you in the year to come!

The HIFIS Team
