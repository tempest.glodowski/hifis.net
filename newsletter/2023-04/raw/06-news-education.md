**Upcoming courses**

HIFIS Education will offer courses on the topics of

* [Introduction to Git and GitLab (at KIT)](https://events.hifis.net/event/796/), 08./09.05.2023
* [Foundations of Research Software Publication (at UFZ)](https://events.hifis.net/event/794/), 01./02.06.2023

Those courses will take place at the respective Helmholtz centre and seats are primarily provided for the hosting centre.
However, if there are seats left, the registration might open for other Helmholtz centres.

Please check our [HIFIS Events page](https://events.hifis.net/category/4) for a full list of workshops, workshop details and registration periods.
