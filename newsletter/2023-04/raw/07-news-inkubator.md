**Research Software Bootcamp**

Helmholtz Federated IT Services (HIFIS) and Helmholtz Information & Data Science Academy (HIDA) jointly offer a Research Software Bootcamp in three parts. 
[RSE Bootcamp Part 1: Python](https://events.hifis.net/event/802/) will take place 3.5.-5.5.2023, but is nearly fully booked. 
However, you may check for any last minute open seats.

**Part 2 and 3** will take place in **June and July**, so watch out for them!