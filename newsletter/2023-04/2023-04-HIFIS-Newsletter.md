---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #005aa0}
</style>
<title>New services available at the Helmholtz Cloud</title>

{% include_relative html_bits/page_intro.htm %}

<a style="text-decoration: none; " href="#usecases">Community Use Cases</a><br>
<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#incubator">News from Helmholtz Incubator</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/section_intro.htm %}

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="usecases"><span style="color:#005aa0"><strong>Community Use Cases</strong></span></span><br>

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="orchestration_components_NL.png" alt="5 Cloud symbolizing the interacting components in orchestrated cloud based workflow" border="0" align="right" />

<div markdown="1">
{% include_relative raw/03-uc_service_pipeline.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<div markdown="1">{% include_relative raw/uc-we-still-want.md %}</div>

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="hifisnews"><span style="color:#005aa0"><strong>News from HIFIS</strong></span></span><br>

<img style="margin: 20px 20px 20px 20px;max-width: 90%;width: 486px;border: 1px solid #005aa0" src="portal-new-services.png" alt="Screenshot of new Cloud Portal with 5 new services" align="right" />
<div markdown="1">
{% include_relative raw/04-news-cloud.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 50px 20px 20px;max-width: 40%;width: 216px;" src="RSD.png" alt="Screenshot of Research Software Directory" border="0" align="left" />

<div markdown="1">
{% include_relative raw/05-news-software.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="everybody_can_code.jpg" alt="Garland with inscription Everybody can code" border="0" align="right" />

<div markdown="1">
{% include_relative raw/06-news-education.md %}
</div>

{% include_relative html_bits/section_separator.htm %}

<div style="line-height:24px " id="incubator"><span style="color:#005aa0"><span style="font-size:16px"><strong>News from the Helmholtz Incubator</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 20%;width: 108px;" src="incubator_logo.png" alt="Logo of Helmholtz Incubator" border="0" align="left" />

<div markdown="1">
{% include_relative raw/07-news-inkubator.md %}
</div>

{% include_relative html_bits/section_separator.htm %}

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
