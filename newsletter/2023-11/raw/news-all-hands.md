**HIFIS all-hands meeting: charting the future**

The recent all-hands meeting at HZDR united HIFIS members and collaborators, sparking discussions on our future direction. Intense ideation sessions have set the course for the reshaping of HIFIS resources for the next years and ensure a smooth transition into a more mature HIFIS.

[Read more...]({% post_url 2023/2023-10-19-all-hands-meeting %})