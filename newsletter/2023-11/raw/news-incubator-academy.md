**Incubator Academy**

The Incubator Summer Academy took place in September. This joint event organised by Helmholtz AI, Helmholtz Imaging, HIDA, HMC and HIFIS was held for the second time. The two-week programme offered various course packages in Data Science methods and skills, attracting 225 participants.

[Read more...]({% post_url 2023/2023-11-09-isa-summary %})
