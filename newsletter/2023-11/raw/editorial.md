Dear HIFIS Community and Friends,

Attentive readers might have recognized at first sight:
we continued to update the look and feel of our services and pages &mdash; a part of this being the new look of our logos in line with the updated Helmholtz visual identity, as you can already see in this newsletter.

On the [Helmholtz Cloud](https://helmholtz.cloud/) portfolio: [InfiniteSpace](https://helmholtz.cloud/services/?filterSearchInput=dcache&serviceID=9b6c63a4-d26b-4ea6-b8b0-88c0be5ea610), our Storage Service powered by dCache is gaining momentum among various communities. A prototype implementation is available for the [NEST computational neuroscience simulation tool](https://nest-desktop.readthedocs.io/en/dev/about/index.html), its use for federated data sharing among the [Pelagic Imaging Consortium](https://modalities.helmholtz-imaging.de/lab/94) is currently being investigated, as well as for open data storage. Stay tuned for upcoming use cases.

In late September, the Helmholtz Assembly of members decided to [continue the funding](https://www.hifis.net/news/2023/10/13/HIFIS-continued-funding.html) for HIFIS and its four sister Incubator Platforms until at least 2029! We are now tasked to further elaborate on the platform's concepts for the upcoming years.

As mentioned in our previous newsletter, HIFIS got ahead to set up first specific procedures and resources to enhance the [resilience of HIFIS core services](https://www.hifis.net/news/2023/11/02/resilience-core-services.html) in case of sudden unavailability of a centre's IT infrastructure due to cyberattacks, but also other potential mishaps like power outages.
When necessary, HIFIS can rapidly activate a duplicate of its core service at an alternative location,
minimizing downtime and ensuring continuous operation for users.

Enjoy reading and using our HIFIS services! Remember to spread the word and [subscribe our newsletter](https://hifis.net/subscribe)!

The HIFIS Team
