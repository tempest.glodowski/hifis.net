**Helmholtz Incubator Software Award: helping you wait until the results are in!**

Recently featured in a Code for Thought podcast episode, the unprecedented Helmholtz Software Award reveals its goals and significance to the research community to auditors. 

As you eagerly await the results of the award, for which we received and accepted more than 40 applications, listening to this podcast will convince you - once more - of the importance of high-quality research software. 

[Read more...]({% post_url 2023/2023-08-14-podcast-award %})