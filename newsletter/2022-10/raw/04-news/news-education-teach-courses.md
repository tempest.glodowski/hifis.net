**TEACH 2 - Open Up!**
On the 9. November 2022 the second installment of the TEACH conference will take place.
_TEACH_ stands for _Talk about Education Across Communities in Helmholtz_ and aims to bring together a wide range of educators, education coordinators, HR developers from all across the Helmholtz Association.
Participants will be presented with a wide range of talks, workshops, posters and discussions revolving around education offers, practices and networking opportunities.
The participation is open to everyone, free of charge. The [agenda and further details can be found here](https://events.hifis.net/e/teach2).

**Upcoming courses:**
HIFIS will offer courses on the topics of

* Foundations of Research Software Publication
* First Steps in Python
* Introduction to Indico
* Using Containers in Science

The registrations are usually open two weeks before the start of the workshop.
Please regularly check our [HIFIS Events page](https://events.hifis.net/category/4) for a full list of workshops, workshop details and registration periods.
