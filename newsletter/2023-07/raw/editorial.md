Dear HIFIS Community and Friends,

As an increasing challenge, operational cyber security is coming into focus for HIFIS. You will have noticed that Helmholtz centres have been affected by cyber attacks. Together with the Working Group of the IT Heads (KoDa), HIFIS is increasingly coordinating Helmholtz centres' response to such threats to increase cyber resilience and ensure overall digital sovereignty. More to come during the next months!

For now, we want to present a selection of our latest activites in this newsletter:
Helmholtz Cloud has got a facelift! Visit our Cloud Portal to enjoy the **new look & feel**. Also, we expanded the service portfolio further, as now also **large-scale storage** is available for research and service pipelines.

The Helmholtz Software Directory is growing fast: There are already over a hundred great software solutions described and linked to their respective development repositories. And there is one new reason to make your research software known in this place: A registration is a prerequisite for participation in the new **Helmholtz Incubator Software Award**.

You want to know more on the Helmholtz-wide training possibilites for researchers on software engineering, provided by HIFIS and our partners? So, tune in to our **podcast** on this, and furthermore don't miss to check out the 2023's version of the **Helmholtz Incubator Academy** and **TEACH Conference**!

Enjoy reading and using our HIFIS services... and of course: Spread the word and [subscribe our newsletter](https://hifis.net/subscribe) ;)

The HIFIS Team
