**Tune in on "Code for Thought"**

Code for Thought is a community podcast for Research, (Open) Science and Engineering. In the latest German episode “Bringt uns Programmieren bei” (teach us programming), the host Peter Schmidt interviews Fredo Erxleben from HIFIS. He talks about his experiences as workshop leader, teaching coding, version control and other important topics to scientists.

[Read more...]({% post_url 2023/2023-06-29-podcast-education %})
