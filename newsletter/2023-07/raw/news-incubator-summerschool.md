**Incubator Summer Academy > Next Level Data Science**

Save the Date: Our Incubator Summer Academy “Next Level Data Science” will take place on September 18–29, 2023!
The Incubator Summer Academy is open to all doctoral and postdoctoral researchers in the Helmholtz Association.
Registration will open mid of August – stay tuned, we will keep you updated on hifis.net.

[Read more...]({% post_url 2023/2023-06-20-summer-academy %})
