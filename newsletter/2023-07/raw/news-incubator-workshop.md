**Incubator Workshop**

With the 13th Incubator Workshop, all platforms (HIFIS, Helmholtz AI, Helmholtz Imaging, HIDA, HMC) continued to exploit their high creative potential to implement far-reaching benefits for the scientific communities. All experts emphasized the enormous value that the Incubator platforms have for the Helmholtz Association. We will continue to explore how the platforms can complement each other even better!

[Read more...]({% post_url 2023/2023-07-11-incubator-workshop-13 %})
