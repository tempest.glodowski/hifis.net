**REMS unveiled: a Helmholtz cloud service to manage access rights**

While some services in the [Helmholtz Cloud](https://helmholtz.cloud/services/) are widely known, such as GitLab or Jupyter, others remain hidden treasures waiting to be discovered. Meet [__REMS__](https://helmholtz.cloud/services/?serviceID=f0ba3101-1480-422e-bdd3-140ceacc4f01) &mdash; a very useful tool for handling access rights to all kinds of resources.
Looking for a way to streamline the many access requests you receive for your data or samples? Take a closer look, it might be just what you're looking for. 

[Read more...]({% post_url 2024/2024-02-22-REMS-Cloud-Service %})