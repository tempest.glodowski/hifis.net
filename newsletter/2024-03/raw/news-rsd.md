**Another round of fresh features for the RSD**

The [Helmholtz Research Software Directory](https://helmholtz.software/) keeps evolving, thanks to new features offered by the original project but also to match the needs of our HIFIS community. Among the latest enhancements, the directory now __automatically updates mention counts__ whenever a reference paper receives a citation! The __software spotlights__ which were previously showcased in hifis.net are also now prominently featured there.

[Read more...]({% post_url 2024/2024-02-05-rsd-update %})