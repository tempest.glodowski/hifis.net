**HIDA Lecture: Hate speech & AI**

Can AI strike the balance between censorship and innovation in tackling HateSpeech? Join Bert Heinrichs from the University of Bonn and the Forschungszentrum Jülich as he delves into the complexities of this topic.

[Register now for the lecture on 18th April at 11 am!](https://www.helmholtz-hida.de/en/events/hida-lecture-series-on-ai-and-large-language-models-1/)