**Service orchestration: hifis.net sets the tune**

A year ago in this newsletter we mentioned how we felt the growing number of Helmholtz Cloud services had to be followed by a simpler orchestration of these services to enable scientific workflows. We wanted to make this task more visible in hifis.net, so it is now included as a specific offer in the [Cloud services portfolio]({% link services/cloud-overview.md %}).

[Read more...]({% link services/cloud/orchestration.md %})
