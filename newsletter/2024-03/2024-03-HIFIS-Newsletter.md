---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #00285a}
</style>
<title>HIFIS News March 2024</title>

{% include_relative html_bits/page_intro.htm %}

<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#incubator">News from Helmholtz Incubator</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/section_intro.htm %}

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="hifisnews"><span style="color:#00285a"><strong>News from HIFIS</strong></span></span><br>

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="{% link assets/img/illustrations/undraw_mobile_encryption_re_yw3o.svg %}" alt="Illustration of encryption" border="0" align="right" />
<div markdown="1">
{% include_relative raw/service-rems.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="HIDA-course-catalog-screenshot.png" alt="HIDA course catalog homepage" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-hifis-courses-2024.md %}
</div>


{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="{% link assets/img/illustrations/undraw_exploring_re_grb8.svg %}"  alt="Road ahead" border="0" align="right" />

<div markdown="1">
{% include_relative raw/news-hzb-cyber.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="{% link assets/img/services/service_pipelines_aas.svg %}" alt="Minimal workflow pipeline" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-orchestration-aas.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="nest-logo.png" alt="NEST logo" border="0" align="right" />
<div markdown="1">
{% include_relative raw/uc-nest.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="{% link assets/img/illustrations/undraw_proud_coder_re_exuy.svg %}" alt="Proud coder illustration" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-rsd.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="{% link submodules/templates/logos/non_HIFIS/HMC_Logo.svg %}" alt="HMC logo" border="0" align="right" />
<div markdown="1">
{% include_relative raw/news-hifis-at-hmc.md %}
</div>


{% include_relative html_bits/section_separator.htm %}

<div style="line-height:24px " id="incubator"><span style="color:#00285a"><span style="font-size:16px"><strong>News from the Helmholtz Incubator</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="HIDA-Lecture-Series-on-AI.png" alt="HIDA lecture series on AI" border="0" align="left" />

<div markdown="1">
{% include_relative raw/incu-HIDA-lecture.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="HIConf2024_announcement.jpg" alt="Helmholtz Imaging 2024 announcement" border="0" align="right" />

<div markdown="1">
{% include_relative raw/incu-HI-conference.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="HI_Website_News_MetricsPaper_Teaser-Image.png" alt="Helmholtz Imaging metrics papers teaser" border="0" align="left" />

<div markdown="1">
{% include_relative raw/incu-HI-publications.md %}
</div>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
