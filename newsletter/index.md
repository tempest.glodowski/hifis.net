---
title: Newsletter
title_image: sign-pen-business-document-48148.jpg
layout: default
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
additional_js: frontpage.js
excerpt:
    HIFIS Newsletter
---

The HIFIS Newsletter will be published quarterly from July 2022 onwards.
Find all published newsletters below in HTML and PDF formats.

Feel free to distribute the links to everyone who might be interested.

## Published Newsletter Issues:

All previous newsletter issues can be found here.

- March 2024: [HTML]({% link newsletter/2024-03/2024-03-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2024-03/2024-03-HIFIS-Newsletter.pdf %})
- December 2023: [HTML]({% link newsletter/2023-12-xmas/2023-12-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2023-12-xmas/2023-12-HIFIS-Newsletter.pdf %})
- November 2023: [HTML]({% link newsletter/2023-11/2023-11-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2023-11/2023-11-HIFIS-Newsletter.pdf %})
- July 2023: [HTML]({% link newsletter/2023-07/2023-07-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2023-07/2023-07-HIFIS-Newsletter.pdf %})
- April 2023: [HTML]({% link newsletter/2023-04/2023-04-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2023-04/2023-04-HIFIS-Newsletter.pdf %})
- December 2022: [HTML]({% link newsletter/2022-12-xmas/2022-12-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2022-12-xmas/2022-12-HIFIS-Newsletter.pdf %})
- October 2022: [HTML]({% link newsletter/2022-10/2022-10-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2022-10/2022-10-HIFIS-Newsletter.pdf %})
- July 2022: [HTML]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.md %}), [PDF]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.pdf %})

## Subscribe and share the links:

{% include subscription_lists.md %}

Spread the word and share the links. Thank you!
