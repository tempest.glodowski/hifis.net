---
title: HIFIS SAB
title_image: default
layout: default
redirect_from:
  - mission/members-sab.html
  - mission/members-sab
  - sab
excerpt:
    Detailed description of the HIFIS governance structures.
---

## Members of the Scientific Advisory Board (SAB)
The Assembly of Members of the Helmholtz Association has installed the Scientific Advisory Board (SAB). 
The following internationally renowned scientists contribute to HIFIS's progress by their advice as SAB members:
* Ari Asmi; Research Data Alliance Assocation AISBL
* Rosa M Badia; BSC
* Magchiel Bijsterbosch; SURF
* Michael Brünig; University of Queensland
* Isabel Campos; CSIC
* Tiziana Ferrari; EGI
* Florian Freund; FhG
* Andy Götz; ESRF
* Christian Grimm; DFN
* Marc Heron; DLS
* Mirjam van Daalen; PSI
* Jörg Herrmann; MPG
* Neil Chue Hong; SSI
* Christine Kirkpatrick; NDS, SDSC
* Rafael Laguna; Open-Xchange AG
* Rupert Lück; EMBL
* Pierre Etienne Macchi; IN2P3 CNRS
* Wolfgang E. Nagel; TU Dresden
* Davide Salomoni; INFN

### HIFIS SAB Meetings and Reports
So far, the HIFIS SAB convened for three annual meetings and provided detailed feedback on progress and potential future developments of HIFIS:

- [1<sup>st</sup> Meeting]({% post_url 2020/05/2020-05-27-sab-summary %}), May  2020: [Report]({% link structure/sab_reports/SAB_Feedback_2020.pdf %})
- [2<sup>nd</sup> Meeting](https://events.hifis.net/event/136/), June 2021: [Report]({% link structure/sab_reports/SAB_Feedback_2021.pdf %})
- [3<sup>rd</sup> Meeting](https://events.hifis.net/event/417/), June 2022: [Report]({% link structure/sab_reports/SAB_Feedback_2022.pdf %})
- [4<sup>th</sup> Meeting](https://events.hifis.net/event/877/), July 2023.