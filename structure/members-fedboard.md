---
title: HIFIS Federation Board
title_image: default
layout: default
redirect_from:
  - mission/members-fedboard.html
  - mission/members-fedboard
  - fedboard
excerpt:
    Detailed description of the HIFIS governance structures.
---

## Members of the HIFIS Federation Board (Lenkungskreis)
The following high-ranking scientists agreed to steer, monitor and strategically orient the activities of HIFIS.

#### Full members
* Michael Felderer, German Aerospace Center (DLR)
* Bernd Mohr, Forschungszentrum Jülich (FZJ)
* Marco Nolden, German Cancer Research Center (DKFZ), _Spokesperson_
* Corinna Schrum, Helmholtz-Zentrum hereon
* Ruth Schwaiger, Forschungszentrum Jülich (FZJ)
* Achim Streit, Karlsruhe Institute of Technology (KIT)

#### Guests
* Volker Gülzow, Deutsches Elektronen-Synchrotron (DESY)
* Uwe Konrad, Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
* Ants Finke, Helmholtz-Zentrum Berlin (HZB)
