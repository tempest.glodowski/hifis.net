---
title: Publications
title_image: default
layout: default
redirect_from:
  - mission/publications.html
  - mission/publications
excerpt:
    Publications on HIFIS.
---

# Publications, Presentations, Selected Blog Posts

### Further links:

- [HIFIS related Publications in Zenodo <i class="fas fa-external-link-alt"></i>](https://zenodo.org/communities/hifis)
- [Software Publications](#software)

### 2023
* Beermann, Thomas, and Wagner, Sebastian. 2023. [__Developing and deploying the Helmholtz Cloud Portal__](https://doi.org/10.5281/zenodo.8424275). In IT4Science-Days, Berlin. Zenodo. DOI:&nbsp;10.5281/zenodo.8424275
* Huste, Tobias. 2023 [__Continuous Integration in Complex Research Software - Tearing Down Borders__](https://doi.org/10.5281/zenodo.10006276). In IT4Science-Days, Berlin. Zenodo. DOI:&nbsp;10.5281/zenodo.10006276
* Wagner, Sebastian. 2023. [__Towards a Global Digital Research Landscape__](https://doi.org/10.5281/zenodo.8424775). In IT4Science-Days, Berlin. Zenodo. DOI:&nbsp;10.5281/zenodo.8424775
* Schmidt, Peter, Erxleben, Fredo. 2023. [__Bringt uns Programmieren bei!__](https://codeforthought.buzzsprout.com/1326658/13072511-de-bringt-uns-programmieren-bei-mit-fredo-erxleben) Code for Thought Podcast, 5,19 (June 2023).
* Meeßen, Christian. 2023. [__Increasing the visibility of Research Software: The Helmholtz Research Software Directory.__](https://doi.org/10.5281/zenodo.7914477) Zenodo. DOI:&nbsp;10.5281/zenodo.7914477
* Schmahl, Ines, Schworm, Stephanie, Erxleben, Fredo, and Lemster, Christine. 2023. [__Report on the TEACH Conference 2022__](https://doi.org/10.5281/zenodo.7956292). DOI:&nbsp;10.5281/zenodo.7956292
* Schlauch, Tobias. 2023. [__Software Citation Current Practice and Recent Developments, Love your data? Make it reproducible!__](https://doi.org/10.48440/os.helmholtz.063) A workshop on reproducibility in data science on 14.02.2023. Potsdam : Helmholtz Open Science Office. DOI:&nbsp;10.48440/os.helmholtz.063
* Erxleben, Fredo. 2023. [__HIFIS: Building a Workshop Portfolio from 0__](https://doi.org/10.5281/zenodo.7669752). In deRSE23 - Conference for Research Software Engineering in Germany (deRSE23). Zenodo. DOI:&nbsp;10.5281/zenodo.7669752
* Thomas Förster. 2023. [__Fostering quality improvement for RSE projects through consulting offers__](https://doi.org/10.5281/zenodo.7742216). In deRSE23 - Conference for Research Software Engineering in Germany (deRSE23). Zenodo. DOI:&nbsp;10.5281/zenodo.7742216
* Konrad, Uwe, Huste, Tobias, Jandt, Uwe, Schäfer, David, and Schnicke, Thomas. 2023. [__From source code to Software as a Service, how to make software more accessible?__](https://doi.org/10.5281/zenodo.7653477). In deRSE23 - Conference for Research Software Engineering in Germany (deRSE23). Zenodo. DOI:&nbsp;10.5281/zenodo.7653477
* Schlauch, Tobias. 2023. [__All you need to know about Software Licenses as a RSE__](https://doi.org/10.5281/zenodo.7660415). In deRSE23 - Conference for Research Software Engineering in Germany (deRSE23). Zenodo. DOI:&nbsp;10.5281/zenodo.7660415
* Huste, Tobias. 2023. [__HIFIS - Boosting Research Software Engineering at Helmholtz__](https://doi.org/10.5281/zenodo.7638538). Zenodo. DOI:&nbsp;10.5281/zenodo.7638538
* Lehnigk, Ronald, Bruschewski, Martin, Huste, Tobias, Lucas, Dirk, Rehm, Markus and Schlegel, Fabian. 2023. [__Sustainable development of simulation setups and addons for OpenFOAM for nuclear reactor safety research__](https://doi.org/10.1515/kern-2022-0107). Kerntechnik 88,2 (Feb. 2023). DOI:&nbsp;10.1515/kern-2022-0107
* Klaffki, Lisa. 2023. [__HIFIS has a Zenodo Community__]({% post_url 2023/2023-01-17-zenodo-community %}). Blog post, January 2023.

### 2022

* **Applications of Helmholtz AAI** for FZJ IT-Forum, December 2022.
* Meeßen, Christian, Hammitzsch, Martin, and Konrad, Uwe. 2022. [**Aktueller Stand des Helmholtz Research Software Directory**](https://doi.org/10.5281/zenodo.7360386). In 3. Helmholtz Open Science Forum: Forschungssoftware, online, November 2022. Zenodo. DOI:&nbsp;10.5281/zenodo.7360386
* Meeßen, Christian, Hammitzsch, Martin, and Konrad, Uwe. 2022. [**The Research Software Directory in Helmholtz**](https://doi.org/10.5281/zenodo.7347366). In The Research Software Directory: Improving the impact of research software, online, November 2022. Zenodo. DOI:&nbsp;10.5281/zenodo.7347366
* Wetzel, Tim, Fuhrmann, Patrick, and Schuh, Michael [**Integrated dataset placement service for scientists**](https://indico.lip.pt/event/1249/contributions/4438/). In [IBERGRID 2022](https://indico.lip.pt/event/1249/page/134-home) Faro, October 2022.
* Förster, Thomas, and Huste, Tobias. 2022. [**Helmholtz Federated IT Services: Enabling innovative software and supporting services for research**](https://indico.lip.pt/event/1249/contributions/4439/). In [IBERGRID 2022](https://indico.lip.pt/event/1249/page/134-home) Faro, October 2022.
* **Helmholtz Cloud and Helmholtz AAI** for LRZ-DKRZ, October 2022.
* Meeßen, Christian, Hammitzsch, Martin, and Konrad, Uwe. 2022. **[Sichtbarkeit und FAIRness von Forschungssoftware erhöhen: das Research Software Directory](https://doi.org/10.5281/zenodo.7083033)**. In dini Workshop "Forschungssoftware managen", Stuttgart, September 2022. Zenodo. DOI:&nbsp;10.5281/zenodo.7083033
* Hüser, Christian, Ziegner, Norman, and Huste, Tobias. 2022. **[Scaling IT Services for Germany’s Largest Research Organisation - An Experience Report](https://doi.org/10.5281/zenodo.7248114)**. In DevOpsDays Berlin, September 2022. Zenodo. DOI:&nbsp;10.5281/zenodo.7248114
* Stoffers, Martin, Schlauch, Tobias, Caspart, René, and Struck, Alexander. 2022. [**Recap: Workshop Let's Talk FAIR for Research Software at SE2022**](https://de-rse.org/blog/2022/06/02/recap-workshop-lets-talk-fair-for-research-software-at-se2022.html). deRSE Blog post, June 2022.
* **Research Software Development @Helmholtz**. In MPG Worskhop Future Opportunities for Software in Research (FORS2022), May 2022.
* **Helmholtz AAI Info-Session** for HZB-IT, May 2022.
* Huste, Tobias. 2022. **[Helmholtz Digital Services for Science — Collaboration made easy.](https://doi.org/10.48440/os.helmholtz.045)**. In Helmholtz Open Science Forum Research Software, April 2022. Potsdam : Helmholtz Open Science Office. DOI: 10.48440/os.helmholtz.045
* Klotz, Andreas, Apweiler, Sander, and Leander-Knoll, Matthias. 2022. [**HIFIS: VO Federation for EFSS**](https://www2.dfn.de/fileadmin/3Beratung/Betriebstagungen/BT76/202203_DFN-BT_Klotz_ForumCloud.pdf). In [76. DFN Betriebstagung](https://www2.dfn.de/veranstaltungen/bt/vortraege/76-betriebstagung-2903-bis-30032022), March 2022. DFN.
* **Helmholtz Cloud Info-Session**. HZB virtual IT-Open Office, March 2022.
* Konrad, Uwe, Meeßen, Christian, Hammitzsch, Martin, Huste, Tobias, and Jandt, Uwe. 2022. [**From License Consultation to Software Spotlights**](https://doi.org/10.5281/zenodo.6248895). In RDA Deutschland Conference, February 2022. Zenodo. DOI:&nbsp;10.5281/zenodo.6248895
* Schlegel, Fabian, Huste, Tobias, Juckeland, Guido, and Konrad, Uwe. 2022. [**OpenSource im Spannungsfeld akademischer und wirtschaftlicher Interessen**](https://www.youtube.com/watch?v=IsOCFNHTHuU). In Software Engineering Conference Deutschland (rSE22), February 2022.
* Konrad, Uwe, Huste, Tobias, Jandt, Uwe, and Schlauch, Tobias. 2022. [**Helmholtz Federated IT Services: Innovative Cloud and Software Services for Science**](https://www.youtube.com/watch?v=4T1oyoEPUpE). In Software Engineering Conference Deutschland (rSE22), February 2022.
* Klotz, Andreas. 2022. [**HIFIS: VO Federation for EFSS**](https://indico.cern.ch/event/1075584/contributions/4658939/). In [CS3 2022 — Cloud Storage Synchronization and Sharing](https://indico.cern.ch/event/1075584/), January 2022.

### 2021

* Jandt, Uwe. 2021. [**HIFIS Survey 2021 Report**](https://hifis.net/news/2021/12/20/survey-report). Blog Post, December 2021.
* Huste, Tobias. 2021. [**Solutions for the Ages — a Short Crash Course on Sustainable Software Development**](https://www.hzdr.de/publications/Publ-32579). In HIDA Annual Conference 2021, November 2021. HZDR. Publ.-Id::&nbsp;32579
* Haupt, Carina, and Stoffers, Martin. 2021. [**Roles in Research Software Engineering (RSE) Consultancies**](https://doi.org/10.5281/zenodo.5530443). In [Research Software Engineers in HPC Workshop at SC21 (RSE-HPC-2021)](https://us-rse.org/rse-hpc-2021/), November 2021. Zenodo. DOI:&nbsp;10.5281/zenodo.5530443
* Klotz, Andreas. 2021. [**HIFIS: VO Federation for EFSS**](https://indico.cern.ch/event/1078853/contributions/4576197/). In [HEPiX Automn 2021 online Workshop](https://indico.cern.ch/event/1078853/), October 2021.
* Jandt, Uwe. 2021. [**Helmholtz IT Services for Science: HIFIS**](https://indico.cern.ch/event/1078853/contributions/4580117/). In [HEPiX Automn 2021 online Workshop](https://indico.cern.ch/event/1078853/), October 2021.
* Jandt, Uwe, and Hardt, Marcus. 2021. [**Helmholtz AAI full presentation**]({% post_url 2021/10/2021-10-22-helmholtz-aai-full-presentation-in-dfn-meeting %}). In [75. DFN Betriebstagung](https://www2.dfn.de/veranstaltungen/bt/infos/to-bt/#c18414), October 2021.
* Klein, Holger, and Jandt, Uwe. 2021. [**Helmholtz Resonator Podcast on HIFIS**](https://resonator-podcast.de/2021/res172-hifis/). [Resonator-Podcast](https://resonator-podcast.de/), October 2021.
* Messerschmidt, Reinhard, Pampel, H., Bach, F., zu Castell, W., Denker, M., Finke, A., Fritzsch, B., Hammitzsch, M., Konrad, Uwe, Leifels, Y., Möhl, C., Nolden, M., Scheinert, M., Schlauch, Tobias, Schnicke, T., and Steglich, D. 2021. [**Checkliste zur Unterstützung der Helmholtz-Zentren bei der Implementierung von Richtlinien für nachhaltige Forschungssoftware**](https://doi.org/10.48440/os.helmholtz.031). Potsdam : Helmholtz Open Science Office. DOI:&nbsp;10.48440/os.helmholtz.031
* Jandt, Uwe. 2021. [**HIFIS: IT-Services für Helmholtz & Partner**](https://www2.dfn.de/fileadmin/5Presse/DFNMitteilungen/DFN_Mitteilungen_99.pdf). In [DFN Mitteilungen](https://www2.dfn.de/publikationen/dfnmitteilungen/), pp. 44--45, June 2021. DFN.
* Huste, Tobias, and Jandt, Uwe. 2021. [**Easy Diagram creation in Gitlab**](https://hifis.net/tutorial/2021/06/02/easy-diagram-creation-in-gitlab.html). Blog post, June 2021.
* Ferguson, L. M., Pampel, H., Bertelmann, R., Dirnagl, U., Zohbi, J. E., Kapitza, D., Keup-Thiel, E., Konrad, U., Lorenz, S., Mittermaier, B., Rechid, D., and Schuck-Zöller, S. 2021. [**Indikatoren für Open Science: Report des Helmholtz Open Science Forum**](https://doi.org/10.48440/os.helmholtz.024). Potsdam : Helmholtz Open Science Office. DOI:&nbsp;10.48440/os.helmholtz.024
* Huste, Tobias. 2021. [**Solutions for the Ages — a Short Crash Course on Sustainable Software Development**](https://www.hzdr.de/publications/Publ-32579). In International Virtual Covid Data Challenge 2021, April 2021. HZDR. Publ.-Id::&nbsp;32579
* Hardt, Marcus. 2021. [**Helmholtz AAI Update**]({% post_url 2021/04/2021-04-14-helmholtz-aai-in-nfdi-meeting %}). Presentation on the role of Helmholtz AAI in the context of AARC, EOSC, and NFDI, April 2021.
* [**Using Containers in Science**](https://hifis.gitlab.io/hifis-workshops/using-containers-in-science/). Workshop material, March 2021.
* Jandt, Uwe. 2021. [**Helmholtz Federated IT Services**](https://www.openstoragenetwork.org/wp-content/uploads/2021/03/National-and-International-Trends-in-Research-Storage-at-Scale-Concept-Paper-1.pdf). In National and International Trends in Research Storage at Scale, Open Storage Network Concept Paper, March 2021.
* Hardt, Marcus. 2021. [**Helmholtz AAI**](http://marcus.hardt-it.de/2103-Helmholtz-AAI). Teaser talk. In [74. DFN Betriebstagung](https://www2.dfn.de/veranstaltungen/bt/infos/to-bt/#c18414), March 2021.
* Wetzel, Tim, Fuhrmann, Patrick, Millar, Paul, and Jandt, Uwe. 2021. [**HIFIS transfer service: FTS for Helmholtz**](https://indico4.twgrid.org/event/14/contributions/399/). In [ISGC 2021 — International Symposium on Grids & Clouds](https://indico4.twgrid.org/indico/event/14/), March 2021.
* Apweiler, Sander, Klotz, Andreas, and Leander-Knoll, Matthias. 2021. [**HIFIS: Sync&Share Federation for Helmholtz**](https://indico.cern.ch/event/970232/contributions/4157924/). In [CS3 2021 — Cloud Storage Synchronization and Sharing](https://indico.cern.ch/event/970232/program), January 2021.

### 2020

* Konrad, Uwe, Förstner, Konrad, Reetz, Johannes, Wannemacher, Klaus, Kett, Jürgen, and Mannseicher, Florian. 2020. [**Digitale Dienste für die Wissenschaft - wohin geht die Reise?**](https://doi.org/10.5281/zenodo.4301924). Zenodo. DOI:&nbsp;10.5281/zenodo.4301924
* Konrad, Uwe, Förstner, Konrad, Reetz, Johannes, Wannemacher, Klaus, Kett, Jürgen, and Mannseicher, Florian. 2020. [**Digital services for science – where is the journey heading?**](https://doi.org/10.5281/zenodo.4301947). Zenodo. DOI:&nbsp;10.5281/zenodo.4301947
* Huste, Tobias, Hüser, Christian, Dolling, Maximilian, Dworatzyk, Katharina, Frere, Jonathan, and Erxleben, Fredo. 2020. **HIFIS Software Survey 2020 Evaluation**. Blog Post Series, Nov 2020-Jan 2021.
  * [A Community Perspective]({% post_url 2021/01/2021-01-21-survey-results-community %})
  * [Consulting Perspective]({% post_url 2020/12/2020-12-16-survey-results-consulting %})
  * [Programming, CI and VCS]({% post_url 2020/11/2020-11-27-survey-results-language-vcs %})
  * [A Technology Perspective]({% post_url 2020/11/2020-11-27-survey-technology %})
* Jandt, Uwe. 2020. [**Federated data storage for Helmholtz Research & Friends**](https://www.openstoragenetwork.org/seminar-series/nov-12-2020-national-and-international-trends-in-research-storage-at-scale/). In National and International Trends in Research Storage at Scale, [Open Storage Network](https://www.openstoragenetwork.org), November 2020.
* Jandt, Uwe, and Wetzel, Tim. 2020. In [EGI conference](https://indico.egi.eu/event/5000/overview) (2-5 November 2020):
  - [**Helmholtz Federated IT and Accessible Compute Resources for Applied AI Research**](https://indico.egi.eu/event/5000/contributions/14353/).
  - [**HIFIS transfer service: FTS for everyone**](https://indico.egi.eu/event/5000/contributions/14383/).
* Jandt, Uwe, Apweiler, Sander, Schollmaier, Laura, Klotz Andreas, and Dolling, Maximilian. 2020. [**Presentations** of all major HIFIS working groups](https://events.hifis.net/event/25/timetable/#all.detailed). In 2nd All-Hands HIFIS Meeting, October 2020.
* Wetzel, Tim. 2020. [**HIFIS backbone transfer service: FTS for everyone**](https://indico.cern.ch/event/898285/contributions/4041954/). In [HEPiX Autumn 2020 Online workshop](https://indico.cern.ch/event/898285/), October 2020.
* Frere, Jonathan. 2020. **Docker For Science**. Blog post series, September 2020:
  * [Part 1: Getting Started with Docker]({% post_url 2020/09/2020-09-23-getting-started-with-docker-1 %})
  * [Part 2: A Dockerfile Walkthrough]({% post_url 2020/09/2020-09-25-getting-started-with-docker-2 %})
  * [Part 3: Using Docker in Practical Situations]({% post_url 2020/09/2020-09-29-getting-started-with-docker-3 %})
* **HIFIS — Platform, Training and Support for a Sustainable Software Development**. In DLR Wissensaustausch-Workshop Software Engineering (WAW SE VII), September 2020.
* [**Zusammenarbeiten mit der Helmholtz-Cloud**](https://www.helmholtz-berlin.de/media/media/oea/aktuell/print//lichtblick/246/hzb_lichtblick_ag-44_september-2020_extern_web.pdf), Lichtblick Helmholtz Zentrum Berlin, p. 4, September 2020.
* Glöckner, Stephan. 2020. [**HZI-led Coronavirus Sero-Survey** in collaboration with HIFIS](https://www.dzif.de/en/how-many-people-are-now-immune-sars-cov-2). DZIF Press Release, August 2020.
  * [German Version](https://www.dzif.de/de/wie-viele-menschen-sind-heute-schon-immun-gegen-sars-cov-2)
* Jandt, Uwe. 2020. **Promoting IT based science at all levels**. In [EGI Newsletter #37](https://preview.mailerlite.com/s9b7l1/1435456442688607486/y2j8/), June 2020.
* Hammitzsch, Martin et al. 2020. [**HIFIS suggestions and guidelines**]({% link services/software/training.md %}#guidelines). Blog post series, May-June 2020.
  * [Collaborative Notetaking]({% post_url 2020/06/2020-06-26-guidelines-for-collaborative-notetaking %})
  * [Video Conferencing]({% post_url 2020/05/2020-05-15-guidelines-for-video-conferencing %})
  * [Chatting]({% post_url 2020/05/2020-05-15-guidelines-for-chatting %})
* Jandt, Uwe. 2020. [**Summary of the Feedback Report of the Scientific Advisory Board**]({% post_url 2020/05/2020-05-27-sab-summary %}). Blog Post, May 2020.
* Huste, Tobias. 2020. [**S/MIME Signing Git Commits**]({% post_url 2020/04/2020-04-15-smime-signing-git-commits %}). Blog post, April 2020.
* Klotz, Andreas, Fuhrmann, Patrick, and Schollmaier, Laura. 2020. [**The HIFIS Cloud Competence Cluster**](https://indico.cern.ch/event/854707/contributions/3680436/). In [CS3 2020 Workshop on Cloud Services for Synchronisation and Sharing](https://cs3.deic.dk/), January 2020.

### 2019

* [**Presentations**](https://indico.desy.de/event/23411/attachments/32388/) at the 1st HIFIS Conference, October 2019.
* Haupt, Carina. 2019. [**Helmholtz Federated IT Services (HIFIS) — Creating Services together**](https://betterscientificsoftware.github.io/swe-cse-bof/2019-11-sc19-bof/04-haupt-helmholtz.pdf). In [Software Engineering and Reuse in Modeling, Simulation, and Data Analytics for Science and Engineering (SC 2019 BOF)](https://betterscientificsoftware.github.io/swe-cse-bof/2019-11-sc19-bof/), November 2019.
* Hagemeier, Björn. 2019. [**HDF Cloud — Helmholtz Data Federation Cloud Resources at the Jülich Supercomputing Centre**](https://doi.org/10.17815/jlsrf-5-173). Journal of large-scale research facilities, 5, A137. DOI:&nbsp;10.17815/jlsrf-5-173
* Frust, Tobias, and Konrad, Uwe. 2019. [**Integrierte Entwicklungs- und Publikationsumgebung für Forschungssoftware und Daten am Helmholtz-Zentrum Dresden-Rossendorf (HZDR)**](https://doi.org/10.5446/42518). In [deRSE19](https://av.tib.eu/series/644), Potsdam, June 2019. Technische Informationsbibliothek (TIB). DOI:&nbsp;10.5446/42518
* Konrad, Uwe. 2019. [**Anforderungen, Wünsche und Erfahrungen für den Aufbau des HIFIS Competence Clusters "Software Services" in der Helmholtz-Gemeinschaft**](https://de-rse.org/de/conf2019/talk/LQDSZW/). In deRSE19, Potsdam, June 2019.
* Frust, Tobias. 2019. [**Continuous Documentation for Users, Developers and Maintainers**](https://zenodo.org/record/3247324). In [Platform for Advanced Scientific Computing Conference 2019 (PASC19)](https://pasc19.pasc-conference.org/), Zurich, Switzerland. Zenodo. DOI:&nbsp;10.5281/zenodo.3247324
* Share and synchronize your data with HZB Cloud Nubes safely


<p id="software"></p>
## Software Publications

HIFIS creates its own software packages, but also contributes to Open Source software in general.

### Software Packages

#### 2023
* Sanjari, Shahab. 2023. [**iqtools: Collection of code for working with offline complex valued time series data in Python**](https://doi.org/10.5281/zenodo.8056233). DOI:&nbsp;10.5281/zenodo.8056233

#### 2022

* [Unattended-Upgrades Role for Ansible](https://github.com/hifis-net/ansible-role-unattended-upgrades) - Ansible role to set up unattended-upgrades on Debian-based systems.
* [Ansible RSD role](https://github.com/hifis-net/ansible-role-rsd) — Ansible role to set up the Research Software Directory.

#### 2021

* [HIFIS Surveyval](https://codebase.helmholtz.cloud/hifis/overall/surveys/hifis-surveyval) — Python Package for analysing survey data.
* [Ansible Netplan Role](https://github.com/hifis-net/ansible-role-netplan) — Ansible role that installs Netplan and configures networking on hosts.
* [Ansible SSH keys role](https://gitlab.com/hifis/ansible/ssh-keys) — Ansible role to distribute authorized SSH public keys to users.

#### 2020

* [Ansible GitLab Role](https://gitlab.com/hifis/ansible/gitlab-role) — Ansible role to install and configure the GitLab Omnibus package, also in a high availability context.
* [Ansible Keepalived Role](https://gitlab.com/hifis/ansible/keepalived-role) — Ansible role that sets up Keepalived for high availabilty.
* [Ansible Redis Role](https://gitlab.com/hifis/ansible/redis-role) — Ansible role for setting up a highly available Redis cluster.
* [Ansible HAProxy role](https://gitlab.com/hifis/ansible/haproxy-role) — Ansible role to set up HAProxy to be used as a load balancer.
* [Ansible GitLab-Runner role](https://github.com/hifis-net/ansible-role-gitlab-runner) — Ansible role for deploying GitLab-Runner (optimized for Openstack).
* [GitLab-Runner for Power](https://gitlab.com/hzdr/gitlab-runner/-/releases) — GitLab-Runner packages for the `ppc64le` architecture.

### Contributions

#### 2022
* [**Molecule Podman Plugin**](https://github.com/ansible-community/molecule-podman) - [Fix missing playbooks in the PyPI wheel](https://github.com/ansible-community/molecule-podman/pull/134)
* [**Kira Dependencies Bot**](https://github.com/wemake-services/kira-dependencies):
  * [Bump ruby version from 2.7.5 to 2.7.6](https://github.com/wemake-services/kira-dependencies/pull/393)
  * [Get rid of bundler deprecation warning](https://github.com/wemake-services/kira-dependencies/pull/396)
* [**Ansible Role Certbot**](https://github.com/geerlingguy/ansible-role-certbot) — [Fix snap symlink task failing in initial dry-run](https://github.com/geerlingguy/ansible-role-certbot/pull/166)
* [**Ansible Role Docker**](https://github.com/geerlingguy/ansible-role-docker) — [Fix docker-compose update](https://github.com/geerlingguy/ansible-role-docker/pull/343)

#### 2021
* [**Gitleaks**](https://github.com/zricethezav/gitleaks) — [Fix example in leaky-repo.toml](https://github.com/zricethezav/gitleaks/pull/559)
* [**Ansible (community.general)**](https://github.com/ansible-collections/community.general) — [Fix HAProxy draining](https://github.com/ansible-collections/community.general/pull/1993)
* [**Ansible Collection Hardening**](https://github.com/dev-sec/ansible-collection-hardening/):
  * [Add variable to specify SSH host RSA key size](https://github.com/dev-sec/ansible-collection-hardening/pull/394)
  * [fix galaxy action to update local galaxy.yml](https://github.com/dev-sec/ansible-collection-hardening/pull/395)
* [**Ansible Role Elasticsearch**](https://github.com/elastic/ansible-elasticsearch) — [Stop plugin install to fail in check mode](https://github.com/elastic/ansible-elasticsearch/pull/787)

#### 2020

* [**Python Poetry**](https://python-poetry.org/) — [Respect REUSE spec when including licenses](https://github.com/python-poetry/poetry-core/pull/57)
* [**Ansible (community.crypto)**](https://github.com/ansible-collections/community.crypto) — [openssl_pkcs12 parse action: always changed in check mode](https://github.com/ansible-collections/community.crypto/issues/143)
* [**Ansible Collection Hardening**](https://github.com/dev-sec/ansible-collection-hardening/) — [Use package state 'present' since 'installed' is deprecated](https://github.com/dev-sec/ansible-collection-hardening/pull/168)
