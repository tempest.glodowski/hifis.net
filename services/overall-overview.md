---
title: Overall Services
title_image: default
layout: default
author: none
redirect_from:
  - services/overall/index_eng.html
  - services/overall/index_ger.html
  - services/overall/
---

<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/overall/'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>