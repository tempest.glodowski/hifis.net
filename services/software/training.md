---
title: Education & Training
title_image: group-of-people-watching-on-laptop-1595385.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
redirect_from:
    - services/overall/tutorials
    - services/overall/guidelines
    - services/overall/learning-materials
    - guidelines
    - workshop-materials
excerpt:
  "We offer courses, materials or workshops to help you
  in getting started or in boosting your software engineering practice."
---

The purpose of the Education & Training component is to offer courses, materials, and workshops to help you in getting started, or to boost your engineering practices. 

The HIFIS Education team consists of small teams of instructors distributed across the DLR, GFZ and HZDR centres. These are supplemented by helpers from the entire Helmholtz Association, depending on the possibilities.

The programme is tailored to the needs of software development in a scientific context. Beginners' courses are regularly offered on important basic skills such as programming in Python, dealing with version control or publishing scientific software. The existing courses are also increasingly being expanded to include more advanced topics. For example, object-oriented programming or using Gitlab CI for project automation are now also part of the portfolio.

Check out the <i class="fas fa-external-link-alt"></i> __[HIDA course catalog][hifis-workshops]__ for upcoming HIFIS courses and events on a range of software-related topics.
Additional events will also be listed on the <i class="fas fa-external-link-alt"></i> __[Helmholtz Indico][hifis-events]__.

On this page, you can find:
- [Workshop materials](#workshop-materials) we use, 
- [Tutorials](#tutorials), 
- [Guidelines](#guidelines), 
- [External resources](#external-resources) to help you get started.

[hifis-events]: <https://events.hifis.net/category/4>

Can't find exactly what you're looking for? HIFIS Education also offers customised courses. Send us a request with your need and an instructor will contact you to discuss the specific content and organisation in detail:
{% include contact_us/mini.html %}
{% include helpdesk/mini.html %}

# Workshop Materials

Our workshop material is available for self-guided learning or as a basis for your own workshops.
Its use and reuse is permitted under a Creative Commons License (see the footers on the workshop pages).

If you find issues or would like to contribute, feel free to take a look at the respective repositories.

## General

| Topic                                  | Materials                                  | Repository                               |
| -------------------                    |--------------------------------------------| -----------                              |
| Research Software Publication          | (In preparation)                           | [Repository][sw-publication-repo]        |
| Container Virtualization in Science    | [Material][containers-in-science-material] | [Repository][containers-in-science-repo] |
| Event Management with Indico           | [Material][indico-material]                | [Repository][indico-repo]                |
| Getting started with Markdown Flavours | (In preparation)                           | [Repository][markdown-repo]              |

[containers-in-science-material]: {{ "workshop-materials/general-container/" | relative_url }}
[containers-in-science-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-using-containers-in-science
[indico-material]: {{ "workshop-materials/tool-indico/" | relative_url }}
[indico-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-event-management-with-indico
[markdown-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-getting-started-with-markdown-flavors
[sw-publication-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/foundations-of-research-software-publication

## Python

| Topic                                | Materials                              | Repository                           |
|--------------------------------------|----------------------------------------|--------------------------------------|
| First Steps in Python                | [Material][python-first-steps-material]| [Repository][python-first-steps-repo]|
| Object-Oriented Programming          | [Material][python-oop-material]        | [Repository][python-oop-repo]        |
| Data Processing with _Pandas_        | [Material][python-pandas-material]     | [Repository][python-pandas-repo]     |
| Data Visualization with _Matplotlib_ | [Material][python-matplotlib-material] | [Repository][python-matplotlib-repo] |
| Test Automation                      | (In preparation)                       | [Repository][python-test-repo]       |

[python-first-steps-material]: {{ "workshop-materials/python-first-steps/" | relative_url }}
[python-first-steps-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/python-first-steps
[python-matplotlib-material]: {{ "workshop-materials/python-matplotlib/" | relative_url}}
[python-matplotlib-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-matplotlib
[python-oop-material]: {{ "workshop-materials/python-oop/" | relative_url }}
[python-oop-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-oop-in-python
[python-pandas-material]: {{ "workshop-materials/python-pandas/" | relative_url }}
[python-pandas-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-pandas
[python-test-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/test-automation-with-python

## Git and GitLab

| Topic                                   | Materials                      | Repository                       |
|-----------------------------------------|--------------------------------|----------------------------------|
| Introduction to Git                     | (In preparation)               | [Repository][git-intro-repo]     |
| Project Management in GitLab            | (In preparation)               | [Repository][gitlab-basics-repo] |
| GitLab for Software Development in Teams| (In preparation)               | [Repository][gitlab-teams-repo]  |
| Continuous Integration with GitLab CI   | [Material][gitlab-ci-material] | [Repository][gitlab-ci-repo]     |


[git-intro-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/introduction-to-git-and-gitlab/workshop-material
[gitlab-basics-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/workshop-project-management-with-gitlab
[gitlab-ci-material]: {{ "workshop-materials/gitlab-ci/" | relative_url }}
[gitlab-ci-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/gitlab-ci/workshop-materials
[gitlab-teams-repo]: https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/software-development-for-teams

## Further Materials

Not all of our workshop materials are published in a nice fashion (yet).
You can find the complete set of materials in the [Helmholtz Codebase](https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops).

## Requesting Workshops

Would you like to have a workshop on a specific topic for your team?

Feel free to <a href="{% link contact.md %}">contact us</a>.

## Other Recommended RSE Material

* [HiRSE Summer of Testing](https://youtube.com/playlist?list=PLzCxBiTw83uil4uoIhi3wlR3qiiulmKJ0) -
  This is a series of videos focussing on testing in the context of Research Software Engineering.
* [Awesome Educational Resources for Research Software Engineering](https://github.com/hifis-net/awesome-rse-education) - 
  This is a curated list of educational resources for Research Software Engineering.

# Tutorials

<div class="flex-cards">
{%- assign posts = site.categories['Tutorial'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

# Guidelines

<div class="flex-cards">
{%- assign posts = site.categories['Guidelines'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

# External Resources

These articles are written by third party organisations, and they aren't necessarily focussed on research software development, but they represent a set of industry best practices for writing software, sharing code, and building software communities.
We've brought these together here to try and answer some common questions that researchers often ask us.

### How Do I Share My Project?

* **Licensing** -
    Open source projects need an open source license - but how do I do that, and what license should I use?
    For a simple way to get started, try [Choose A License][choose-a-license], and for a more in-depth guide to creating open source projects, try [The Open Source Guide][open-source-guide/legal].
* **Citations** -
    It's important to prepare software projects for citation as part of sustainable research software development.
    Simple ways to both prepare for citation, and cite other researchers' software, are described at this [Research Software Citation][research-software/citation] guide.
* **Documentation** -
    Writing good documentation can be hard, but Write the Docs have put together a [Beginner's Guide to Writing Documentation][write-the-docs/beginners-guide], starting with a single README file.

If you're just starting out with this topic, and you have some code you want to make ready for publication/open source sharing, we run regular workshops where we can help you with all of these topics.
Click to [**see all our workshops**][hifis-workshops] and find out more.

[choose-a-license]: <https://choosealicense.com/>
[open-source-guide/legal]: <https://opensource.guide/legal/>
[research-software/citation]: <https://research-software.org/citation>
[write-the-docs/beginners-guide]: <https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/>
[hifis-workshops]: <https://www.helmholtz-hida.de/course-catalog/en/?search%5Bfilter%5D%5B0%5D=tags%3AHIFIS>

### What Are Best Practices for Research Software?

* **Ethical Software** -
    Ethical research practices should also be visible in how we write research software.
    [The Turing Way][turing-way] is a guide to these best practices in data science, particularly reproducibility.
* **Software Layout** -
    Common patterns for software aim to make it as easy as possible for reviewers or new team members to understand how a project works.
    One pattern is using common project folder layouts.  The [Cookiecutter Data Science][cookiecutter-data-science] project provides a way to get started quickly on new data science projects in a manner that other people will be able to recognise and understand.
* **Committing** -
    Reading through commits in a shared project can be a valuable way of understanding changes that you or other people have made, and figuring out when a bug appeared.
    [How to Write a Git Commit Message][git-commit-message] explains how to describe the contents of a commit in a clean and readable way.
    Also, as a deep read, try [My favourite Git commit][my-favourite-commit].

[turing-way]: <https://the-turing-way.netlify.app/>
[cookiecutter-data-science]: <https://drivendata.github.io/cookiecutter-data-science/>
[git-commit-message]: <https://chris.beams.io/posts/git-commit/>
[my-favourite-commit]: <https://dhwthompson.com/2019/my-favourite-git-commit>


<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Still Got Questions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a> with what other questions you want answered, and what articles you'd find helpful.
  </p>
</div>

