---
title: Benefits for Helmholtz Cloud Service Providers
title_image: background-cloud.jpg
layout: services/default
author: none
additional_css:
  - image-scaling.css
excerpt: >-
    By providing a service via the Helmholtz Cloud, you increase the value of your services and resources.
---


By providing a service via the Helmholtz Cloud, you improve the value of your services and resources.

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="data sovereignty of helmholtz cloud services"
        src="{{ site.directory.images | relative_url }}/services/HIFIS_Data_Sovereignty.svg"
    />
    </div>
    <div class="text-box">
<div>
<h2> Compliance, IT-Security ... </h2>
    <p>
    <ul>
        <li>You as a Service Provider don't have to deal with <b>Authentication and Authorisation issues of external users</b>. The <a href="{% link aai/index.md %}">Helmholtz AAI</a> allows to manage cross-institutional user groups while ensuring the correct authentication of each user. It acts as a hub between the user institution's identity provider (IdP) and the service. You can use it to handle a wide range of use cases, from simple to complex.</li>
        <li>From the user’s perspective, the Helmholtz AAI eases the <b>login process</b> using the home credentials.</li>
        <li>With the Helmholtz AAI, you are using a proven, reliable service that <b>meets international standards</b> and that is <b>developed and maintained continuously</b>.</li>
        <li>The HIFIS team <b>assists technically and procedurally</b> with service integration and with user support during operation.</li>
	    <li>The <b>legal and GDPR aspects</b> for provision and use of services are guarded by the Helmholtz AAI Policies and the upcoming Helmholtz Cloud regulations. Therefore, a compliance basis is established. For the final service-specific steps, the HIFIS team supports you in compiling the necessary documents for GDPR compliance.</li>
    </ul>
    </p>
</div>

<div>
<h2>Good practice: HIFIS Expertise and Experience</h2>
   <p>
   <ul>
       <li>The <b>network of Helmholtz-wide service admins and the HIFIS team</b> are supporting you during the Service Onboarding as well as the service operation.</li>
       <li>Experiences made in the HIFIS context may <b>simplify the local service integration</b>, e. g. if a complex user management for internal and external users is needed. HIFIS has established processes not only for the integration but for the complete lifecycle of a service (onboarding, operating, review, offboarding).</li>
       <li>Enable your local sciencists to create <b>tailored digital workflows</b> with seamless interfaces between Helmholtz Cloud services.</li>
    </ul>
    </p>
</div>

<div>
<h2>User Support</h2>
   <p>
   <ul>
      <li>HIFIS offers an <b>overall ticket system for support</b>, based on Zammad. This way, you do not need to open your system for external users, but can still be sure that problems will be addressed in a timely manner.</li>
   </ul>
   </p>
</div>

<div>
<h2>Synergy Potentials</h2>
   <p>
   <ul>
      <li>The increased number of active users and more diverse use cases gained through the Helmholtz Cloud allows to <b>increase the maturity of the service</b> provided. In turn, this can improve the negotiation position towards, e.g., manufacturers or software developers.</li>
   </ul>
   </p>
</div>

<div>
<h2>Image and Competence Building</h2>
   <p>
   <ul>
      <li>On a meta level, becoming a service provider within the Helmholtz Cloud can <b>demonstrate your experience and competence</b> to a wider audience.</li>
      <li>By providing a service via Helmholtz Cloud, you can improve your standing for providing this service or even a certain service type. This way you can <b>co-create the environment of the Cloud</b> as well as demonstrate your expertise and engagement to stakeholders.</li>
      <li>Services in the Helmholtz Cloud need to have a good quality level as entry requirement. This characteristic radiates to users and stakeholders: If your service is included in the Helmholtz Cloud, they can be sure that it is offered on a resilient level. <b>This builds trust and fosters collaborations.</b></li>
      <li>The Helmholtz-wide experience exchange of Service Providers and IT admins leads to the expansion of one’s own horizon. This may ease the development of future workflows or services and can help you to build a network.</li>
      <li>The participation in the Helmholtz Cloud is by no means a one-way dedication of the provider's resources (time and effort) - you can use this as content for reports or business justifications for a centre’s activities.</li>
   </ul>
   </p>
</div>

<h2> FAQ - Frequently Asked Questions</h2>

<div>
   <p>
   <b>Does a service have to be open for all Helmholtz users?</b>
   <ul>
      <li>No, Helmholtz Cloud services may - but don't have to - be open for every Helmholtz user. The service provider will keep the complete control of any limitation for a service that is necessary, e. g. duration, number of users, resources, or availability.</li>
      <li>Even if the service is used by a limited user group, HIFIS will be happy to support your use case.</li>
   </ul>
   </p>
</div>

<div>
   <p>
   <b>I'm afraid that the additional effort cannot be handled by my institution / my project group.</b>
   <ul>
      <li>There are several possibilities to share the load of the user support. The HIFIS ticketing system might step in; and also, the first level support might be covered by the using institution or the project group.</li>
      <li>In any case, the service provider has to take care of the second level support because the IT admins need to handle the service configurations.</li>
   </ul>
   </p>
</div>

<div>
   <p>
   <b>What happens when I cannot maintain the service any longer: Would HIFIS step in here?</b>
   <ul>
      <li>All Helmholtz Cloud services are reviewed regularly. If a service cannot be maintained any more, HIFIS will look for another service provider and take care of the service offboarding (and possibly support organising the data migration).</li>
   </ul>
   </p>
</div>

<div>
   <p>
   <b>The necessary work to make my service ready for the Helmholtz Cloud might be too complex or requires too much effort. How can I estimate the work needed? Can HIFIS support me with this?</b>
   <ul>
      <li>The HIFIS team will be pleased to support you in every step of the service integration, e. g. connecting the Helmholtz AAI. Be aware, though, HIFIS is not in the position to handle your internal processes and regulations, nor can HIFIS implement internal technical changes.</li>
   </ul>
   </p>
</div>

<div>
   <p>
   <b>If we provide the service via the Helmholtz Cloud, there will be additional effort / cost to a higher amount of users without direct payment. Can this be compensated?</b>
   <ul>
      <li>All Helmholtz Cloud services are provided are free of charge. As mentioned, you are free to define limiations with respect to users and usage of the service. Addtional external costs, for example licenses, can be taken over by specifically defined user groups.</li>
   </ul>
   </p>
</div>

<h3>Feedback and Support</h3>
<div>
    <p>
        Your service has specific technical challenges to function as a cloud service, maybe in the fields of AAI, authorisation roles, or licence limitations? We, the HIFIS Cloud Team, are ready to help. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>
