---
title: HIFIS for Helmholtz Cloud Service Providers
title_image: matthew-waring-MJAoiige14E-unsplash.jpg
layout: services/default
author: none
additional_css:
  - image-scaling.css
redirect_from:
    - hifisfor/csp
excerpt: >-
    How to hand in new service to be offered in Helmholtz Cloud.
---

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="screenshot of Helmholtz Cloud Portal"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal.jpg"
    />
    </div>
    <div class="text-box">
    <h1> Science does not stop at the border of your centre. Why should your digital services and resources? </h1>
    <h2>Becoming Service Provider</h2>
    <p>
    <b> Science needs borderless and free interaction. This is why the demand of your local scientists goes far beyond your institute. By providing a service via the Helmholtz Cloud, you increase the value of your services and resources.</b>
    </p>
    <p>
    Find more about the benefits <a href="{% link services/cloud/provider_benefit.md %}"><strong>here</strong>.</a>
    </p>
    </div>
</div>

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="application form in Plony"
        src="{{ site.directory.images | relative_url }}/services/application_form.jpg"
    />
    </div>
    <div class="text-box">
    <h2>Submit a new Helmholtz Cloud Service</h2>
    <p>
        You can hand in your service application for the Helmholtz Cloud anytime. Use our <a href="https://plony.helmholtz.cloud/">Service Database Plony</a> and follow the step-by-step instruction on the landing page.
        </p>
        <p>
        In the <a href="https://helmholtz.cloud/">Helmholtz Cloud</a>, Helmholtz Centres provide IT services for joint use and overall added value to the Helmholtz Association. Applications originating from local or domain-specific contexts converge to cross-domain and cross-centre collaborative services.
    </p>
    </div>
</div>
<div class="clear"></div>

<div>
<h2>Helmholtz Cloud Service Portfolio</h2>
    <p>
    The Helmholtz Cloud Service Portfolio strives to cover the whole scientific process and offers a federated community cloud with uniform access to Helmholtz employees and their project partners, thus enabling them to conduct and support excellent science. It is managed carefully based on Information Technology Infrastructure Library (ITIL) and consists of three main components:
    </p>
    <ul>
        <li>the Service Pipeline = <a href="https://plony.helmholtz.cloud/sc/public-service-portfolio/">Upcoming Services</a></li>
        <li>the Service Catalogue = currently provided Services, available on <a href="https://helmholtz.cloud/services/">Helmholtz Cloud Portal</a></li>
        <li>the Retired Services = previously provided services, not offered anymore</li>
    </ul>
</div>

<div>
<h2> HIFIS Technical and Administrative Documentation</h2>
    <p>
        All details on technical and administrative procedures are publicly available in the <a href="https://hifis.net/doc/">HIFIS technical Documentation</a>:
    </p>
    <ul>
        <li><a href="https://hifis.net/doc/helmholtz-aai/">Helmholtz AAI</a></li>
        <li><a href="https://hifis.net/doc/helmholtz-aai/policies/">Helmholtz AAI Policies</a></li>
        <li><a href="https://hifis.net/doc/core-services/fts-endpoint/">Backbone Core Services</a></li>
        <li><a href="https://hifis.net/doc/cloud-services/">Helmholtz Cloud</a></li>
        <li><a href="https://hifis.net/doc/process-framework/Chapter-Overview/">Process Framework for Helmholtz Cloud Service Portfolio</a></li>
        <li><a href="https://hifis.net/doc/software/">Software Service</a></li>
    </ul>
</div>

<div>
<h3>Service Operation KPI</h3>
    <p>
        The usage of each Helmholtz Cloud Serivce and its overall contribution to the success of the Helmholtz Cloud is monitored continuously.
    </p>
    <p>
        Please refer to the <a href="https://hifis.net/doc/cloud-service-kpi/">documentation pages</a> for details on the procedures of KPI data aquisition and calculation, as well as most recent and previously published results.
    </p>
</div>

<h3>Feedback and Support</h3>
<div>
    <p>
        Your service has specific technical challenges to function as a cloud service, maybe in the fields of AAI, authorisation roles, or licence limitations? We, the HIFIS Cloud Team, are ready to help. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>


