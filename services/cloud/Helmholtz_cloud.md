---
title: Helmholtz Cloud 
title_image: background-cloud.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
redirect_from:
    - services/cloud/Service_portfolio
    - services/cloud/cloud_login
excerpt: >-
    The Helmholtz Cloud offers IT services to all Helmholtz centres and the entire scientific community and partners.
---

<div class="image-block">
    <img
        class="help-image left"
        alt="Helmholtz Cloud Portal Screenshot"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
            <strong>In the <a href="https://helmholtz.cloud/">Helmholtz Cloud</a>, members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.</strong><br>
        The Service Portfolio covers the whole scientific process and offers Helmholtz employees and their project partners a federated community cloud with uniform access for them to conduct and support excellent science. 
        The connection to international developments (e.g. EOSC at European level) is regarded as essential and the "FAIR" principles in particular are taken into account.
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud services"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal_Services.png"
        style="max-width: 25rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h3>Helmholtz Cloud Offerings</h3>
        <p>
            Formerly isolated applications converge to central collaboration services for science as well as infrastructure, offered in the Helmholtz Cloud. By 2024, the Helmholtz Cloud portfolio includes more than 30 services. The complete list and all further information can be found at the Helmholtz Cloud Portal. A selection:
        </p>
            <ul>
                <li> <a href="https://helmholtz.cloud/services/?serviceID=ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b">HIFIS Events</a> – Managing events for everyone </li>
                <li> <a href="https://helmholtz.cloud/services/?serviceID=1be91786-b7e7-4fa3-81d9-1b95dd03cd52">Mattermost</a> – Web based chat service </li>
                <li> <a href="https://helmholtz.cloud/services/?serviceID=cea6f848-b46b-4340-92fe-17da42a78829">Notes</a> – Collaborative writing and sharing of Markdown documents </li>
                <li> <a href="https://helmholtz.cloud/services/?filterSearchInput=jupyterlab">Jupyter</a> – Interactive computing via Jupyter Notebooks </li>
                <li> <a href="https://helmholtz.cloud/services/?serviceID=c5d1516e-ffd2-42ae-b777-e891673fcb32">Helmholtz Codebase</a> – Collaborative software development platform with extensive Continuous Integration (CI) offering </li>
                <li> All Services on <a href="https://helmholtz.cloud/services/">Helmholtz Cloud Portal</a> </li>
            </ul>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="Service Portfolio in terms of ITIL"
        src="{{ site.directory.images | relative_url }}/services/service_portfolio.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h3>Service Portfolio and Upcoming Services</h3>
        <p>
            The Service Portfolio Management consists of three main components: 
        </p>
        <ul>
            <li> the <a href="https://plony.helmholtz.cloud/sc/public-service-portfolio">Service Pipeline</a> = Upcoming Services </li>
            <li> the Service Catalogue = currently provided Services, available on <a href="https://helmholtz.cloud/services/">Helmholtz Cloud Portal</a> </li>
            <li> the Retired Services = taken out of service (currently none) </li>
        </ul>
        <p>
            Our recruitment team is constantly on the lookout for new services to include in our portfolio and to find providers for any missing services. We operate using a priority system, taking into account factors such as the number of requests we receive for specific services. 
        </p>
        <p>
            If you notice that a service is missing from our offering, we would be grateful if you could reach out to us at <a href="mailto:support@hifis.net">support@hifis.net</a> so that we can update our tracking system and prioritise according to the community needs. Also, if you are interested in seeing an existing service offered in a "pro" version, please do let us know as well. The more requests we receive, the more likely we are to be able to negotiate pro-versions with service providers.
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud Services orchestration"
        src="{{ site.directory.images | relative_url }}/services/service_orchestration.svg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h3>Service Orchestration</h3>
        <p>
            Helmholtz Cloud Services cover most of the research lifecycle, but until now, users have had to connect these complementary services to work together manually. A recent focus of the Cloud Cluster is to remedy this by providing support to automate service orchestration and promote better composability of existing services. To find out more, please visit our dedicated <a href="{% link services/cloud/orchestration.md %}">service orchestration page</a>.
        </p>
    </div>
</div>

<div>
    <h3>Seamless Collaboration</h3>
    <p>
        The Helmholtz-wide, internationally accessible authentication and authorization infrastructure <a href="{% link aai/index.md %}">Helmholtz ID</a> enables login to all Cloud Services with the known credentials of the home Helmholtz centres. Furthermore, invited guests from outside Helmholtz are also able to use Helmholtz Cloud services, e.g. via their home organisation's login, or via public identity providers such as ORCID. In this central login, the Helmholtz ID, 7000 active users were recorded as of January 2024, trend rising.
    </p>
    <p>
        Interested in learning more?
        <ul>
            <li>The login process from a user's perspective is <a href="{% link aai/howto.md %}">visualised in this tutorial</a>.</li>
            <li>The organisational framework is provided by the <a href="https://hifis.net/doc/backbone-aai/policies/">Helmholtz AAI Policies</a>.</li>
        </ul>
    </p>
    <p>
        Helmholtz Cloud is funded by the Helmholtz Association of German Research Centres. As a result, the use of Helmholtz Cloud services can be offered free of charge to Helmholtz users and their collaboration partners.
    </p>
</div>

### Service Operation KPI
The usage of each Helmholtz Cloud Serivce and its overall contribution to the success of the Helmholtz Cloud is monitored continuously.
Please refer to the [documentation pages](https://hifis.net/doc/cloud-service-kpi/) for details on the procedures of KPI data aquisition and calculation,
as well as most recent and previously published results.

### <i class="fas fa-address-book"></i> Request for Service and Support
You cloudn't find what you were looking for? We welcome your suggestions, tips and comments. They help us to become better. Please contact us at [{{ site.contact_mail }}](mailto:{{ site.contact_mail }}).
