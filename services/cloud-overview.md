---
title: Cloud Services
title_image: default
layout: default
author: none
redirect_from:
  - services/cloud/index_eng.html
  - services/cloud/index_ger.html
  - services/cloud/
---

In the Helmholtz Cloud, members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.

<div class="flex-cards">
{%- assign posts = site.pages | where_exp: "item", "item.path contains 'services/cloud/'" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post excerpt=true %}
{% endfor -%}
</div>