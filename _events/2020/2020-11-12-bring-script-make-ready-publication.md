---
title: "Bring Your Own Script and Make It Ready for Publication"
layout: event

# workshop, seminar, lecture or discussion
type: workshop

# IDs from https://codebase.helmholtz.cloud/hifis/software.hifis.net/blob/master/_data/hifis_team.yml
organizers:
  - schlauch
lecturers:
  - stoffers
  - dolling

start:
    date:   "2020-11-12"
    time:   "09:00"
end:
    date:   "2020-11-13"
    time:   "13:00"
location:
    campus:  "Online Event"
    room:   ""
registration_link: https://events.hifis.net/event/27
registration_period:
    from: "2020-10-05"
    to: "2020-11-09"
fully_booked_out: true

excerpt: "We will help you polish an existing project to a publication-ready state:\n
<ul>\n
    <li>Reviewing installation instructions and documentation</li>\n
    <li>Helping you decide on a license</li>\n
    <li>Preparing the required publication steps and more…</li>\n
</ul>\n
Any programming language is welcome!"

---

## Content

We will provide you with actionable advice about how to polish your software 
project before publishing it or submitting it alongside a publication.
This includes 
* Documentation
* Open source licensing 
* Software citations 
* Archiving and more… 

Practical exercises using either a demo project or your own will teach you
to apply the presented strategies.

## Prerequesits

Basic Git skills are required.
A good and quick tutorial can be found in the 
[Software Carpentry's "Git Novice" episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
Participants need to bring their own laptop with the project set up,
plus the Git command line or a graphical client, and a modern web browser.
