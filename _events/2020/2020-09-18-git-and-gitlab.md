---
title: Project management with GitLab
layout: event
organizers:
  - erxleben
lecturers:
  - erxleben
  - lokamani
  - Schmerler, Steve
type:   workshop
start:
    date:   "2020-09-18"
end:
    date:   "2020-09-21"
registration_link: https://events.hifis.net/event/21/
location:
    campus: "Online Event"
fully_booked_out: true
registration_period:
    from:   "2020-08-24"
    to:     "2020-09-13"
excerpt:
    "This workshop will focus on project management with the GitLab platform.
    A primer for the prerequisite version control system Git will be offered to those not yet familiar with it."
---

## Goal

The workshop is aimed at those who have to manage development projects in a research context.
It introduces a workflow and best practises for day-to-day operations 
with the aim to increase productivity, overall quality and make everyones life a bit easier.

## Content

This course is split into two parts:

1. Introduction to version control using the tool Git
2. Project Management with the Web-Platform [GitLab](https://about.gitlab.com)

Version Control is an essential building block in managing digital projects of any scale and enabling successful collaboration. 
Building on top of this, an advanced project management system provides further potential for enhanced productivity.

## Requirements
No previous knowledge in the covered topics will be required. 
Participants who are already familiar with Git may opt to skip the first day of the workshop. 
You will be prompted about this choice during the registration.

Further details can be found at the [event page].

[event page]: https://events.hifis.net/event/21/
