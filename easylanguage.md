---
title: HIFIS in easy language  
title_image: default
layout: default
excerpt:
    Information in easy language about HIFIS
lang_ref: easylanguage
---

Welcome to the website of HIFIS.  
Here you can find information in Easy Language.

## What is HIFIS?

{:.treat-as-figure}
{:.float-right}
![Tools]({% link assets/img/hifisfor/poster-1.svg %})
Lorna Schütte, CC-BY-NC-SA

HIFIS is a digital platform for the Helmholtz Association.  
We help scientists by giving them access to computers and tools.  
This includes things like:
- programs, 
- places to store files,
- and other things needed for research.  

HIFIS also gives advice and help with using these things.  
We especially help scientists make good research programs.

## The Helmholtz Cloud

HIFIS also offers Helmholtz Cloud services.  
Cloud services are services you can use on the internet.  
Before, different services were separate.  
Now they are all together in the Helmholtz Cloud.  
There are now more than 30 services available, for:
- science, 
- working together, 
- technology, 
- very fast computers, 
- and saving data. 

People can use these services after they sign in with their Helmholtz account.

{:.treat-as-figure}
{:.float-right}
![Data sovereignity]({% link assets/img/hifisfor/poster-2b.svg %})
Lorna Schütte, CC-BY-NC-SA

## Keeping data safe and available

HIFIS makes sure secret data is safe.  
We do this by keeping and working with data in Helmholtz's data centers.  
This means the data stays in Germany and can be used by future researchers.  
This helps save time for people looking for data or keeping it safe.

## Supporting Research Software development

HIFIS helps make good research programs.  
It teaches people who want to learn more about making programs.  
There are beginner and advanced courses available.  
It also gives free advice for researchers who need help with specific programs.
