# Use this makefile in Linux to build and serve the local development environment
# with one command.
# Please make sure to have Jekyll and ffmpeg installed

.PHONY: all install clean serve install-dependencies install-submodules jumbotrons

all: install jumbotrons serve

install: install-dependencies install-submodules

serve:
	bundle exec jekyll serve --future

clean:
	rm -rf assets/img/jumbotrons/desktop
	rm -rf assets/img/jumbotrons/display_2k
	rm -rf assets/img/jumbotrons/phone
	rm -rf assets/img/jumbotrons/tablet
	rm -rf .jekyll-cache
	rm -rf _site

install-dependencies: Gemfile Gemfile.lock
	bundle install

install-submodules: .gitmodules
	git submodule update --init --recursive

jumbotrons:
	bash scripts/create_jumbotrons.sh assets/img/jumbotrons/
