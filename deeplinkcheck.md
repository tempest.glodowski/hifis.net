---
title: List of deep links
title_image: default
layout: default
excerpt:
    This is a list of published deep links to HIFIS and HIFIS documentation pages.
---

## Links to hifis.net

* <https://hifis.net/doc>
* <https://hifis.net/policies>
* <https://hifis.net/team>
* <https://hifis.net/contact>
* <https://hifis.net/roadmap>
* <https://hifis.net/guidelines>
* <https://hifis.net/services/overall/tutorials>
* <https://hifis.net/services/overall/guidelines>
* <https://hifis.net/news>
* <https://hifis.net/services>
* <https://hifis.net/mission/publications>
* <https://hifis.net/partners>
* <https://hifis.net/services/cloud/Helmholtz_cloud>
* <https://hifis.net/events>
* <https://hifis.net/pr>
* <https://hifis.net/media>
* <https://hifis.net/media/HIFIS_overview_en.pdf>
* <https://hifis.net/media/HIFIS_overview_de.pdf>
* <https://hifis.net/sab>
* <https://hifis.net/spotlights>
* <https://hifis.net/survey2021>
* <https://hifis.net/documentation>
* <https://hifis.net/services/hifis-spotlight-2021>
* <https://hifis.net/consulting>


## Links to software.hifis.net

* <https://software.hifis.net>
* <https://software.hifis.net/services>
* <https://software.hifis.net/services/training>
* <https://software.hifis.net/services/helmholtzgitlab>
* <https://software.hifis.net/services/consulting>
* <https://software.hifis.net/blog>
* <https://software.hifis.net/events>



## Links to Technical documentation

* <https://hifis.net/doc/core-services/aai-proxy/>
* <https://hifis.net/doc/core-services/fts-endpoint/>
* <https://hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/#selected-services-for-initial-helmholtz-cloud-service-portfolio>
* <https://hifis.net/doc/service-integration/pilot-services/pilot-services/>
* <https://hifis.net/doc/backbone-aai/list-of-connected-centers/>

## Links to aai.helmholtz.de
_from 2022-11-22, these should redirect only_

* <https://aai.helmholtz.de>
* <https://aai.helmholtz.de/news>
* <https://aai.helmholtz.de/concept>
* <https://aai.helmholtz.de/policies>
* <https://aai.helmholtz.de/faq.html>
* <https://aai.helmholtz.de/howto>
* <https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html>

## Others

* <https://plony.helmholtz.cloud>
* <https://helmholtz.cloud>
* <https://helmholtz.software>
