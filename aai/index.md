---
title: Helmholtz Login & Helmholtz AAI
title_image: markus-spiske-PsRUMc7vilg-unsplash.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
    - image-scaling.css
excerpt:
  "Seamless Access to Cloud Services for Helmholtz & Friends."
redirect_from:
    - services/backbone/aai.md
    - helmholtz-aai
    - helmholtzaai
---

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="Sketch: Seamless access to Helmholtz Resources for Helmholtz Scientists and external partners, finely controllable by access lists"
src="{{ site.directory.images | relative_url }}/hifisfor/poster-2b.svg"
/>
</div>
<div class="text-box" markdown="1">
# What is it about?

HIFIS built, maintains and continuously expands a technical and procedural infrastructure that allows users from all Helmholtz Centres as well as external collaboration partners to **access distributed resources** — for example cloud services — in a seamless way.

To enable this, the **Helmholtz Authentication and Authorisation Infrastructure (AAI)** has been implemented.

The Helmholtz AAI:

- Enables seamless login to the Helmholtz Cloud Services and other resources for any Helmholtz users and invited partners from external institutions, by using only the home institution's username and passphrase.
- Allows cross-institutional group management and fine-grained resource sharing via so-called [Virtual Organisations (VO)](https://hifis.net/doc/helmholtz-aai/howto-vos/) Management.
- Maintains compatibility with international initiatives, such as the European Open Science Cloud EOSC by using frameworks such as the [AARC blueprint](https://aarc-community.org/architecture/).
- Allows for easy connection of [new institutions](https://hifis.net/doc/helmholtz-aai/howto-idps/) and [new services](https://hifis.net/doc/helmholtz-aai/howto-services/).
- Transfers and processes only minimal sets of personal data; for example, passwords do not leave your home institution.
- Strongly facilitates parallel usage and even interconnection of multiple services by Single-Sign On (SSO).
- Understands different identity qualities via [Levels of Assurance](https://refeds.org/assurance).

</div>
</div>
<div class="clear"></div>

### How to use it?

- [**Login and User account information**](https://login.helmholtz.de/home/): After first login, accepting all terms and conditions, you are already a member of Helmholtz AAI. Congratulations!
- [**Manage a group (VO)**](https://login.helmholtz.de/upman/): To do this, you need to be a manager of such group, for example after being invited by another manager or [registering your own VO](https://hifis.net/doc/helmholtz-aai/howto-vos/) beforehand. And you need to have a [second authentication factor registered](https://hifis.net/doc/helmholtz-aai/howto-mfa/) for security reasons.
- [**Helmholtz Cloud services**](https://helmholtz.cloud/): Check out our service portfolio that can be used after AAI login, if you are a Helmholtz member or part of a collaboration group. Don't forget to read the service descriptions, as some are usable under preconditions.
- For technical details, refer to our [**documentation**](https://hifis.net/doc/helmholtz-aai/concepts/).

## How does this work?

From user perspective, the connected [**Helmholtz Cloud services**](https://helmholtz.cloud) can be logged in by clicking  "Helmholtz Login", "Helmholtz AAI" or comparable buttons.
This ultimately allows the users to select their home institution and log in via the home institution's credentials.

Have a look at our [**illustrated tutorial on this**]({% link aai/howto.md %}) to see more of it!

If you happen to run into any issues,
check our [FAQ]({% link faq.md %}#helmholtz-aai)
and never hesitate to contact our integrated helpdesk at <support@hifis.net>.

### The technical view

All [details on the technical and procedural implementation can be found in the documentation](https://hifis.net/doc/helmholtz-aai/).

Spoken on a very high level, there are at least three instances exchanging data with each other:

- the service,
- the central community AAI (_login.helmholtz.de_), and
- the user institution's Identity Provider (IdP).

The first station itself can consist of multiple sub-instances, for example involving an [Infrastructure Proxy](https://hifis.net/doc/helmholtz-aai/concepts/#central-and-distributed-software-stacks), to streamline multiple service connections of one institution to the whole infrastructure.

As a user, you are basically following the route downwards and then up again when logging in to a service.

### Compliance

All participants, including end users, need to comply to the [Helmholtz AAI policies](https://hifis.net/doc/helmholtz-aai/policies/), and  possibly additional policies related to specific groups (VOs).
Further, you are informed about and need to agree to every transfer and processing of personal data.
Sorry for bothering you on all of these steps, but it's mandated by GDPR.
But it's handy that you can save your preferences and not be bothered again on this when you log in the next time.

Furthermore, a Helmholtz-wide rule set is currently being set up by HIFIS in order to facilitate [cross-centre sharing of Helmholtz Cloud resources]({% link services/cloud/Helmholtz_cloud.md %}) in the future.

## Comments? Suggestions? Questions?

1. Check our [FAQ]({% link faq.md %}#helmholtz-aai)
2. Contact us at <support@hifis.net>.
