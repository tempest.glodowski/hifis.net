---
title: "Call for Abstracts for TEACH 2 is open"
title_image: photo-of-open-signage-2763246.jpg
date: 2022-06-16
authors:
  - klaffki
layout: blogpost
categories:
  - News
redirect_from: news/education/announcement/event/2022/06/16/CfA_Teach-2.html
tags:
  - Education
  - Announcement
  - Event
excerpt: >
  The second installment of the TEACH event is going to take place 9 November, 2022, in an online format. The Call for Abstracts is open now.
---

## TEACH 2—Open up!
The second installment of the TEACH event is going to take place **9 November, 2022,** in an online format. 
It aims at bringing together Training Coordinators, Instructors and Trainers, Personnel Developers and Researchers. 
The participants will exchange experience and best practices, collaborate and share resources on the whole education life cycle.
For more impressions have a look at the report from last year's [TEACH conference]({% post_url 2022/2022-02-14-report-on-teach %}).

## Call for Abstracts is open
For TEACH 2022, the Call for Abstracts is already open. You can submit a proposal for a talk or discussion, a poster, an elevator pitch or a workshop. 
For details see the [submission page](https://events.hifis.net/event/312/abstracts/).

Submit your abstract till **Sunday, 3 July, 2022,** and become part of an education network!
