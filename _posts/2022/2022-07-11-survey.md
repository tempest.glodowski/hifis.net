---
title: "Helmholtz AI needs YOU for their survey!"
title_image: towfiqu-barbhuiya-oZuBNC-6E2s-unsplash.jpg
data: 2022-07-11
authors:
  - klaffki
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AI
  - Survey
excerpt: >
    We would be extremely grateful for your participation in the Helmholtz AI community survey 2022!  Helmholtz AI has put together a survey to help them determine how they can better serve you, the users.
---

## Helmholtz AI needs YOU!

In support of our sibling Incubator plattform [Helmholtz AI](https://helmholtz.ai), we forward their call to action:

WE need YOU!
We would be extremely grateful for your participation in our Helmholtz AI community survey 2022! 
We have put together a survey to help us determine how we can better serve you, the members of the Helmholtz AI community. 
Are the support structures and communication channels currently in place at the Helmholtz AI platform serving your needs, or is there something we can improve on? 
The survey should only take around 15 minutes. 
To access the survey, [simply click here](https://contrib-survey.helmholtz-muenchen.de/index.php?r=survey/index&sid=233748).
 
All answers are anonymous, so please feel free to give us your honest feedback!
 
The survey closes on **Tuesday, 16 August, 2022**.
 
If you have any questions about the survey, do not hesitate to contact Helmholtz AI at: <contact@helmholtz.ai>.
 
Thank you in advance for your participation! 
