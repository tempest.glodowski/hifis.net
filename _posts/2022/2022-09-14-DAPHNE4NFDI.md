---
title: "HIFIS supports DAPHNE4NFDI"
title_image: cytonn-photography-n95VMLxqM2I-unsplash.jpg
data: 2022-09-14
authors:
 - "Amelung, Lisa"
 - "Barty, Anton"
layout: blogpost
additional_css:
  - image-scaling.css
categories:
 - Use-Case
tags:
  - NFDI
  - Helmholtz Codebase
  - HIFIS Events
  - Sync & Share
  - Mattermost
excerpt: >
  The goal of DAPHNE4NFDI is to create a comprehensive infrastructure to process research data from large scale photon and neutron infrastructures. A project of this scale needs a reliable infrastructure to ensure communication, data exchange and publishing of the results. Therefore, DAPHNE4NFDI became the second major HIFIS user among the NFDI consortia.
---

## DAPHNE4NFDI: Data from Photon and Neutron Experiments

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="DAPHNE4NFDI logo"
src="{% link  assets/img/third-party-logos/DAPHNE4NFDI_logo.png  %}"
/>
</div>
</div>


[DAPHNE4NFDI](https://www.daphne4nfdi.de/english/index.php) (**DA**ta from **PH**oton and **N**eutron **E**xperiments for NFDI) is one of 19 consortia receiving funding as part of the [German National Research Data Infrastructure](https://www.nfdi.de/?lang=en) (NFDI). 
The aim of DAPHNE4NFDI is to create a comprehensive infrastructure to process research data from large scale photon and neutron infrastructures according to the [FAIR principles](https://www.go-fair.org/fair-principles/) (Findable, Accessible, Interoperable, Reusable).

Uniquely, DAPHNE4NFDI engages directly with the user community to develop user-driven data solutions and infrastructure for the wider photon and neutron community. 
Hence, the DAPHNE4NFDI consortium consists of experts from [Committee Research with Synchrotron Radiation](https://www.sni-portal.de/en/user-committees/committee-research-with-synchrotron-radiation) (KFS) and [Committee Research with Neutrons](https://www.sni-portal.de/en/user-committees/committee-research-with-neutrons) (KFN), experts from the different science fields and techniques at universities, research institutes and large-scale facilities, and strongly interacts also with other NFDI consortia.

## DAPHNE4NFDI becomes second major HIFIS user among NFDI consortia
A project of this scale needs a reliable infrastructure to ensure communication, data exchange and publishing of the results.
Therefore, DAPHNE4NFDI contacted HIFIS and became their second major user among the NFDI consortia after [PUNCH4NFDI]({% post_url 2022/2022-06-28-use-case-PUNCH %}).
At this point, DAPHNE4NFDI makes use of a [Virtual Organization](https://hifis.net/doc/helmholtz-aai/howto-vos/) (VO) to organize access rights for services like [Sync & Share](https://helmholtz.cloud/services/?serviceID=6dd798c4-72cc-4661-aae8-f47a1f3852ce).
Events are organized by the consortium using [HIFIS Events](https://helmholtz.cloud/services/?serviceID=ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b) and communication among project participants was made available via [Mattermost](https://helmholtz.cloud/services/?serviceID=1be91786-b7e7-4fa3-81d9-1b95dd03cd52).
Recently, DAPHNE4NFDI opened up a group in [Helmholtz Codebase](https://helmholtz.cloud/services/?serviceID=c5d1516e-ffd2-42ae-b777-e891673fcb32) to collaborate and build software within the different task areas of the project to later make it available to the community.

## Get in contact
For DAPHNE4NFDI: [Anton Barty](mailto:anton.barty@desy.de) (Speaker), DESY; [Lisa Amelung](mailto:lisa.amelung@desy.de) (Project Coordinator), DESY

For HIFIS: [HIFIS Support](mailto:support@hifis.net)
