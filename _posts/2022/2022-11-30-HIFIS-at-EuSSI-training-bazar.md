---
title: "HIFIS at the EuSSI training bazar"
title_image: adi-goldstein-mDinBvq1Sfg-unsplash_shrinked.jpg
data: 2022-11-30
authors:
  - erxleben
layout: blogpost
categories:
  - News
tags:
  - Education
  - Report
excerpt: >
    Together with the sister platforms HIDA and Helmholtz AI, HIFIS presented itself on the first EuSSI training bazar — an international event to network with European scientific software training initiatives.
---

### What is EuSSI?

The [_European Software Sustainability Institute_](https://eussi.org) is a network of organizations following the example of the [SSI in the United Kingdom](https://www.software.ac.uk/).
Its goal is to raise awareness and share methods to improve the sustainability of reseach software.

### The Training Bazar

One measure to tackle these challenges is to take a closer look at the training activities regarding scientific software development.
For this very reason, the _EuSSI_ has recently invited many European training initiatives to share their goals, approaches and experiences regarding the training for research software engineering.
Together with our colleagues from [HIDA](https://www.helmholtz-hida.de/) and [Helmholtz AI](https://www.helmholtz.ai/), we presented the Incubator workshop process.
We gave an overview over the approaches taken by the three platforms, their common aspects, as well as highlighting the individual capabilities of each.

Multiple other European initiatives and institutions also presented their respective teaching endeavours, among which were 

* [Netherlands eScience Center](https://www.esciencecenter.nl/),
* [CodeRefinery](https://coderefinery.org/),
* [HSF](https://hepsoftwarefoundation.org/) and
* [The Software Carpentries](https://software-carpentry.org/)

Following these initial presentations a lively discussion was held to share experiences, network among the organizations and identify common challenges in a bid to start pooling our ressouces and knowledge to solve those.

## Takeaway Messages

It became clear very quickly, that many approaches on the topic of teaching Research Software Engineering across Europe are relatively young, but have already gathered considerable experience.

The demand for RSE training opportunities is very high and current endeavours are not nearly sufficient to effectively cover this.
As a limiting factor the overall availability of _active_ instructors was identified.
But why are there not enough instructors to hold more classes?
Being an instructor is to often regarded as a "side quest" and often not considered a valid choice for a full-time career / job definition by individuals and organisations alike.
Coupled with non-permanent employment positions, it leads to many trained instructors who only rarely get the opportunity to apply or further develop their skills.
A strong culture shift is needed in this regard.

Due to this bottleneck in available instructors' and organizers' time (which very often are the same person), a further approach is to strive to automate as many parts around the workshop organisation as possible.
Within Helmholtz, we use [HIFIS Events](https://helmholtz.cloud/services/?serviceID=ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b) for advertising, registration and sharing information as needed software or log-in data for the participants.

With regards to training materials it is a declared goal of the participants to increase the sharing and re-useing amongst each other and to promote _Open Education Resources_.
We started to publish our HIFIS workshop materials already some time ago, which you can find along with some tutorials at the [Learning Materials]({% link services/software/training.md %}#workshop-materials).


## Questions? Comments? Suggestions?
If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
