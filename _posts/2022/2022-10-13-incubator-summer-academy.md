---
title: "Looking back at the Incubator Summer Academy"
title_image: jason-goodman-Oalh2MojUuk-unsplash.jpg
data: 2022-10-13
authors:
  - schlauch
  - huste
layout: blogpost
categories:
  - News
excerpt: >
  The five Incubator platforms Helmholtz AI, Helmholtz Imaging, HIDA, HMC and HIFIS
  have teamed up to create an exciting program at the Incubator Summer Academy.  
  In this post we take a look back at an intense two-week program.
---

{:.float-left}
{:.treat-as-figure}
![Banner image for the Incubator Summer Academy]({% link assets/img/posts/2022-05-20-save-the-date-summer-academy/Incubator_Summer_Academy_visual_broad.jpg %})
© HIDA

The [Incubator Summer School – From Zero to Hero][academy-link] took place from September 12–23, 2022.
The five Incubator platforms
[Helmholtz.AI][hai],
[Helmholtz Imaging][hip],
[HIDA][hida],
[HMC][hmc] and
HIFIS teamed up
to create an exciting program consisting of:

* Data science workshops from beginner to expert level,
* a data challenge,
* keynote speeches, and
* plenty of digital and on-site networking opportunities.

The HIFIS team was substantially involved in organising the summer school
which was largely carried out using numerous [Helmholtz Cloud][cloud] services.
In addition, we also contributed to the workshop program by offering:

* Workshops about Python basics, data analysis with Python, and data visualization using Python,
* Workshops focused on Git , GitLab and continuous integration with GitLab,
* A workshop about good practices concerning publishing research software to address reproducibility of scientific results.

Particularly, the Python basics workshop was a basis for the workshops of other Incubator platforms.
Therefore, we also scheduled additional pre summer school events to address the heavy demand.
The Incubator Summer School attracted a lot of attention throughout Helmholtz.
More than 426 persons from all Helmholtz centres and partners registered
for one or more events of the summer school.
Overall, we were able to offer 324 persons a seat in at least one event.

Building on this success, we hope to contribute to an even better Helmholtz-wide summer school in the future.

<div class="alert alert-primary">
    <h3 class="alert-heading">Are you interested in expanding your knowledge in the field of software engineering?</h3>
    <p>HIFIS can help you with getting started or in boosting your software engineering practice.</p>
    <ul>
        <li>
            <a href="https://events.hifis.net/category/4/">Future HIFIS Workshops</a>
        </li>
        <li>
            <a href="{% link services/software/consulting.html %}">Software Engineering Consulting</a>
        </li>
        <li>
            <a href="{% link services/software/training.md %}">Learning Material</a>
        </li>
    </ul>
</div>

[academy-link]: https://events.hifis.net/event/398/
[hip]: https://helmholtz-imaging.de/
[hai]: https://www.helmholtz.ai/
[hmc]: https://helmholtz-metadaten.de/
[hida]: https://www.helmholtz-hida.de/
[cloud]: https://helmholtz.cloud
