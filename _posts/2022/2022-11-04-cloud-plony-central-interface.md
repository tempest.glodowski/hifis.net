---
title: "Plony, the service database, becomes the central interface"
title_image: background-cloud.jpg
data: 2022-11-04
authors:
  - spicker
  - holz
layout: blogpost
categories:
  - News
tags:
  - Cloud Services
  - Service Review Process
  - Service Onboarding Process
  - Milestone 2022
additional_css:
  - image-scaling.css
excerpt: >
  The Service Database Plony becomes the central communication platform between providers of Helmholtz Cloud services and the HIFIS team. Helmholtz Cloud stakeholders and the HIFIS team can now handle key processes seamlessly online, including the onboarding process and the upcoming service review process.
---

## Service Descriptions are automatically transferred from Plony to the Helmholtz Cloud Portal

Since the beginning of September 2022, the HIFIS team established the interface between the Helmholtz Cloud Portal and the service database [Plony](https://plony.helmholtz.cloud/). Plony now automatically transfers the service information to be displayed in the Helmholtz Cloud Portal. This eliminates the need for manual steps and is a prerequisite for the change of service information during operation.

Plony includes all service information necessary for the operation of a Helmholtz Cloud service, which the service provider is giving during the onboarding process. [Helmholtz Cloud Portal](https://helmholtz.cloud/) publishes information of key interest to the user. Furthermore, Plony contains non-public information, e.g. technical information about Helmholtz AAI connectivity (Find more information about [Process Framework for Helmholtz Cloud Service Portfolio](https://www.hifis.net/doc/process-framework/Chapter-Overview/)).

## Service Portfolio Review Process November 2022
Having all service information available in Plony, the handling of the Service Portfolio Review Process becomes much easier. Service Providers can check whether service information is up-to-date in Plony and can directly initiate changes if necessary. This reduces formerly necessary manual steps and improves the overview during the Service Portfolio Review.

## Questions? Comments? Suggestions?
If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
