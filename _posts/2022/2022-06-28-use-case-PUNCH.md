---
title: "HIFIS supports PUNCH4NFDI (updated)"
title_image: branimir-balogovic-fAiQRv7FgE0-unsplash.jpg
data: 2022-06-28
authors:
  - "Schwarz, Kilian"
  - klaffki
layout: blogpost
categories:
  - Use-Case
redirect_from:
  - use%20case/helmholtz%20aai/sync%20&%20share/hifis%20transfer%20service/2022/06/28/use-case-PUNCH.html
  - use%20case/2022/06/28/use-case-PUNCH
tags:
  - Helmholtz AAI
  - Sync & Share
  - HIFIS Transfer Service
excerpt: >
    In August 2021, HIFIS started to support PUNCH4NFDI by adapting the Helmholtz AAI to their requirements. Since then, the HIFIS-support for PUNCH4NFDI has been growing into providing an initial set of collaborative tools which can be accessed through the AAI.
---



## PUNCH and the National Research Data Infrastructure

{:.treat-as-figure}
{:.float-left}
![colourful 0s and 1s forming a funnel]({% link assets/img/posts/2022-06-27-use-case-PUNCH/punch-logo-S.png %})

The PUNCH (**P**articles, **U**niverse, **N**u**C**lei and **H**adrons) communities represent about 9.000 scientists with a PhD in Germany, from particle, astro-, astroparticle, hadron and nuclear physics. 

Doing research data management for so many scientists sounds like an ambitious enterprise and needs ambitious tools to become true. 
To tackle this task, the consortium [PUNCH4NFDI](https://www.punch4nfdi.de) was set up within the [NFDI](https://www.nfdi.de/?lang=en) (**N**ationale **F**orschungs-**D**aten-**I**nfrastruktur, National Research Data Infrastructure) Association. 

The scope of this group is wide, as PUNCH physics addresses the fundamental constituents of matter and their interaction, as well as their role for the development of stars and galaxies and the large scale structures of the universe. 
The researchers involved are from universities, the Max Planck Society, the Leibniz Association, and the Helmholtz Association. 
Each of them has one or more institutional affiliations which come with their own accounts and services.

## Collaboration is key—but please keep it simple

In the context of research data and meta data management, PUNCH4NFDI needed to ensure effective collaboration among all these scientists. 
In order to meet this requirement, it is necessary to provide collaborative tools to make the exchange of information and communication as comfortable and easy as possible, while also being safe and trustworthy. 
One thing everyone wanted to avoid was to make all participating scientists remember a bunch of new accounts for these services and also for using the federated computing and storage infrastructure. 
The solution was to create or, better, adopt an easy to use single-sign-on framework.
As the [Helmholtz AAI]({% link aai/index.md %}) (Authorisation and Authentication Infrastructure) had already been put in place by [HIFIS](https://hifis.net), there was no need to build the PUNCH service from scratch. 
Instead, [in August 2021]({% post_url 2021/08/2021-08-20-support-punch4nfdi %}), HIFIS started to support PUNCH4NFDI by adapting the Helmholtz AAI to their requirements.

This infrastructure now allows every user to login to any PUNCH4NFDI service using the password of their home institution (or, alternatively, Github, ORCID or other social identity providers).
Since then, the HIFIS-support for PUNCH4NFDI has been growing into providing an initial set of collaborative tools which also can be accessed via the [PUNCH AAI](https://login.helmholtz.de/punch/): 
These services allow the sharing of documents and simultaneous editing via
[Sync & Share](https://helmholtz.cloud/services/?serviceID=6dd798c4-72cc-4661-aae8-f47a1f3852ce) and an easy planning of meetings or workshops, realised by DESY Indico. 
The latter is a parallel instance to [HIFIS Events](https://helmholtz.cloud/services/?serviceID=ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b) and adopted its user account management from there. 
The latest addition from the HIFIS portfolio is the [HIFIS Transfer Service]({% post_url 2021/10/2021-10-20-use-case-hts %}), allowing scientists to manage transfers of large data sets between sites.

 
Further components of the PUNCH4NFDI portfolio, and also connected to the commonly used AAI, 
are the [PUNCH Gitlab](https://gitlab-p4n.aip.de/) to enable the sharing of software, data, and documentation, 
as well as PUNCH Mattermost for effortless communication between the scientists.

The AAI single-sign-on and the PUNCH services making use of it undergo a continuous developing process in close collaboration between HIFIS experts and PUNCH4NFDI, to ensure that all current and future use cases are taken into account. 
Special focus is laid on the group management so that services can be used according to the rights provided by the user groups of the AAI. 

## Get in contact
For PUNCH4NFDI and PUNCH AAI: Kilian Schwarz, kilian.schwarz@desy.de

For HIFIS: Uwe Jandt, support@hifis.net
