---
title: "Incubator Summer Academy—From Zero to Hero"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2022-05-20
authors:
  - huste
layout: blogpost
categories:
  - News
redirect_from: news/announcement/event/incubator/2022/05/20/save-the-date-summer-academy.html
tags:
  - Announcement
  - Event
  - Incubator
excerpt: >
  The five incubator platforms Helmholtz.AI, Helmholtz Imaging, HIFIS, HIDA and HMC have teamed up to create an exciting program.
---

## Save the Date!

**Our Incubator Summer Academy "From Zero to Hero" will take place on September 12–23, 2022!**

The five platforms
[Helmholtz.AI](https://www.helmholtz.ai/),
[Helmholtz Imaging](https://www.helmholtz-imaging.de/),
HIFIS,
[HIDA](https://www.helmholtz-hida.de/) and
[HMC](https://helmholtz-metadaten.de) have teamed up to create an exciting program consisting of:

* Data science workshops from beginner to expert level,
* a data challenge,
* keynote speeches, and
* plenty of digital and on-site networking opportunities.

_More information about the program and registration will be available here soon._

{:.treat-as-figure}
![Banner image for the Incubator Summer Academy]({% link assets/img/posts/2022-05-20-save-the-date-summer-academy/Incubator_Summer_Academy_visual_broad.jpg %})
© HIDA
