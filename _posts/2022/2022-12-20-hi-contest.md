---
title: "Submit your image to win Best Scientific Image 2023!"
title_image: blue_curtains.jpg
date: 2022-12-20
authors:
 - klaffki
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - Helmholtz Imaging
  - Contest
excerpt: >
  The call for Best Scientific Image 2023 is open. Submit your image(s) for a chance to win Best Scientific Image until Wednesday, 15 March 2023.
---

### Submit your image(s) for a chance to win!

The call for Best Scientific Image 2023 is open. 
Submit your image(s) for a chance to win Best Scientific Image. 
The contest is open to anyone who works at a Helmholtz Center. 
An international jury will award valuable prizes. 
The winning images will be displayed at the Helmholtz Imaging Conference 2023 on 14–16 June 2023 in Hamburg and become part of a travelling exhibition to be showcased in Helmholtz Centers and beyond. 
In addition, twelve winning images will be published in the **Helmholtz Imaging calendar 2024**. 
Helmholtz Imaging is looking forward to your contributions! 

Submit your image(s) by **Wednesday, 15 March 2023** [here](https://awards.helmholtz-imaging.de), where you can also find more details!

### Questions? Comments? Suggestions?

If you have any feedback, don't hesitate to contact [Knut Sander](mailto:knut.sander@helmholtz-imaging.de) for Helmholtz Imaging.
