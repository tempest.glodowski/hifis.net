---
title: "Bachelor Thesis to improve Helmholtz AAI Processes"
title_image: sergi-kabrera-2xU7rYxsTiM-unsplash.jpg
data: 2023-10-26
authors:
 - "Glodowski, Tempest"
 - "Klotz, Andreas"
layout: blogpost
additional_css:
  - image-scaling.css
categories:
 - News
tags:
 - Helmholtz Cloud
 - Helmholtz AAI
 - Bachelor
excerpt: >
   The HIFIS team supervised a bachelor's thesis, aiming at the redesign and implementation of the registration process for User Groups in the Helmholtz AAI.
---

<div class="floating-boxes">
<div class="image-box align-right">

<img class="right large nonuniform"
alt="Screenshot of VO registration form in plony"
src="{% link assets/img/posts/2023-10-26-VO-registration-process/AAI-Reg-Screenshot.png %}"
/>
</div>
</div>
<div class="text-box" markdown="1"> 

# New Registration Process for Helmholtz AAI User Groups

Digital collaboration is becoming increasingly important in research. This represents 
research centres with the challenge of finding ways to strengthen the digital collaboration 
of their researchers. To be able to share digital resources, User Groups are 
used within the Helmholtz Association as a digital representation of real (research) 
groups across institutions and disciplines. Prior to usage these groups must be registered 
in the Helmholtz authentication and authorisation infrastructure ([Helmholtz AAI](https://hifis.net/aai/)).

Within the Helmholtz AAI, the User Groups are referred to as Virtual Organisations (VOs).
This registration of the User Groups is currently not automated and is therefore cumbersome.
The aim of the bachelor thesis was the redesign of the VO Registration
Process and the implementation of a user surface in the service data base [Plony](https://plony.helmholtz.cloud/). For this purpose, the process was first modelled as a UML activity diagram
and then implemented as a web application in Plony.

The thesis describes the requirements, design, and implementation of the VO Registration
Process. The focus is on the creation of the design and the implementation
of the backend and frontend. The discussion shows that a new process could be
successfully modelled and was used as a basis for the implementation. Thus, a web
application with a form for the registration of User Groups was implemented.

To complete the implementation, the next step is to realise the connection to the 
Helmholtz AAI, among other things, so that the User Groups can also be created there automatically.

## Questions? Comments? Proposals?
If you have anything to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
