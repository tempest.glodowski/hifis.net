---
title: "Looking back at the Incubator Summer Academy 2023"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2023-11-09
authors:
 - huste
layout: blogpost
additional_css:
  - image-scaling.css
categories:
 - News
tags:
 - RSE
 - Incubator Summer Academy
 - Education
 - Software
excerpt: >
   For the second time, the incubator platforms came together
   to organize a joint education and training event:
   the Incubator Summer Academy.
   We can look back on an extensive two-week program that
   is well worth repeating in 2024.
---

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="image for the Incubator Summer Academy"
src="{% link assets/img/posts/2023-06-20-save-the-date-summer-academy/summer-academy-2023.png %}"
/>
</div>
<div class="text-box" markdown="1"> 

For the second time, the incubator platforms
[Helmholtz AI](https://www.helmholtz.ai/),
[Helmholtz Imaging](https://helmholtz-imaging.de/),
[HIDA](https://www.helmholtz-hida.de/),
[HMC](https://helmholtz-metadaten.de/) and
HIFIS
came together to organize a joint education and training event:
the Incubator Summer Academy.

The joint two-week program was structured into several [course packages](https://events.hifis.net/event/858/program) covering state of the art Data Science methods and skills, as well as networking opportunities in the Summer Academy Gathertown space!
Ranging from fundamental course packages as for instance _Python_, or _Introduction to Scientific Metadata_ to advanced topics as _Machine Learning Based Image analysis_,
the program offered participants to select course packages that best suited their experience levels and interests.

In total, 225 unique participants joined the event.
In 44 hours, HIFIS instructors taught 4 course packages which were attended by 93 participants.
The participant feedback was very positive.

We can look back on an extensive two-week program that is well worth repeating in 2024.

In any case, you don't need to wait that long to participate in a HIFIS workshop.
Further HIFIS workshops are held regularly.
All information is available [here]({% link services/software/training.md %}).

</div>
