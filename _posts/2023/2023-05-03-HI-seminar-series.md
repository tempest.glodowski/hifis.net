---
title: Six Main Tasks in Image Processing
title_image: chris-montgomery-smgTvepind4-unsplash.jpg
date: 2023-05-03
authors:
  - "Kriegel, Katharina"
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Imaging
  - Announcement
  - Event
excerpt: >
    The seminar series takes place on Thursday afternoons and tackles questions in image processing.
---

<div class="floating-boxes">

<div class="image-box">
  <img class="right medium nonuniform"
         alt="colorful bubbles on black background, dates and title of the seminar series"
         style="min-width: 150px; max-width: 550px;"
         src="{% link assets/img/posts/2023-05-03-seminar-HI/hi-series.jpeg %}">
</div>

<div class="text-box" markdown="1">

# Seminar Series: Six Main Tasks in Image Processing
The virtual seminar series covers applied imaging processing, its concepts and techniques.
Topics range from reconstruction & denoising, tracking, segmentation, visualization and explainable machine learning.
The seminar will take place on **Thursdays, 14.00-15.30** via Zoom and starts on **May 4**.

# The Program

4.5.2023: The six main tasks in image processing: an overview; speaker: Prof. Philip Kollmannsberger (Heinrich-Heine-Universität Düsseldorf)

25.5.2023: Tomographic methods in medical imaging; speaker: Dr. Christoph Lerche (PET Physics Group at Forschungszentrum Jülich)

15.6.2023: Tracking of objects: from one to many; speaker: Prof. Carsten Rother (HCI, Universität Heidelberg)

29.6.2023: Machine Learning for Image Segmentation; speaker: Prof. Dagmar Kainmüller (Helmholtz Imaging, Max-Delbrück Center, Berlin)

6.7.2023: Visualizing spatial datasets; speaker:  Deborah Schmidt (Helmholtz Imaging, Max-Delbrück Center, Berlin)

27.7.2023: Explainable Machine Learning; speaker: Prof. Ullrich Köthe (Interdisciplinary Center for Scientific Computing (IWR) University of Heidelberg)

Details for the six events as well as the registration can be found [here](https://helmholtz-imaging.de/events/six-main-tasks-in-image-processing/).

The event is organized by [International Helmholtz Research School of Biophysics and Soft Matter](https://www.ihrs-biosoft.de/), [BioInterfaces International Graduate School](https://www.bif-igs.kit.edu/), [Helmholtz Information & Data Science School for Health](https://www.hidss4health.de/) and one of our partner plattforms, [Helmholtz Imaging](https://helmholtz-imaging.de/).