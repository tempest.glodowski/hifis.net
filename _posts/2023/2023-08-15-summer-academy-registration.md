---
title: "Incubator Summer Academy: Registration is open"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2023-08-15
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Event
  - Helmholtz Incubator
excerpt: >
  The registration for the Incubator Summer Academy is open now! 
---

# Registration is open now
## Put your data science skills to the next level with the Summer Academy

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="image for the Incubator Summer Academy"
src="{% link assets/img/posts/2023-06-20-save-the-date-summer-academy/summer-academy-2023.png %}"
/>
</div>
<div class="text-box" markdown="1"> 

Once more, the five platforms
[Helmholtz.AI](https://www.helmholtz.ai),
[Helmholtz Imaging](https://www.helmholtz-imaging.de/),
[HIFIS](https://www.hifis.net),
[HIDA](https://www.helmholtz-hida.de/) and
[HMC](https://helmholtz-metadaten.de) have teamed up to create an exciting program consisting of  a variety course packages covering state of the art Data Science methods and skills, as well as networking opportunities in our Summer Academy Gathertown space!  

This online event will take place on **September 18–29, 2023**.
If you want to level up your data science skills (or develop any), take a look at the [program](https://events.hifis.net/event/858/program) and [choose your course package](https://events.hifis.net/event/858/registrations/).
**The registration is now open until August 25.**

Ranging from fundamental course packages as for instance “Python”, or “Introduction to Scienctific Metadata” to advanced topics as “Machine Learning Based Image analysis”, the program offers participants to select course packages that best suit their experience levels and interests.

The Incubator Summer Academy is open to all doctoral and postdoctoral researchers in the Helmholtz Association.

## How to register
1. Log in to indico first at [HIFIS Events](https://events.hifis.net).

2. Have a look at the course packages and the timetable. Choose the course package(s) you would like to attend. Please keep in mind that some workshops require certain previous experience/knowledge. Also,  you should not pick two course packages that overlap in time.

3. If you want to register for a course package, click on the link "→ Register here ←" displayed in the overview of course packages. Alternatively, you can click on the  "Registration" button on the left hand side of this general information page to find a list with all available registration forms.

4. You will be directed to the registration form for that course package. Fill it in and click on "Apply" or "Register". 

5. You will receive a registration notice via e-mail. Please note that this is NOT yet a confirmation.

6. Please make your own calendar entries so that you do not miss the course package you registered for.

7. If you are selected as a participant for the course package, you will be notified about ten days to one week prior to the Incubator Summer Academy.

## Questions? Comments? Suggestions?
For the Summer Academy: [HIDA](mailto:hida-courses@helmholtz.de)

For HIFIS: [HIFIS Support](mailto:support@hifis.net).
