---
title: "New service: DESY provides dCache InfiniteSpace"
title_image: background-cloud.jpg
data: 2023-07-11
authors:
  - jandt
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - dCache
  - Storage
  - Cloud Services
excerpt: >
   Exa-scale storage now available as a service: Check DESY's dCache InfiniteSpace!

---

# Exa-scale storage now available as a service: Check DESY's dCache InfiniteSpace

[dCache InfiniteSpace](https://helmholtz.cloud/services/?serviceID=9b6c63a4-d26b-4ea6-b8b0-88c0be5ea610) as the 32<sup>th</sup> Helmholtz Cloud service rounds off the service portfolio: Large scale storage is now available as a service via the [Helmholtz ID]({% link aai/index.md %}) to all user groups of the [Helmholtz Cloud](https://helmholtz.cloud/).

## Mass Storage from DESY for Helmholtz

<figure>
    <img src="{% link assets/img/posts/2022-06-29-use-case-dCache/dCache-logo.svg %}" alt="Raven as logo for dCache" style="float:right;width:15%;min-width:120px;padding:5px 5px 20px 20px;">
</figure>

[dCache](https://www.dcache.org/) is an open source project which developed a system for storing and retrieving large amounts of data, providing world-wide access. 
It has been built and is further developed by Deutsches Elektronen-Synchrotron [(DESY)](https://desy.de/), the Fermi National Accelerator Laboratory [(FNAL)](http://www.fnal.gov/) and the Nordic e-Infrastructure collaboration [(NeIC)](https://neic.no/nt1/).

Thus, the system was a perfect candidate for DESY to provide mass storage Helmholtz-wide via Helmholtz Cloud. 
Actually, it has been one of the first services connected to the Helmholtz AAI in early 2020 as a demonstrator. 
Now, it has become a regular service and is provided via the Helmholtz Cloud Portal branded as [“dCache InfiniteSpace”](https://helmholtz.cloud/services/?serviceID=9b6c63a4-d26b-4ea6-b8b0-88c0be5ea610).

The service allows Helmholtz-based user groups to store, process and publish research data with practically no storage limits.
It is a perfect match to combine with data processing pipelines (service orchestration) that the HIFIS team is currently setting up in collaboration with research groups.
On top, the dCache software features advanced authorization delegation, provides a user-transparent workflow to enable buffering and pre-fetching of data transfers, thus minimizing performance losses due to latency. It also supports integration with large data transfer services such as CERN’s File Transfer Service.

## Early Adopters become regular Users

A few Helmholtz user groups have been using this service already in its prototypic phase.
For example, [Helmholtz AI](https://www.helmholtz.ai/)'s SeisBench project uses this service as  [Mass Storage for Machine Learning in Seismology]({% post_url 2022/2022-07-15-use-case-dCache %}).

Also our partners from [Helmholtz Imaging](https://helmholtz-imaging.de/projects/ongoing/) are using the storage.
We also [showcased our first implementation of a service pipeline for scientific data processing]({% post_url 2023/2023-04-18-service-orchestration %}) that will be further extended in collaboration with [Helmholtz Imaging Modalities](https://modalities.helmholtz-imaging.de/).
In this pipeline, multiple services, including the large scale storage, are chained in a modular fashion to process data in an automatable and reproducible way.

On top, the storage service is an important element in large scale data transfer, for example employing the [HIFIS transfer service]({% post_url 2021/10/2021-10-20-use-case-hts %}).

## Get in contact
For dCache InfiniteSpace or anything else HIFIS-related: [HIFIS Support](mailto:support@hifis.net)

---

## Changelog

2023-08-14:
The service can now be accessed via Helmholtz Cloud Portal, hence links in this blog post have been adjusted.
