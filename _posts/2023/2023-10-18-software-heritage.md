---
title: Codebase is now a forge in the Software Heritage project!
date: 2023-10-18
authors:
  - servan
  - ziegner
  - huste
layout: blogpost
title_image: default
categories:
  - News
tags:
  - Codebase
  - HIFIS Software
  - Sustainability
excerpt:
  Public Helmholtz Codebase repositories are archived in the Software Heritage initiative.
---

# Preserving the Helmholtz Codebase legacy in the universal software archive

Our [Helmholtz Codebase](https://codebase.helmholtz.cloud) is now part of the archived forges which are regularly harvested by the [Software Heritage Archive](https://archive.softwareheritage.org/). You can browse through the archived repositories of our forge [here](https://archive.softwareheritage.org/browse/search/?q=https%3A%2F%2Fcodebase.helmholtz.cloud&with_visit=true&with_content=true). It is more than a digital formality, it shows our dedication to preserving the history and evolution of our research software and our commitment to the open-source community.

{:.treat-as-figure}
{:.float-left}
![SH illustration]({% link /assets/img/posts/2023-10-18-software-heritage/software-heritage-api-768x429.png %})
© [Software Heritage](https://www.softwareheritage.org/2023/07/25/software-heritage-graphql-explorer-more-power-to-your-apis/)

## Software Heritage

[Software Heritage](https://www.softwareheritage.org/) aims to collect and preserve software in source code form, because "software embodies our technical and scientific knowledge and humanity cannot afford the risk of losing it". It is an open, non-profit initiative unveiled in 2016 by [Inria](https://www.inria.fr/en). It is supported by a broad panel of institutional and industry partners, in collaboration with UNESCO. HIFIS, with its strong focus on research software engineering quite naturally support their [mission](https://www.softwareheritage.org/mission/).

## What does it mean for your project?

1. That you don't need to use the ["save code now"](https://archive.softwareheritage.org/save/) function of the Software Heritage archive, as your repository - as long as it is set to "public" in GitLab - is now automatically and regularly harvested as part of the Helmholtz Codebase forge.
2. That the Step 2 in the [HOWTO archive and reference your code](https://www.softwareheritage.org/howto-archive-and-reference-your-code/) is taken care of.
3. That you can use the Persistent Identifier (SWHID) which identifies each and every source code artifact, that can also be used as a [badge](https://www.softwareheritage.org/2020/01/13/the-swh-badges-are-here/).
4. That other researchers will be able to find your repository via the Software Heritage Archive.

