---
title: "A Podcast about HIFIS Education"
title_image: matt-botsford-OKLqGsCT8qs-unsplash.jpg
date: 2023-06-29
authors:
 - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - HIFIS Education
  - Media
excerpt: >
  Code for Thought is an RSE podcast and in the latest episode, Fredo Erxleben from HIFIS Education talks about his experience as trainer.
---

<div class="floating-boxes">
<div class="image-box align-right">
<a href="https://codeforthought.buzzsprout.com/1326658/13072511-de-bringt-uns-programmieren-bei-mit-fredo-erxleben">
<img class="right medium nonuniform"
alt="Banner image for the podcast episode"
src="{% link assets/img/posts/2023-06-28-podcast-education/Ausbildung.png %}"
/>
</a>
</div>
</div>

# A Podcast about HIFIS Education
[Code for Thought](https://codeforthought.buzzsprout.com/1326658) is a community podcast for Research, (Open) Science and Engineering.
In the latest German episode "Bringt uns Programmieren bei" (teach us programming), the host Peter Schmidt interviews Fredo Erxleben from [HIFIS Education](https://hifis.net/services/software/training).
He talks about his experiences as workshop leader, teaching coding, version control and other important topics to scientists.

<i class="fas fa-headphones"></i><a href="https://codeforthought.buzzsprout.com/1326658/13072511-de-bringt-uns-programmieren-bei-mit-fredo-erxleben"> Feel free to tune in anytime! </a><i class="fas fa-headphones"></i>

## There's more to listen to!
Are you a podcast fan? Then you might also want to listen to [another German episode from 2021](https://resonator-podcast.de/2021/res172-hifis/), where Holger Klein from the [Helmholtz Resonator](https://resonator-podcast.de/) talks with Uwe Jandt (HIFIS Coordination) and Carina Haupt (HIFIS Software Services) about the platform in general.

## Get in Contact
For HIFIS Education: [HIFIS Support](mailto:support@hifis.net)