---
title: "Five new services enrich the Helmholtz Cloud (updated)"
title_image: background-cloud.jpg
date: 2023-03-27
authors:
 - holz
layout: blogpost
categories:
  - News
tags:
  - Cloud Services
excerpt: >
  The Helmholtz Cloud Service Portfolio continues to grow steadily.
---

## Five new Services online in Cloud Portal, and even more to come!

In the first quarter of 2023, lots of new services joined our Helmholtz Cloud Service Portfolio.
As LaTeX is widely used for writing in the scientific community, we added the online LaTeX editor [Collabtex](https://helmholtz.cloud/services/?serviceID=59a9d116-a897-4eac-a8f5-b810ab3838fe) in January, provided by HZDR.
But we also gained four other scientific services:
- [SciFlow](https://helmholtz.cloud/services/?serviceID=5b9113f5-e3cf-4920-b081-822a58902b0f), provided by HZB,  is an online editing tool that allows researchers to easily collaborate on scientific publications.
- [webODV](https://helmholtz.cloud/services/?serviceID=a2b9dae1-9840-4e7d-9564-de7af4704b97), provided by AWI,  is a widely used software package for the analysis, exploration and visualization of oceanographic and other environmental data. 
- [Helmholtz RSD](https://helmholtz.cloud/services/?serviceID=6f188651-a257-43fa-8878-0b7fb18d54b2), provided by GFZ,  is a software catalogue designated to Research Software that is being developed within the Helmholtz Association.
- [REMS](https://helmholtz.cloud/services/?serviceID=f0ba3101-1480-422e-bdd3-140ceacc4f01), provided by DKFZ,  allows to manage resource entitlements and access rights to research datasets.

Additionally, the three services already existing in the [Service Pipeline](https://plony.helmholtz.cloud/sc/public-service-portfolio) have been supplemented by two brand-new services, which will be available in Helmholtz Cloud soon:
- FileSender, provided by DKFZ, is a web-app for sending large files, quickly and securely.
- HubTerra Sensor Management, provided by GFZ, allows the comprehensive acquisition, administration and export of meta data of platforms, sensors and measurement configurations by stations and campaigns operated in the Helmholtz research field Earth & Environment.

## Nine Helmholtz Centres are already acting as Service Providers

With the new services in the catalogue, we proudly reach the number of nine different Helmholtz centres providing Helmholtz Cloud Services! With a total of thirty services online in Cloud Portal and overall five upcoming services we are continuously expanding the quality of our service portfolio. Thanks to our users and special thanks for your contributions!

## Changelog

- 2023-03-27: Added 5<sup>th</sup> recently added service (REMS)
- 2023-04-19: Changed some wording and updated numbers of services in Cloud Portal and Pipeline.
