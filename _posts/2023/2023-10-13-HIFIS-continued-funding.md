---
title: "HIFIS funding guaranteed until 2029"
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
date: 2023-10-13
authors:
 - servan
 - konrad
 - jandt
layout: blogpost
categories:
  - News
tags:
  - Helmholtz Incubator
  - Announcement
  - Evaluation
excerpt: >
  End of September, Helmholtz decided to continue funding the five incubator platforms until 2029!
---
# Five more years of continued funding to look forward to

There's reason to celebrate:
The Helmholtz Assembly of Members (German: Mitgliederversammlung, MV) decided to continue the funding for HIFIS and its four sister Incubator Platforms at least until 2029.
This good news came after the [extraordinarily positive feedback by an external review board that we received end of last year]({% post_url 2022/2022-12-12-evaluation-update %}).
With that, the Helmholtz MV rewarded us according to our fantastic results and commitment!

All platforms are now tasked to further elaborate on their future plans and hand in their concepts in for the next Helmholtz MV in spring 2024. So: Let's rock!
