---
title: "The April issue of our newsletter is here"
title_image: paula-hayes-Eeee5H-yuoc-unsplash.jpg
data: 2023-04-21
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Newsletter
excerpt: >
  Subscribe to our newsletter to get further issues!


---

# Find out about new services and orchestrated service pipelines
During the last months we have been busy to expand our [Helmholtz Cloud](https://helmholtz.cloud/) portfolio and added dedicated [scientific services]({% post_url 2023/2023-03-27-new-services-in-helmholtz-cloud-service-portfolio %}).

As further development, we introduce orchestrated service pipelines:
We showcase this with a [use case]({% post_url 2023/2023-04-18-service-orchestration %}) HIFIS set up together with its sister plattform [Helmholtz Imaging](https://helmholtz-imaging.de/) to build automated, distributed image processing pipelines.

We invite you to discover this and more news in our latest newsletter in [HTML]({% link newsletter/2023-04/2023-04-HIFIS-Newsletter.md %}) or as [PDF]({% link newsletter/2023-04/2023-04-HIFIS-Newsletter.pdf %}).

# Interested in our next steps?
Then don't miss our next newsletters to track the progress and perspectives of HIFIS!
**[Subscribe here](https://lists.desy.de/sympa/subscribe/hifis-newsletter)**, and spread the word and link.
Check also our newsletters [archive]({% link newsletter/index.md %}) and [blog posts]({% link posts/index.html %}).
