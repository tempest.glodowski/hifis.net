---
title: Apply for the first Helmholtz Incubator Software Award
title_image: fauzan-saari-AmhdN68wjPc-unsplash.jpg
date: 2023-07-03
authors:
 - klaffki
 - konrad
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
redirect_from:
  - softwareaward
tags:
  - HIFIS Software
  - Announcement
excerpt: >
  The Helmholtz Incubator Software Award aims to
  promote the development of professional and high-quality research software and to
  recognize the commitment to software as the basis of modern Data Science.
---

# The Helmholtz Incubator Software Award
The President of the Helmholtz Association offers a new prize, the Helmholtz Incubator Software Award.
It aims to promote the development of professional and high-quality research software and to recognize the commitment to software as the basis of modern Data Science. 
The award will put the spotlight on sustainable development and operation of research software.
It recognizes the importance of collaboration between software developers and will help making research software available for re-use.

## Call is open 
On the first of July, the call for the Helmholtz Incubator Software Award was published and sent to the Helmholtz centres. 
It will be awarded to Helmholtz scientists, engineers and teams (including PhD students) who develop outstanding software solutions. 
The prize can be awarded in up to three categories: 
**scientific originality**, **sustainability** and **newcomer**. 

## Apply now!

The call is **open until September 30<sup>th</sup>, 2023**.
More [information and details can be found in the call]({% link assets/documents/01_Call_Helmholtz_Incubator_Software_Award.pdf %}).
Don't miss the opportunity!

## Podcast

Feel free to tune in to the [podcast on this topic](https://codeforthought.buzzsprout.com/1326658/13276955-de-extra-software-preisauschreibung-bei-helmholtz-mit-dr-uwe-konrad) <i class="fas fa-headphones"></i>!

## Get in Contact

[HIFIS Support](mailto:support@hifis.net)

---

## Changelog

- 2023-08-03: Added link to podcast