---
title: "All-Hands Meeting of HIFIS"
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
date: 2023-10-19
authors:
  - jandt
  - servan
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Event
excerpt: >
  On October 11-12, we welcomed all HIFIS members and close collaborators to our first full f2f All-hands meeting at HZDR. In the light of the positive decision of the Helmholtz Assembly of Members in September to continue the platforms, we collected numerous new ideas on how we could and should develop HIFIS further.
---
# All-hands HIFIS Meeting in Dresden-Rossendorf

![Group Photo of HIFIS All-hands meeting]({% link assets/img/posts/2023-10-19-all-hands-meeting/all-hands-group.jpg %})

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right small nonuniform"
alt="Presentation of discussion results"
src="{% link assets/img/posts/2023-10-19-all-hands-meeting/boards-presentation.jpg %}"
/>
</div>
<div class="text-box" markdown="1">

On October 11-12, we welcomed all HIFIS members and close collaborators to our All-hands meeting at [HZDR](https://www.hzdr.de).
This also included members of our [scientific advisory board](https://hifis.net/sab) and [federation board](https://hifis.net/fedboard).
The meeting was actually the first in-person event comprising all HIFIS members, since all previous meetings were either virtual or centering around smaller groups.

We gathered to:

- keep all branches of HIFIS and our stakeholders updated on our progress and development directions,
- foster networking between our branches, as well as with scientific user groups,
- develop our working groups to align with the anticipated future developments of HIFIS, particularly considering the positive [decision of the Helmholtz Assembly of Members in September to continue the platforms]({% post_url 2023/2023-10-13-HIFIS-continued-funding %}).

During the meeting, we collected numerous new ideas on how we could and should develop HIFIS further in order to even better serve the needs of scientistis and to support science overall. Lively discussions, such as "No limits here: what would you wish HIFIS to be by 2025, by 2030?" proved extremely popular, as they facilitated the rapid generation of creative ideas.

## Follow-up

The initial phase of HIFIS has been completed successfully, so we are currently adapting our resources to tackle new challenges and ensure long-term sustainability of our services. The increased need for cross-cluster collaboration and overarching tasks will be reflected in the near future by adjusting working groups and corresponding meeting formats.
