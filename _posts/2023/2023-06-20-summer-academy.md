---
title: "Incubator Summer Academy > Next Level Data Science"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2023-06-20
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Event
  - Incubator
excerpt: >
  The five Incubator Platforms Helmholtz.AI, Helmholtz Imaging, HIFIS, HIDA and HMC have teamed up again to create an exciting program.
---

# Save the Date!

{:.summary}
**Our Incubator Summer Academy "Next Level Data Science" will take place on September 18–29, 2023!**

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="image for the Incubator Summer Academy"
src="{% link assets/img/posts/2023-06-20-save-the-date-summer-academy/summer-academy-2023.png %}"
/>
</div>
<div class="text-box" markdown="1"> 

Once more, the five platforms
[Helmholtz.AI](https://www.helmholtz.ai),
[Helmholtz Imaging](https://www.helmholtz-imaging.de/),
[HIFIS](https://www.hifis.net),
[HIDA](https://www.helmholtz-hida.de/) and
[HMC](https://helmholtz-metadaten.de) have teamed up to create an exciting program consisting of  a variety course packages covering state of the art Data Science methods and skills, as well as networking opportunities in our Summer Academy Gathertown space!  

Ranging from fundamental course packages as for instance “Python”, or “Introduction to Scienctific Metadata” to advanced topics as “Machine Learning Based Image analysis”, the program offers participants to select course packages that best suit their experience levels and interests.  

The Incubator Summer Academy is open to all doctoral and postdoctoral researchers in the Helmholtz Association.

**Registration will open mid of August – stay tuned, we will keep you up to date here!**

## Questions? Comments? Suggestions?
If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
