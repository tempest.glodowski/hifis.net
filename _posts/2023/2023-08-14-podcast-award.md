---
title: "A Podcast about the new Software Award"
title_image: matt-botsford-OKLqGsCT8qs-unsplash.jpg
date: 2023-08-14
authors:
 - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Media
  - Helmholtz Incubator
excerpt: >
  Code for Thought is an RSE podcast and in the latest episode, Uwe Konrad from HIFIS talks about the Helmholtz Software Award
---

<div class="floating-boxes">
<div class="image-box align-right">
<a href="https://codeforthought.buzzsprout.com/1326658/13276955-de-extra-software-preisauschreibung-bei-helmholtz-mit-dr-uwe-konrad">
<img class="right medium nonuniform"
alt="Banner image for the podcast episode"
src="{% link assets/img/posts/2023-08-14-podcast-softwareaward/HelmholtzPreis.png %}"
/>
</a>
</div>
</div>

# The Helmholtz Incubator Software Award
As frequent readers of this blog already know, [Code for Thought](https://codeforthought.buzzsprout.com/1326658) is a community podcast for Research, (Open) Science and Engineering. 
At the end of July, an extra episode was published where the host Peter Schmidt interviewed HIFIS coordinator Uwe Konrad from Helmholtz Zentrum Dresden-Rossendorf.
They talked about the [Helmholtz Incubator Software Award]({% post_url 2023/2023-07-03-software-award %}), which is offered for the first time and aims to promote the development of professional and high-quality research software.

<i class="fas fa-headphones"></i><a href="https://codeforthought.buzzsprout.com/1326658/13276955-de-extra-software-preisauschreibung-bei-helmholtz-mit-dr-uwe-konrad"> Feel free to tune in anytime! </a><i class="fas fa-headphones"></i>

## There's more to listen to!
Are you a podcast fan? Then you might also want to listen to other podcast episodes that cover HIFIS topics: 
Earlier this year, Peter Schmidt already hosted an episode with HIFIS.
In the German episode "Bringt uns Programmieren bei" (teach us programming), he interviews Fredo Erxleben from [HIFIS Software]({% link services/software/training.md %}) about his experiences as workshop leader, teaching coding, version control and other important topics to scientists.
And there is [another German episode from 2021](https://resonator-podcast.de/2021/res172-hifis/), where Holger Klein from the [Helmholtz Resonator](https://resonator-podcast.de/) talks with Uwe Jandt (HIFIS Coordination) and Carina Haupt (HIFIS Software Services) about the platform in general.

## Get in Contact
For Code for Thought: [Peter Schmidt](mailto:code4thought@proton.me)

For HIFIS: [HIFIS Support](mailto:support@hifis.net)