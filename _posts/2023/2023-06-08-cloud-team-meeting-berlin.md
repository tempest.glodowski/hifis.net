---
title: "Insights from the Cloud Team Meeting 2023"
title_image: cloud-meeting-2023-stickers-2.jpg
date: 2023-06-08
authors:
 - spicker
layout: blogpost
categories:
  - News
tags:
  - Cloud Services
excerpt: >
  In our fourth HIFIS Cloud team meeting, we identified the milestones to further develop the user experience of the Helmholtz Cloud.
---

# In our fourth HIFIS Cloud team meeting, we identified the milestones to further develop the user experience of the Helmholtz Cloud

To improve user experience (UX), we have to go beyond the concept of interconnecting individual components with specific work packages. This way of thinking proved to be very successful as it accelerated the way to set up the Helmholtz Cloud for our scientific users in a short term. Now, with the Helmholtz Cloud being increasingly used, our approach needs to be adapted: Strictly distingiushing the specific components is not sufficient anymore, as they only work as a whole. 

Within our workshop, we took time to emphasize how a user experiences the Helmholtz Cloud. We asked ourselves: Imagine you are a new Helmholtz employee and your team leader asks you to find out how to use a Helmholtz Cloud Service. For this purpose, we used the Six-thinking-hats method [1,2] to discuss the login process. This allowed us to include all relevant perspectives.

Although the individual components already function reliably, their user-friendliness and functionality can be improved. Many ideas are waiting in the backlog to be developed. The workshop results clearly showed priorities, correlations and dependencies of the issues. Also the future structure of working groups and meetings became clear.

## Comments and suggestions

Contact us anytime in English or German / Deutsch: [contact us](mailto:support@hifis.net)!

---

## References

[1] [Wikipedia: Six thinking hats](https://en.wikipedia.org/wiki/Six_Thinking_Hats) (English)  
[2] [Projekte Leicht gemacht Blog](https://projekte-leicht-gemacht.de/blog/softskills/kreativitaet/sechs-denkhuete/) (German)
