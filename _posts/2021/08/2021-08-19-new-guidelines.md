---
title: "New Guidelines for Helmholtz AAI Participants"
title_image: default
data: 2021-08-19
authors:
  - "Sander Apweiler"
  - "Marcus Hardt"
  - "Uwe Jandt"
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    We renewed the <a href="https://hifis.net/doc/helmholtz-aai/">Helmholtz AAI documentation</a> and provide guidelines and how-to pages for different participants of Helmholtz AAI:  Users, Service Providers, Group Managers, etc.
---

# Updated Helmholtz AAI Documentation with guidelines

We updated the Helmholtz AAI documentation available at [hifis.net/aai/doc]({% link aai/doc.md %}). Have a look!

Besides a fancy new theme that allows improved navigation, we also included tutorials and guidelines for numerous Helmholtz AAI participants:

* [**End Users**](https://hifis.net/doc/helmholtz-aai/howto-users/)
* **Group Managers** aka VO Managers:
    - [How to register a group/VO](https://hifis.net/doc/helmholtz-aai/howto-vos/) 
    - [How to manage your groups](https://hifis.net/doc/helmholtz-aai/howto-vo-management/)
* [**Service Providers**](https://hifis.net/doc/helmholtz-aai/howto-services/)
* [**Identity Providers**](https://hifis.net/doc/helmholtz-aai/howto-idps/)

### More infos

Further pages include information on:

* [Connected Organisations](https://hifis.net/doc/helmholtz-aai/list-of-connected-organisations/), especially Helmholtz Centres
* [Currently registered groups/VOs](https://hifis.net/doc/helmholtz-aai/list-of-vos/)
* [Consumed and provided Attributes](https://hifis.net/doc/helmholtz-aai/attributes/)
* [Assurance Information](https://hifis.net/doc/helmholtz-aai/assurance/)
* ... and more!


### Having problems? Missing anything?

If you need assistance or want to give feedback, please **contact us at <mailto:support@hifis.net>**.
