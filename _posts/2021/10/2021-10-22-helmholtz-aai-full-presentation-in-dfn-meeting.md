---
title: "Helmholtz AAI full presentation in 75. DFN Betriebstagung"
title_image: default
data: 2021-10-22
authors:
  - jandt
  - "Marcus Hardt"
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    After a brief pitch in the previous meeting, Helmholtz AAI is being presented in full breadth in the 75. DFN Betriebstagung.
---

# Helmholtz AAI in DFN

After a brief pitch in the previous meeting, Helmholtz AAI is being presented in full breadth in the 75. DFN Betriebstagung. {% comment %} The 75. BT is no longer online, see https://www.dfn.de/news/veranstaltungen/?currentCategory=tagung {% endcomment %}

* The [presentation slides can be viewed here](https://marcus.hardt-it.de/2110-Helmholtz-AAI-DFN).
* Sources for the same are accessible at the [Helmholtz Gitlab](https://codebase.helmholtz.cloud/helmholtz-aai/presentations/-/tree/master/2110-Helmholtz-AAI-DFN).

#### Details

The presentation intends to spark an open discussion on its specialty of introducing a proxy component, allowing better scalability in federations with numerous participants.

A further point of interest is the future perspective of Helmholtz AAI and its interconnection to 
[AARC](https://aarc-project.eu/),
[DFN](https://www.dfn.de/en/),
and federations
([NFDI](https://www.nfdi.de/),
[EGI](https://www.egi.eu),
[EUDAT](https://eudat.eu),
[EOSC](https://eosc-portal.eu/),
[GÉANT](https://www.geant.org/), ...).

#### Links

Find more detailed information on:

* [Helmholtz AAI documentation](https://hifis.net/doc/helmholtz-aai/)
* [Helmholtz AAI usage statistics](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/file/plots/202102_Backbone/plot.png?job=plot_general)
* [Overview on connected services](https://hifis.net/doc/cloud-services/list-of-services/) on different integration levels
