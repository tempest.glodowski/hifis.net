---
title: "Helmholtz AAI login to Helmholtz Cloud Services"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2021-06-23
authors:
  - "Uwe Jandt"
layout: blogpost
categories:
  - Tutorial
tags:
  - Helmholtz AAI
excerpt: >
    Pictured tutorial on usage of Helmholtz AAI login and registration procedures for Helmholtz Cloud Services, using the HIFIS Events Management Platform at <a href="https://events.hifis.net">events.hifis.net</a> as an example.
---

# Login to Helmholtz Cloud Services via Helmholtz AAI

Helmholtz AAI allows seamless access to a large and growing number of Cloud Services,
many of them accessible via the [Helmholtz Cloud Portal](https://helmholtz.cloud/services/) and
supported by [HIFIS]({% link index.md %}).

#### Exemplary Helmholtz Cloud Service: Events Management Platform

As a popular example, the HIFIS [Event Management Platform](https://events.hifis.net/) has been
[made available]({% post_url 2021/04/2021-04-27-announce-hifis-event-platform %})
for all Helmholtz and collaboration partners.
This Tutorial describes the login process for this exemplary service, however, all other services function analogously.

### Easy login via your username at your home institute

Like for all Helmholtz Cloud Services, you can **use your home institution's login** to use the service seamlessly.
This is managed via the [Helmholtz AAI]({% link aai/index.md %}) (Authentication and Authorisation Infrastructure).

#### Click-by-Click: Login to the service via Helmholtz AAI

<i class="fas fa-lightbulb"></i> _Click to open screenshots for each step._

<details class="my-2">
<summary><strong>Open the service</strong> &mdash; in this example the HIFIS Event Management Platform &mdash; in your browser.
Do so by <a href="https://helmholtz.cloud/services/?filterSearchInput=hifis%20events&serviceDetails=svc-ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b">locating it in the Helmholtz Cloud Portal</a> and clicking on "Go to service", or
direct access in the browser via <a href="https://events.hifis.net">https://events.hifis.net</a>.</summary>
<div markdown=1>
{:.treat-as-figure}
![Cloud portal link to service]({% link assets/img/how-to-aai/events.hifis.net/0_cloud_portal.png %})
</div>
</details>

<details class="my-2">
<summary>Click on <strong>Login</strong> at the top right.</summary>
<div markdown=1>
{:.treat-as-figure}
![Login button on events.hifis.net]({% link assets/img/how-to-aai/events.hifis.net/1_events_login.png %})
</div>
</details>

<details class="my-2">
<summary>In some cases, you need to additionally click on <strong>Helmholtz AAI</strong>.
<i>This step is mostly skipped.</i>
</summary>
<div markdown=1>
{:.treat-as-figure}
![Keycloak login page, click on Helmholtz AAI]({% link assets/img/how-to-aai/events.hifis.net/2_keycloak.png %})
</div>
</details>

<details class="my-2">
<summary><strong>Select your home institution</strong>, i.e. the institution where you already have your home account.
You can scroll down or use filtering, e.g. <i>helmholtz</i> or <i>desy</i>.
The example is about DESY.
You have to find your own Helmholtz Centre or institution.
</summary>
<div markdown=1>
{:.treat-as-figure}
![Choice of institutions in Unity]({% link assets/img/how-to-aai/events.hifis.net/3_unity_institutions.png %})
</div>
</details>

<details class="my-2">
<summary>Can't find your institution or it is not supported?
As a fallback, a <strong>login from ORCID, Github or Google</strong> can be performed.
Select it accordingly.
</summary>
<div markdown=1>
{:.treat-as-figure}
![Choice of institutions in Unity]({% link assets/img/how-to-aai/events.hifis.net/3_unity_orcid.png %})
</div>
</details>

<details class="my-2">
<summary>Provide your <strong>username, passphrase</strong> of your home institution. There is no new password or username required.</summary>
<div markdown=1>
{:.treat-as-figure}
![Login at home IdP]({% link assets/img/how-to-aai/events.hifis.net/4_home_idp.png %})
</div>
</details>

<details class="my-2">
<summary>Accept data transmissions from your home institution's identity provider (IdP) to Helmholtz AAI, and further to events.hifis.net.</summary>
<div markdown=1>
{:.treat-as-figure}
![Consent collection]({% link assets/img/how-to-aai/events.hifis.net/5a_consents_collected.png %})
</div>
</details>

<details class="my-2">
<summary>Only if you log in the first time &mdash; <strong>Register at Helmholtz AAI</strong>.</summary>
<div markdown=1>
* Click on **Register**.
* Please check your name and mail address.
* Read and accept the AAI usage policies.   
    You may choose to remember your decision, so you won't be bothered again.  
    If you later decide otherwise, you can revoke consent and delete stored data [here](https://login.helmholtz.de/home/).
* Click on **Submit**.

{:.treat-as-figure}
![Unity Registration]({% link assets/img/how-to-aai/events.hifis.net/5b_unity_registration.png %})
</div>
</details>

<details class="my-2">
<summary>Continue with normal log-in and enjoy access to the Helmholtz Cloud Service(s)!</summary>
<div markdown=1>
{:.treat-as-figure}
![Successfully logged in to events.hifis.net]({% link assets/img/how-to-aai/events.hifis.net/6_events_logged_in.png %})
</div>
</details>


### Having problems with login to a Cloud Service?

If you have persisting problems, please **contact us at <support@hifis.net>**, providing:
* The exact service (and possibly sub-page) you want to access,
* The exact error your receive,
* The URL / address of the error page,
* The time point of your (failed) login attempt.

---

### Changelog

- 2022-12-13 Removed some outdated or irrelevant information, some formulation fixes.