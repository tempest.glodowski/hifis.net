---
title: "Helmholtz AAI in 74. DFN Betriebstagung"
title_image: default
data: 2021-03-23
authors:
  - jandt
  - "Marcus Hardt"
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    Helmholtz AAI is briefly pitched in the 74. DFN Betriebstagung.
---

# DFN Betriebstagung

Helmholtz AAI is being briefly presented at the **74. DFN Betriebstagung**.

Click [here](http://marcus.hardt-it.de/2103-Helmholtz-AAI) for the presentation slides.

### Future venue

In fall 2021, Helmholtz AAI will be presented in longer form at the upcoming DFN venue. Don't miss that opportunity to learn more about it!

### Details

See here for some detailed information as mentioned in the presentation.

* AAI usage statistics: <https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/master/gallery/gallery_plots/aai.md>
* Pilot Services: <https://hifis.net/doc/cloud-services/>
* Supported Virtual Organisations: <https://hifis.net/doc/helmholtz-aai/list-of-vos/>
* Connected Helmholtz Centres: <https://hifis.net/doc/helmholtz-aai/list-of-connected-organisations/>
* Documentation: <https://hifis.net/doc/helmholtz-aai/>

---

## Changelog

- 2023-10-16: Updated Links.
