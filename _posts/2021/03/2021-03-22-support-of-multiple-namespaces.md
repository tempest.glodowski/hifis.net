---
title: "Support of multiple namespaces in entitlement"
title_image: default
data: 2021-03-22
authors:
  - jandt
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    The Helmholtz AAI supports the usage of multiple namespaces in entitlements. For now, VO membership information can be created using different namespaces.

---

# Multiple namespaces

The Helmholtz AAI supports the usage of multiple namespaces in entitlements. For now, VO membership information can be created using different namespaces.

For details, see documentation in <https://hifis.net/doc/backbone-aai/urn-registry/>
