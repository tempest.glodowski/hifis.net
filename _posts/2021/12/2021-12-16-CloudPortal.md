---
title: "20 Helmholtz Cloud Services"
title_image: background-cloud.jpg
date: 2021-12-16
authors:
  - spicker
layout: blogpost
categories:
  - News
excerpt: >
  In the last few weeks, 10 new services were published at Helmholtz Cloud Portal. 

---

### 20 Helmholtz Cloud Services available
In the recent weeks, 10 additional services were brought online. With this increase, the number of services available has more than doubled over the course of the year.

The complete Helmholtz Cloud Service Catalogue in the [Helmholtz Cloud Portal](https://helmholtz.cloud/services/) now comprises a total of 20 services provided by seven Helmholtz Centres. A broad spectrum of service categories around the entire scientific process is covered.   

#### 10 new Helmholtz Cloud Services
* Compute Projects by Jülich
* HAICORE by Jülich
* HIFIS Events by DESY
* JSC Data Projects by Jülich 
* JupyterHub by DESY
* Jupyter on HAICORE by KIT
* LimeSurvey by DKFZ 
* LimeSurvey by HMGU
* notes@DESY by DESY
* Singularity by Jülich

### Upcoming Services
The portfolio is continuously being updated. Next upcoming services will presumably be:

* Rancher managed Kubernetes by DESY
* Redmine by HMGU
* Singularity (on HAICORE) by KIT

#### Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>



