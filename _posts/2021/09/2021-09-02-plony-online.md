---
title: "Plony enables online application for new Cloud Services"
title_image: clark-tibbs-oqStl2L5oxI-unsplash.jpg
date: 2021-09-02
authors:
  - holz
  - spicker
layout: blogpost
categories:
  - News
excerpt: >
    We are happy to announce that the Application Form for potential Helmholtz Cloud Services is now online available in Plony!

---

{{ page.excerpt }}
The application of a new cloud service is the first step of the entire Helmholtz cloud service portfolio management. Plony will be extended continuously to cover further process steps. Check out at [**plony.helmholtz.cloud** <i class="fas fa-external-link-alt"></i>](https://plony.helmholtz.cloud).

## Register your Services for Helmholtz Cloud

This means that from now on, as a service provicer you can hand in your services for Helmholtz Cloud at any time!
You find an instruction on the Application Form on the landing page of Plony.

#### Features of Plony in general and Application Form specifically:

* Connected to Helmholtz AAI – simply login with the credentials of your home institution.
* Flexible in use – you can start filling your Application Form, save it and continue whenever you want without loosing information already typed in.
* Guiding through selection criteria – exclusion criteria are built into the Application form and validation functionality allows you to check whether you fulfill all required criteria.
* Transparency in every status – using the overview “My Services” you can see all your applications sorted by status. To improve transparency about applications within your Helmholtz center, you can even see sent applications filled out by colleagues from your Helmholtz center.

#### As soon as you sent your application to HIFIS, we will check it and give you feedback:

* Either there are some questions/things need to be clarified and we’re giving the application back to you, or
* everything is fine and your service is added to the Helmholtz Cloud Service Portfolio.
  In this case we will ask you to give us some more information by filling out the service canvas.

#### Next Steps

We are planning to implement the service canvas in Plony next – as soon as it is available, you can give all information on your service via Plony.

## Comments and Suggestions

If you have questions need assistance, please do not hesitate to contact.

Thank you in advance for handing in your applications and thereby extending our Helmholtz Cloud Service Portfolio!

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>
