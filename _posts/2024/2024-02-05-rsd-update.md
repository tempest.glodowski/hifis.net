---
title: "Helmholtz RSD improved"
title_image: pexels-cottonbro-5483071.jpg
date: 2024-02-05
authors:
 - meessen
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - Community
  - Cloud Services
  - Helmholtz RSD
excerpt: >
  The Helmholtz RSD has received an update with new exciting features.
---

The [Helmholtz RSD](https://helmholtz.software) has received a major update to and now provides some new exciting features.

### Citation scraping 

The latest update provides the long awaited feature that will help software maintainers to effortlessly manage the mentions of their software entry in the Helmholtz RSD. Based on **reference papers**, citations can now be efficiently added to a software entry. The user adds DOIs from publications that are relevant to the software (e.g. method papers) via the **Reference papers** pane in the software settings:

![RSD Reference papers]({% link assets/img/posts/2024-02-05-RSD/reference_papers.png %})

Using these DOIs, the Helmholtz RSD regularly collects citations form [OpenAlex.org](https://openalex.org) and automatically adds them to the list of mentions.

### HIFIS Software Spotlights

![HIFIS Software Spotlights in the RSD]({% link assets/img/posts/2024-02-05-RSD/spotlights.png %})

The [HIFIS Software Spotlights](https://hifis.net/spotlights) were the first software entries to be visible in the RSD. The HIFIS Spotlights do now have their representation in the Helmholtz RSD and appear at the top of the Software grid.

### ORCID coupling and public profile pages

Helmholtz RSD users can now **link their ORCID** to their RSD user account. This new feature opens up the possibility to create public profile pages, providing a comprehensive showcase of the user's software entries and projects where they have contributed.

### Feedback

Are you missing features or discovered a bug, or need help with the Helmholtz RSD in general? Please contact us via [support@hifis.net](mailto:support@hifis.net?subject=[RSD]).