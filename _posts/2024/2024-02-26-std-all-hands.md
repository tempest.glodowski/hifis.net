---
title: "HIFIS All-Hands Meeting 2024 in DLR Köln"
title_image: adi-goldstein-mDinBvq1Sfg-unsplash.jpg
date: 2024-02-26
authors:
  - servan
layout: blogpost
categories:
  - News
tags:
  - Event
excerpt: >
  Save the date for the next all-hands meeting of HIFIS in DLR Köln!
---
{:.float-right}
![illustration]({% link assets/img/illustrations/undraw_time_management_re_tk5w.svg %})

# Save the date for the next all-hands meeting of HIFIS!

We invite you to __mark your calendars__ for the next HIFIS all-hands meeting, scheduled to take place at [DLR Cologne](https://www.dlr.de/de/das-dlr/standorte-und-bueros/koeln/anreise-und-lage) on September 12-13, 2024. More details, including registration and the event programme, will be available soon on the [__event page__](https://events.hifis.net/event/1219/).

Prepare to learn everything you wish to know about __HIFIS 2.0__ and explore the latest developments in the different clusters. Be sure to pack your HIFIS hoodies, clear up your ideas for the brainstorming sessions and practice drinking in small glasses.

Following [our last gathering at HZDR]({% post_url 2023/2023-10-19-all-hands-meeting %}), __we look forward to seeing you again in Cologne__.

<i class= "fas fa-lightbulb"></i> Should you have any suggestion or query for the meeting, please contact us at [HIFIS Support](mailto:support@hifis.de).
