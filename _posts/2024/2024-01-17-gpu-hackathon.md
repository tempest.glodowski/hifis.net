---
title: "Helmholtz GPU Hackathon, Deadline: Jan 31"
title_image: pexels-cottonbro-5483071.jpg
date: 2024-01-17
authors:
  - Juckeland, Guido
  - jandt
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
excerpt: >
  Helmholtz GPU Hackathon, Apr. 23-26 @ CASUS, Görlitz (Submission deadline: Jan 31)
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right large nonuniform"
    alt="GPU Hackathon"
    src="{% link /assets/img/posts/2024-01-17-gpu-hackathon/GPU Hackathon.jpg %}"
    />
  </div>
</div>

# Helmholtz GPU Hackathon

CASUS, Helmholtz Zentrum Dresden-Rossendorf (HZDR), Forschungszentrum Jülich (FZJ), and the Helmholtz Information & Data Science Academy (HIDA) together with NVIDIA and OpenACC will co-organize the Helmholtz GPU Hackathon 2024.

The event will be hosted by **CASUS in Görlitz April 23 through 26, 2024** with a virtual kick-off on April 15, 2024 (and a brief virtual get together on Apr 8).

GPU Hackathons provide exciting opportunities for scientists to accelerate their large scale AI or HPC research under the guidance of expert mentors from national laboratories, universities and industry leaders in a collaborative environment.
  
## What happens at such a GPU Hackathon?  
  
Teams of about 3-6 people apply to work on porting (parts of) their code onto the GPU or refining an already existing GPU implementation. This can be classic simulation or also large scale AI models.
Teams can use almost any programming model to do so, provided we find mentors for support.
Each team will receive two mentors to help in the process. All of this is free of charge (but you have to cover your team’s travel expenses to Görlitz). We will be using the Jülich HPC Systems (JURECA, JUWELS Booster or even an early access system for JUPITER - the upcoming exascale system at Jülich Supercomputing Center (JSC)).
A maximum of eight teams can be accepted for the event.
  
  
## Prerequisites and how to apply?  
  
- Teams are expected to be fluent with the code or project they bring to the event and motivated to make progress during the hackathon.   
- A minimum of 3 team members must participate throughout the entire event. Lack of complete participation will result in the entire team being excused.  
- Projects brought to the event are required to have a license attached and detailed in the application. For more information on why licenses are important and how to obtain one, please use following links:
    - <https://tldrlegal.com/>
    - <https://choosealicense.com/>
  
**Deadline for the team application is January 31, 2024!**

Please [**register here**](https://www.openhackathons.org/s/siteevent/a0C5e000008ncDFEAY/se000223).
  
In case of questions please do not hesitate to contact <support@hifis.net>.
