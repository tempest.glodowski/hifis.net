---
title: "Best Scientific Image Contest 2024: submit your image"
title_image: BSIC-2024-Teaser-image_3500x1969px_web.jpg
date: 2024-02-13
authors:
 - Kriegel, Katharina
 - servan
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - Helmholtz Imaging
  - Contest
excerpt: >
  Helmholtz Imaging is calling for the Best Scientific Image 2024. Submit your image and get a prize!
---

# Best Scientific Image Contest 2024: Submit your image by 23 February 2024

Helmholtz Imaging is calling for the Best Scientific Image 2024. __Submit your captivating scientific image(s) by 23 February 2024__ for a chance to __win Helmholtz Imaging Best Scientific Image 2024__, and valuable __prizes totaling 5000 EUR__!

{:.float-right}
![BSIC 2024 teaser image]({% link /assets/img/posts/2024-02-13-imaging-contest/BSIC-2024-Teaser-image_920x690px_web.jpg %})

The [image competitions in 2021, 2022, and 2023](https://helmholtz-imaging.de/best-scientific-image-gallery/best-scientific-image-contest-2023/) were a huge success, and winning images travelled across Germany highlighting the remarkable portfolio of the Helmholtz Association in imaging and the contribution of imaging science in addressing major societal challenges.

__Images eligible for the competition can be:__
- The outcome of any imaging technique developed or applied at Helmholtz Centers.
- Artistic compositions resulting from the combination of various images.
- Images need to be accompanied by a clear, informative and engaging description, providing context on the scientific significance, uniqueness, and origin of the image.

Participation is open to all individuals working at a Helmholtz Center.

A distinguished international jury will evaluate entries based on scientific merit, originality, and the artistic or visual impact of the image.

The winning images will be prominently featured at the [Helmholtz Imaging Conference 2024](https://helmholtz-imaging.de/news/helmholtz-imaging-conference-2024/) on 14-15 May in Heidelberg, and shared with the public, media, and scientific institutions. Additionally, 12 selected images will be showcased in the Helmholtz Imaging calendar 2025, and the top 20 images will participate in a traveling exhibition displayed in various Helmholtz Centers and beyond.

Aside from the __Jury Award__ your images have two additional opportunities to be recognized:
- __Public Choice Award__: The entire imaging community will vote for their favorite image.
- __Participants Choice Award__: Participants of the Helmholtz Imaging Conference will choose their preferred image.

Save the date for the award ceremony at the Helmholtz Imaging Conference 2024 on 14-15 May & join Helmholtz Imaging at their 4th annual conference.

__Get your images ready & submit them via__ [__Helmholtz Imaging’s contest portal__](https://awards.helmholtz-imaging.de/).

[![BSIC 2024 announcement collage]({% link /assets/img/posts/2024-02-13-imaging-contest/BSIC-2024_announcement_collage_1360x680px_web.jpg %})](https://awards.helmholtz-imaging.de/)