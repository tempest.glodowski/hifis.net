---
title: "Service spotlight: Resource Entitlement Management (REMS) by DKFZ"
title_image: jonny-gios-SqjhKY9877M-unsplash.jpg
data: 2024-02-22
authors:
 - Florian Döring
 - Anchal Kamra
layout: blogpost
categories:
 - News
tags:
 - Helmholtz Cloud
 - Database
 - Health
 - Science
excerpt: >
   Resource Entitlement Management System (REMS) is an open source tool for managing access rights to different kinds of resources. REMS enables users to apply for access rights easily and offers a secure way for the data owners to manage access to their data.
---

{:.treat-as-figure}
{:.float-right}
![illustration]({% link /assets/img/illustrations/undraw_online_information_4ui6.svg %})

# Manage Access to Research Datasets: REMS briefly

*Resource Entitlement Management System* (REMS) is an open source tool for managing access rights to different kinds of resources. REMS enables users to apply for access rights easily and offers a secure way for the data owners to manage access to their data.

The resources in REMS do not necessarily have to be in electronic data sets. It is possible to manage access to basically anything as long as it is identified by an identifier. This includes for example research datasets and biological samples.

REMS is a service developed by [CSC – IT Center for Science Ltd.](https://www.csc.fi/en/home) and [Nitor](https://www.nitor.com/en).

## Proven DKFZ software solutions for Helmholtz support the research process

It is not always possible to share data publicly. Sometimes data can contain sensitive personal data that restricts its use (e.g., due to data subject’s consent). However, sensitive data is also very important for research, which is why accessing it needs to be easy but secure. Thus, it is advisable to use a system like REMS to assure a process to grant controlled access.

With REMS, the DKFZ provides a Helmholtz Cloud Service to streamline the handling of Data Access Requests for research data hosted at archives, such as [EGA](https://ega-archive.org/) and [GHGA](https://www.ghga.de/). REMS plays an essential role in tracking the status of requests and generating reports. Since early 2023, it is usable across the Helmholtz Association and provided [via the Helmholtz Cloud Portal](https://helmholtz.cloud/services/?serviceID=f0ba3101-1480-422e-bdd3-140ceacc4f01).

## Who can use REMS

REMS is useful for individual researchers who want to get access to certain resources, as well as service providers who want to manage access to their material.

### Benefits for the resource owner

* **A dynamic way to handle applications and manage access rights**: For example, the data owner can define, how involved they want to be in the approval process, and how the process proceeds.
* **An audit trail of all committed actions and enhanced reporting tools to improve application traceability**: One can view the entire processing history of the application.
* **Customisable application process**: The application forms can be created and modified individually.
* **Automation**: Some functions can be automated to reduce unnecessary workload, such as approving and rejecting applications.

Applicants can log in to REMS using [Helmholtz Login](https://www.hifis.net/aai/), fill in the data access application and agree to the dataset's terms of use. The REMS system then circulates the application to the resource owner or designated representative for approval. REMS also produces the necessary reports on the applications and the granted data access rights.

### The three roles in REMS explained

* **Resource applicant**: is allowed to use the service and to apply for resources directly after the first login. This is the standard role for all REMS users.
* **Application handler**: manages the access to resources for resource applicants. This role is assigned to users by organization administrators.
* **Organization administrator**: To assign this role to users, please get in contact with us.


## Get in contact
If you want to start using REMS to manage access rights to your resources, contact us at [support@hifis.net](mailto:support@hifis.net?subject=%5Bdkfz-rems%5D).
