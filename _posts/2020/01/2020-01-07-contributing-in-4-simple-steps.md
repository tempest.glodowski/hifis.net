---
title: Contributing in 4 Simple Steps
date: 2020-01-07
authors:
  - erxleben
layout: blogpost
title_image: default
categories:
  - Tutorial
tags:
  - example
  - markdown
excerpt:
  Want to contribute? No time to read long tutorials?
  Then this contribution quickstart guide is for <em>you</em>.
additional_css:
  - 2020-01-07-contributing-in-4-simple-steps/button-link.css 
  - image-scaling.css
---

# What This Guide will (not) Cover

This tutorial will teach you a quick way to get a basic blog post set up and
published on [hifis.net](https://hifis.net).
If you plan to write something bigger that might take several days to write up
and refine, [Christian's Guide][ref1] is the better choice for you.

The guide will not go into the details of fancy formatting and styling,
however you might want to check the [Template Blog Post][ref2] for
examples of common blogging tasks.
Additionally, the [Kramdown Quick Reference][ref3] may prove helpful.

_Okay, enough chit-chat, let's write a blog post!_

# Step 1: Create an Issue

By opening an issue you let the core team know what you are planning
(which is a nice thing to do).
It helps to gather the important information right from the start.

{: .button-link}
[Open a new issue on the HZDR Gitlab now][issue-link]

Fill out the information as requested in the

```
>>>
  Quotation blocks
>>>
```

There is no need to set anything else, so you may now click the `Submit Issue`
button.
You will get redirected to the **issue page** that was just set up.
Here you can lead a discussion with other contributors and the core team about
your ideas or ask questions you have beforehand.
It might be a good idea to bookmark it for later reference.
Also note that your new issue has been assigned a unique **issue number** to
identify it.

On this **issue page** you will find the `Create merge request` button.
Click it to continue to the next step.

# Step 2: Set up the Basis

_Gitlab_ now has set up a **merge request page** for you and — even more
important — created a new _branch_ for your issue.

> If you are not familiar with _git_ think of a _branch_ like your own version
> of the project, where you can do whatever you like without worrying about
> breaking anything on the live system.

To avoid chaos and confusion, specific ordering and naming conventions have been
established, dictating which files go into which folders.

For that purpose you can click the `Open in Web IDE` button on the
**merge request page**.
This _Web IDE_ is like a small file browser and editor in one and all you need
for the rest of the tutorial.

## Everything in the Right Place

Your blog post must be put into the folder `/_posts/YYYY/MM/` where `YYYY`
is the 4-digit year, `MM` is the 2-digit month (and later we need `DD` for the
2-digit day).
If this path does not fully exist, you should now create it.

<div class="floating-boxes">
<div class="image-box align-left">
    <img class="left" style="min-width: 300px; max-width: 300px;" src="{% link assets/img/posts/2020-01-07-contributing-in-4-simple-steps/create_folder.png %}" alt="Dropdown Menu example" />
</div>
<div class="text-box" markdown="1">
The dropdown-menu to create a new sub-folder becomes visible on hovering the
parent folder.

This is the folder in which your blog-post _per se_ will be located.
If you want to include images, formulas or external resources a bit more has to
be done.
Please either refer to the [Blog post template][ref2] or ask
`@software-hifis-net` for help in a comment on the **merge request page**.

## Files

Within your (probably newly created) folder you now create a file with your blog
post.
The file must be named after the pattern `YYYY-MM-DD-title-slug.md` where
`YYYY`, `MM`, `DD` is the same as above and the `title-slug` is — surprise — a
slugified version of your blog post title.

> If you do not know, what _slugification_ is, do not despair: There is an
> [online generator][slug-generator] that can do that for you.

For comparison, the full path of this blog post looks like this:

```
/_posts/2020/01/2020-01-07-contributing-in-4-simple-steps.md
```
</div>
</div>
<div class="clear"></div>

## Frontmatter

When you select your newly created file in the _Web IDE_ it will instantly open.
Copy the following block into the first lines and replace the parts in brackets.
The lines with a leading `#` are comments and can safely be removed.

```yml
---
title:  <Your post title as it appears on the website>
date:   <Desired shown publication date as YYYY-MM-DD>
authors:
  - <Your team member ID (usually the last name in lower case)>
# - <Repeat for each author>
layout: blogpost
title_image: default
categories:
  - <choose one of "blog", "announcement", "tutorial", "report">
# You can come up with a new category if none of the above fits,
# but please consult the team beforehand
tags:
  - <Short free text>
# - <Repeat for each tag>
excerpt:
  <One or two lines that serve as a preview text in the blog post overview>
---
```

This chunk of text is called the _frontmatter_ and helps our website backend to
recognize the following text as a blog post and generate all the nice things
around it.

# Step 3: Add Content

Leave an empty line behind the _frontmatter_ and then write to your heart's
content.
While the _gitlab_ _Web IDE_ only highlights the vanilla _Markdown_ syntax, our
blog system actually understands the far mightier _Kramdown_ dialect, so you can
use everything in [The Kramdown Quick Reference][ref3].

Once you started to make changes the `Commit` button appears.
If you click it, the view changes:

* The _Unstaged Changes_ and _Staged Changes_ appear on the left side
* A comparison view occupies the center and right side

{: .float-left}
{% include video.html
    path="2020-01-07-contributing-in-4-simple-steps/commit_demo.webm"
    caption="Making a <em>commit</em> on the WebIDE.<br/>
    Note the two clicks on the <em>commit</em> button."
%}

1. Your file appears in the _unstaged changes_.
2. Double click it to move it to _staged changes_
(Double clicking it again will move it back to _unstaged changes_).
3. Click `Commit` once more and enter a meaningful message what your last
changes were.
4. Click `Commit` a final time.

Your changes are now stored as a _commit_ (think of it as a permanent checkpoint) on your _branch_ in the _repository_.

### Side Notes

> If you want to continue editing (or abort making a commit), you can switch
> back to the _editing view_ with the button on the far left side under the
> project logo.

> The whole _staging changes_ mechanic seems a bit excessive if there is only
> one file involved, but becomes a boon in more complex scenarios.
> You can abbreviate the process a bit by leaving out step 2.
> The button for step 3 then is called `Stage & Commit`.

> Please end your document with one(!) empty line.
> It will not show up in the final result, but makes a lot of tools involved in
> the review process very happy.

# Step 4: Get it Reviewed

So you finished your work, and now it is time for it to be presented to the
whole wide world.

But wait! Have you missed anything? Is everything as it should be?

To make sure, it is customary to have your blog post reviewed beforehand.
You can start by going to the **merge request page**.

The super-fast way of triggering the process is to simply leave the
following magic comment:

```
@software-hifis-net: Review please.
/label ~"Progress::Can review"
/assign @software-hifis-net 
/wip
```

Adding Emojis for accelerated response time is optional.

A core team member will get back to you and start looking into the matter.
They might suggest changes and will help getting the visual appearance right.
Your blog post will soon see the light of day.

Well done!

[ref1]: {% post_url /2019/11/2019-11-22-How-to-Create-a-new-Blog-Post %}
[ref2]: {% post_url /2019/10/2019-10-29-Blog-Post-Features %}
[ref3]: https://kramdown.gettalong.org/quickref.html

[issue-link]: https://codebase.helmholtz.cloud/hifis/overall/hifis.net/issues/new?issuable_template=blog-post
[slug-generator]: https://blog.tersmitten.nl/slugify/
