---
title: Overview of HIFIS Workshops in 2020
date: 2020-02-21
authors:
  - Leinweber, Katrin
layout: blogpost
title_image: default
categories:
  - Announcement
tags:
  - workshop
excerpt:
    "
    HIFIS workshop cluster in spring and autumn 2020: Basic Software Carpentry,
    Introduction to GitLab and Bring Your Own Script.
    Specialized workshops are being planned: Catch them all
    by keeping an eye on our events page!
    "
---

As announced on [our events page][events], we offer several different
software development trainings to researchers.

1. "Software Carpentry" workshops introduce scientists to use- and powerful tools:
  Shell, Python and R for effective and reproducible scientific programming,
  and Git for version controlling your projects.
  Our first such event is planned for [March 31st and April 1st][200331].

2. "Bring Your Own Script" workshops ([like on May 12th and 13th][200512])
  will help you to make your code publication-ready:
  Regardless of your preferred programming language, we will advise you on all aspects
  of making your research software (re)usable for others, e.g. how to add a license.

3. "GitLab for Software Development in Teams" introduces advanced GitLab features:
  GitLab's project management, collaboration and automation features will help software development teams
  to improve software quality and speed up their release cycles.
  Join us on [June 9th and 10th][200609]!

Each event page provides more details on the topics to be covered,
possible prerequisites and registration instructions.
Please find all those details on the [individual event pages][events].

More specialized events outside this core curriculum
— like an [Introduction to Snakemake][snakemake] —
are being planned, so best keep an eye on that page!


[events]: https://events.hifis.net/category/4/

[200331]: {% link _events/2020/2020-03-31-the-carpentries-workshop.md %}
[200512]: {% link _events/2020/2020-05-12-ready-script-for-publication.md %}
[200609]: {% link _events/2020/2020-06-09-GitLab-Software-Development-Teams.md %}
[snakemake]: {% link _events/2019/2019-12-11-snakemake-introduction-workshop.md %}
