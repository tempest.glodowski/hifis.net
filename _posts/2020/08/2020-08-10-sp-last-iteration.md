---
title: "Last steps towards initial service portfolio"
title_image: default
date: 2020-08-10
authors:
  - Schollmaier, Laura (HZB)
layout: blogpost
categories:
  - News
excerpt_separator: <!--more-->
---

Starting with over 100 services as candidates for the upcoming Helmholtz Cloud, we had to define transparent and objective criteria to make the services comparable. 
We established a service selection process consisting of three iterations to shrink the service candidate list to a handable size. Now we are conducting the 3rd and last iteration of the service selection in which each remaining service is documented with detailed information.
Using this information the final score of each service can be calculated. The score will give priority on which services will be integrated into Helmholtz Cloud first.

