---
title: "Coding, Cookies, COVID-19"
date: 2020-06-11
authors:
  - dolling
  - dworatzyk
layout: blogpost
title_image: default
categories:
  - Report
tags:
  - hacky hour
excerpt:
    "
    The first HIFIS Hacky Hour was held online at 
    <a href=\"https://meet.jit.si/hifishackyhour\">jitsi</a>. 
    Here we give an overview from the organizers point of view.
    "
---

### What the hack is a Hacky Hour?
There are different answers to this question. 
An excellent one was provided by [Amanda Miotto][hackyhourhandbook].
We were forced by current events to find another.
In the original idea, a Hacky Hour is an informal meet-up for scientists to discuss research software and tools.
It also serves to get answers to technical questions in a cozy café-like environment.
Inspired by this, a similar event was being planned within HIFIS. 
Due to the COVID-19 pandemic we needed to adapt this concept a little bit.
Instead of a face-to-face get-together with cookies and coffee (or tea), we set-up a virtual meeting via Jitsi. 
But when one door closes, another door opens: This virtual format allowed us to bring together researchers from different Helmholtz centers in a joint meet-up. 
The _HIFIS Hacky Hour_ was born. 

### Why did we need a Hacky Hour?
Most areas of software development build on a strong community, for example [FOSS][FOSS] or forums like _Stack Overflow_.
Within the Helmholtz Association, however, opportunities to exchange experience with other researchers and [research software engineers][RSE] about software related topics are rather rare.
In particular in view of the current home office situation, we had the feeling that there was an increased need for sharing ideas, socializing, and support with individual software projects.
To fill this gap and to improve the situation, we adapted the Hacky Hour idea and organized a (bi-)weekly virtual meeting.

### How exactly did we organize the Hacky Hour?
Previous attempts to establish one quickly evolved into a mini-lecture-series. 
This approach died off rather quickly.
But the idea of a _Hacky Hour_ survived.
In this earlier approach certain topics (chosen by the organizers) did not raise much interest, so we tried a more community-driven approach:
* While the first topic ideas were suggested by us and a random one was picked for the first meeting, in the following sessions possible topics were collected in a _CodiMD_-like [pad][metapad].
* In this pad, participants could add their topic suggestions and vote for the next topic at the end of each Hacky Hour.
* The upcoming topic was chosen by the last meeting's participants (or randomly if there was no vote), ensuring that people were interested in the next session's topic.
* We then came together to discuss the topic and tried to answer questions that were raised by participants as a hive mind. 
By engaging participants in answering questions and in the planning of the next session, we try to avoid a simple helpdesk-situation.

![XKCD: wisdom of the ancients]({{ site.directory.images | relative_url }}/posts/2020-06-11-HackyHour/wisdom_of_the_ancients.png "XKCD: wisdom of the ancients")

### First impressions
When we set up the first meeting rather spontaneously taking an impromptu approach, we had no expectations (or practical experience) at all. 
It was reasoned that if nobody showed up, it would not have been a big waste of effort.
 All we had to do was setting up a virtual meeting room and e-mailing invitations to thousands of researchers (which is currently pretty cheap and time efficient).
And surprise: Of about 1500 e-mail recipients nine persons (whoa - an unexpectedly great response!) showed up for the first meeting to discuss the topic 
_Public Money – Public Code_.

[hackyhourhandbook]: https://github.com/amandamiotto/HackyHourHandbook
[FOSS]: https://fsfe.org/freesoftware/comparison.en.html
[RSE]: https://de-rse.org/en/
[metapad]: https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA?both
