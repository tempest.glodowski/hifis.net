---
title: "Helmholtz AAI Policies finalised"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2020-11-20
authors:
  - jandt
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    The Helmholtz AAI polcies have been finalised and approved by the HIFIS Coordination Board.

---

# Policies for Helmholtz AAI

The policies for the Helmholtz Authentication and Authorisation Infrastructure (AAI)  have been finalised and approved by the HIFIS Coordination Board. 
They apply for every participant using the Helmholtz AAI.

The structure of the policies was aligned with the [AARC Policy Development
Kit](https://aarc-community.org/policies/), so that the policies are
compatible with major infrastructures, such as WLCG and EOSC.

All policies and How-to are documented in the HIFIS/Helmholtz AAI Technical documentation at **<https://hifis.net/policies>**

### Brief overview

Specific policies are provided for different types of participants: 
* Infrastructure Management
* Infrastructure Security Contact
* Identity Provider
* Virtual Organisation (VO) Management
* SP-IdP Proxy
* Service Providers
* End users

![Overview plot](https://hifis.net/doc/helmholtz-aai/policies/helmholtz-aai/Policy_overview.png)
