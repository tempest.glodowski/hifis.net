- Subscribe here to receive all further [**HIFIS newsletter**](https://lists.desy.de/sympa/subscribe/hifis-newsletter) issues automatically.
- For more frequent updates, you are invited to regularly check our [**blog posts page**]({% link posts/index.html %})
- Subscribe to <a class="rss-posts-button" href="{{ '/feed/news.xml' | relative_url }}"
                       title="Subscribe to HIFIS Blog Posts RSS Feed (RSS Reader required)"
                       type="application/atom+xml"
                       download>
                       <strong>HIFIS Blog Posts RSS Feed</strong>
                    </a> (RSS Reader required)
- Subscribe to [**AAI Announcement letter**](mailto:helmholtz-aai-announcement-subscribe@fz-juelich.de) /
  [unsubscribe](mailto:helmholtz-aai-announcement-unsubscribe@fz-juelich.de)
