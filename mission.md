---
title: HIFIS Vision and Mission
title_image: default
layout: default
redirect_from:
  - vision
excerpt:
    HIFIS Vision and Mission
---

# Our Vision

The vision of the Helmholtz Incubator platform “HIFIS - Helmholtz Federated IT Services” is:

**Empowering scientists with digital services**

at every step of the scientific workflow.

HIFIS fosters collaboration and facilitates integrated scientific workflows across all research fields, Helmholtz centres, external collaboration partners and Incubator platforms by offering a unified login, shared cloud services, organizing common education and training events, and adopting a de-central and modular approach.
This stimulates innovation and knowledge exchange among the research centres.

## Mission

Following our vision, HIFIS is concentrating its efforts on:

- facilitating cooperation across scientific disciplines and institutions,
- building the best cloud platform for science,
- providing federated sovereign digital infrastructures, advocating and developing them,
- promoting awareness for scientific software as a driver of scientific progress, and
- enabling all people creating software at Helmholtz to apply modern software engineering principles that serve  good scientific practice.

To further follow our vision and mission, as well as to adapt to upcoming challenges, HIFIS is continuously adapting its focus areas and guiding its strategic direction.

**Helmholtz Digital Services for Science — Collaboration made easy.**

## Further Information

Almost all information on HIFIS is publicly available.

* [Publications]({% link publications.md %})
* [Learning Materials]({% link services/software/training.md %}#workshop-materials)
* [Media and Materials]({% link media/index.md %})
* [Technical and Administrative Documentation]({% link documentation.md %})
* Original [platform proposal for HIFIS](https://www.helmholtz.de/fileadmin/user_upload/01_forschung/Helmholtz_Inkubator_HIFIS.pdf)

### Contact

Contact us at <support@hifis.net> if you are missing any information, or if you have requests or comments.
