---
date: 2021-12-01
title: Tasks in December 2021
service: software
---

## Conceptualize Binder / ShinyR Integration into GitLab
Allowing scientists or reviewers to easily interact with computational environments
can be considered a key aspect for reproducible and easy-to-use research software.
Integrating tools like [Binder](https://mybinder.org/) or [ShinyR](https://shiny.rstudio.com/)
into the [Helmholtz-wide GitLab](https://codebase.helmholtz.cloud)
will allow scientists to easily make their research software product available
to others without the need for installing and configuring tools locally.
In this milestone, a concept will be elaborated on how this integration
may be realized.
