---
date: 2020-01-01
title: Tasks in January 2020
service: software
---

## Initial training of Carpentries Instructors
In preparation of providing Helmholtz-wide training events 12 Carpentries
instructors are trained as part of the membership of the HZDR and GFZ in
the [Carpentries](https://carpentries.org/).
