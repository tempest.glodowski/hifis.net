---
date: 2023-09-30
title: Tasks in Sep 2023
service: cloud
---

##  Download Area for service related documents
For the user to easily find up-to-date documents related to the service (e. g. AVV templates), we will set up a download area in Plony. Logged in users find all documents related to the service in one central place. In order easily find the download area, every service card will include a counter, thus giving the user the information how many documents can be found in the download area (including a link to get there).