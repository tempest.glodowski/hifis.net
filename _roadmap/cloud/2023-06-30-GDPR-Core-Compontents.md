---
date: 2023-06-30
title: Tasks in Jun 2023
service: cloud
---

##  GDPR Joint Controller Agreement about Helmholtz Cloud Core Components 
In close cooperation with the Helmholtz Data protection officers, a Joint Controller Agreement is finalized that is regulating the processing of personal data in the [Helmholtz Cloud Core Components](https://www.hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.2_Service-Type-Definitions/3.2.0_Service-Type-Definitions/). The Core Components are needed to run the Helmholtz Cloud: Helmholtz AAI, Helmholtz Cloud Portal, Service Database and Helpdesk. 


