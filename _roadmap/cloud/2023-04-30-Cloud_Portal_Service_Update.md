---
date: 2023-04-30
title: Tasks in Apr 2023
service: cloud
---

## Helmholtz Cloud Portal - Technical and user interface updates
The [Helmholtz Cloud Portal](https://helmholtz.cloud) has been migrated towards a new technical foundation, allowing for easier and more secure maintenance, functionality updates and integration.
Furthermore, new user-oriented work flows can be integrated in coordination with the Service DB (Plony).
The presentation of the services changes slightly. Based on the new back-end, user interface improvements and facelifts will be integrated more efficiently.

