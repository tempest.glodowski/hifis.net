---
date: 2021-11-01
title: Tasks in Nov 2021
service: cloud
---

## Process Framework for Helmholtz Cloud Service Portfolio
The Process Framework for Helmholtz Cloud Service Portfolio is published in version 1.0. It focuses on the explanation of the processes regarding the Service Portfolio Management for Helmholtz Cloud, giving an overview of which processes exist, how they interconnect, which roles are involved in each process and what is included in each process step.
