---
date: 2021-12-01
title: Tasks in Dec 2021
service: cloud
---

## Pilot-Status for at least one meta service
At least one of the prominent services (such as Nextcloud, Open Stack or Jupyter) or a combination of those is extended to a meta service: the meta service, which is provided by several Helmholtz Centers, is working as a federation or cooperation instead of separated local solutions.
