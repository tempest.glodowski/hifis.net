---
date: 2021-04-01
title: Tasks in Apr 2021
service: cloud
---

## Start evaluation of further Services
While the integration of the initial service portfolio will continue, the evaluation of further services will start. 
This process will presumably involve the review of postponed services from the initial service selection as well as service selection criteria, focusing on service usage and optimal user experience.
