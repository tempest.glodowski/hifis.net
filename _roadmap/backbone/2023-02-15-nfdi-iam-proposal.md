---
date: 2023-02-15
service: backbone
---

## AAI: HIFIS members are part of the NFDI IAM proposal
Using the Helmholtz-AAI as a starting point, the NFDI IAM proposal was
submitted to support the 26 NFDI Consortia. The participating HIFIS
members are tasked to maintaining full compatibility with the
Helmholtz-AAI and hence the AARC blueprint, and to establish a two-way communication channel amongst the
participants.
