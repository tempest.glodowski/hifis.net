---
date: 2022-10-01
title: Tasks in October 2022
service: backbone
---

## AAI: Foster automated deprovisioning information query from IdPs
During 2021, the framework to automatically request user deprovisioning information from IdPs via attribute query has been principally set up for the central AAI (development) instance and a small set of Helmholtz IdPs.
During 2022, the task is to remove remaining inconsistencies and set up the productive deprovisioning process for all IdPs,
at least with the already implemented fallback of manual user based confirmation of active accounts.

