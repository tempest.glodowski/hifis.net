---
date: 2023-07-01
service: backbone
---

## First Coordination of future operational Cybersecurity Activities
The HIFIS Backbone cluster, together with Cloud cluster, will define first coordinating steps to align with Helmholtz KoDa's activities in fostering Operational Cybersecurity.
