---
date: 2023-06-01
service: backbone
---

## AAI: Fostering the participation of non Helmholtz IdPs in Helmholtz AAI
Users from numerous non-Helmholtz organisations can in principle access Helmholtz AAI and Cloud services by logging in via their home IdP. However, lack of standard conformity, local policies and technical inconsistencies frequently prevent successful authentication, frustrating the users and also putting high load on the HIFIS support.
A frequently used workaround, authenticating via social IdPs (ORCID, Github, Google) allows users to access our services with caveats.
Using AAI statistics of user's originating organisations, we will identify organisations with significant numbers of users, and non-working IdP based authentication, so that we can concentrate efforts on actively integrating these organisations systematically.
The identification and workflow to do so is planned to be established by late spring 2023, with follow-up work being continuous.
