---
date: 2023-11-15
service: backbone
---

## AAI: Policy Review Process (close collaboration with Cloud Cluster)
With experiences made during the annual housekeeping, user deprovisioning, adoption of new use cases (for example, including robot accounts), and updates in the upstream AARC guidelines (SIRTFI-v2, REFEDS Assurance Framework V2), the AAI policies may need regular updates.
This and yet to be defined elements will be part of the policy review process that is due to be defined.
As this strongly related to similar review activities of the HIFIS Cloud cluster for the Helmholtz Cloud services, there will be a close collaboration between clusters on this.
