---
date: 2023-11-01
service: backbone
---

## AAI: Robot accounts, group accounts
Group Accounts and Robot Accounts are not yet fully supported by DFN AAI and thus Helmholtz AAI.
As such accounts are envisioned to play an increasing role in the Helmholtz AAI and Helmholtz Cloud, HIFIS will foster to establish an AARC guideline to fully support such accounts.
In parallel, HIFIS will provide first conceptually compatible implementations so that ongoing use cases can be supported soon.
