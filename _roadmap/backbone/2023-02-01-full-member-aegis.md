---
date: 2023-02-01
service: backbone
---

## AAI: HIFIS becoming full AEGIS member
For over one year, HIFIS already has been an observing member of the AARC Engagement Group for Infrastructures (AEGIS), which facilitates activities for the adoption of harmonised federation solutions and thus the design and adoption of AARC guidelines.

Based on the many practical use cases in the context of Helmholtz AAI and Helmholtz Cloud, as well as the continuous increase of user numbers and participating groups, HIFIS was asked to step up as a full member.
It is planned to do so by February; we look forward to a fruitful cooperation on further developing harmonised federated access to digital resources for science.
