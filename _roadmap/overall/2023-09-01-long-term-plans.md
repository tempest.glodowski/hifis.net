---
date: 2023-09-01
service: overall
---

## Detailed elaboration on long-term plans
Following the first presentation of envisioned future developments of HIFIS
in the Helmholtz Incubator Workshop in July,
further details are planned to be worked out until fall 2023.
