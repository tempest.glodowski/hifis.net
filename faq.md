---
title: <i class="fas fa-question-circle"></i> FAQ — Frequently Asked Questions
title_image: default
layout: default
excerpt:
    Collection of Frequently Asked Question (FAQ) about HIFIS.
---

## Content
{:.text-success}

- [General Questions about HIFIS](#general-questions-about-hifis)
- [Access and Service Portfolio](#access-and-service-portfolio)
- [HIFIS for Scientists](#hifis-for-scientists)
- [Workshops and Events](#workshops-and-events)
- [Helmholtz AAI](#helmholtz-aai)
- [Questions / Feedback (English or German)](#contact-us)

---

## General Questions about HIFIS
{:.text-success}

<div class="my-3">
<strong id="what-is-the-mission-of-hifis">
<a class="link-anchor" href="#what-is-the-mission-of-hifis"><i class="fas fa-link"></i></a>
What is the mission of HIFIS?
</strong><br>
    <b>Our Aim</b>
<p>The top position of Helmholtz research is increasingly based on cross-centre and international cooperation and common access to data treasure and -services.
At the same time, the significance of a sustainable software development for the research process is recognised.</p>

<p><b>HIFIS builds and sustains an excellent IT infrastructure connecting all Helmholtz research fields and centres.</b></p>

<p>This includes the Helmholtz Cloud, where members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use. HIFIS further supports Research Software Engineering (RSE) with a high level of quality, visibility and sustainability.</p>

<p>The HIFIS clusters develop technologies, processes and policy frameworks for harmonized access to fundamental backbone services, such as Helmholtz AAI, and Helmholtz Cloud services. HIFIS Software also provides education, technology, consulting and community services.</p>

<p><b>Helmholtz Digital Services for Science — Collaboration made easy.</b></p>
        <p><i class="fas fa-headphones"></i>
        <strong>You are invited to <a href="https://resonator-podcast.de/2021/res172-hifis/">tune in to the recent Resonator podcast on HIFIS</a>, to get an idea on our aims and scope!</strong> (German only)
        <i class="fas fa-headphones"></i></p>
    </div>

---

<div class="my-3">
<strong id="timeline">
<a class="link-anchor" href="#timeline"><i class="fas fa-link"></i></a>
What is the timeline for HIFIS?
</strong><br>
    <p>The HIFIS platform was set up in 2019, with basic infrastructure services (AAI) and pilot services online since 2020. The <a href="{% post_url 2020/10/2020-10-13-initial-service-portfolio %}">initial cloud service portfolio has been made public</a> by
    end of 2020;
    the production version of the <a href="{% post_url 2021/11/2021-11-17-CloudPortal %}">Helmholtz Cloud Portal was made available</a> by November 2021.
    The finalisation of the cloud rulebook and GDPR compliant agreement is planned during 2022.</p><p>
    Please also refer to our <a href="{{ "roadmap" | relative_url }}"><strong>HIFIS Roadmap</strong></a> to stay up to date.
    </p>
</div>

---

<div class="my-3">
<strong id="more-information-on-hifis">
<a class="link-anchor" href="#more-information-on-hifis"><i class="fas fa-link"></i></a>
Where can I find more information about HIFIS?
</strong><br>
    <p>There are several places to look for more information about the platform, its technical and organisational details:</p>
    <ul>
        <li>Our <a href="{% link mission.md %}">Mission Statement</a></li>
        <li><a href="{% link publications.md %}">Publications, Presentations and Selected Blog Posts</a></li>
        <li>The <a href="{% link documentation.md %}">Technical and Administrative Documentation</a></li>
        <li>Our <a href="{% link structure/index.html %}">Organisational Structure</a></li>
        <li>Our <a href="{% link reports.md %}">Annual Reports</a></li>
        <li>A <a href="{% link services/overall/survey.html %}">Survey</a> that we conducted to better understand the needs of our users</li>
        <li>An Overview about <a href="{% link partners.md %}">Partners</a> within and beyond Helmholtz</li>
    </ul>
</div>

---

<div class="my-3">
<strong id="where-can-i-register-for-a-newsletter">
<a class="link-anchor" href="#where-can-i-register-for-a-newsletter"><i class="fas fa-link"></i></a>
Where can I register for a newsletter?
</strong><br>
        <p>Since July 2022, HIFIS is sending quarterly newsletters via <code class="language-plaintext highlighter-rouge">newsletter@hifis.net</code>. You are invited to register to never miss any news and upcoming events! Newsletters published so far can be <a href="{% link newsletter/index.md %}">found here</a>.</p>
</div>
<div markdown=1>
{% include subscription_lists.md %}
</div>

---

<div class="my-3">
<strong id="how-should-i-acknowledge-hifis-assistance-in-a-publication">
<a class="link-anchor" href="#how-should-i-acknowledge-hifis-assistance-in-a-publication"><i class="fas fa-link"></i></a>
How should I acknowledge HIFIS assistance in a publication?
</strong><br>
    <p>Please include the following text:</p>
    <blockquote>
    <p>We gratefully acknowledge the HIFIS (Helmholtz Federated IT Services)
    team for … (e.g. support with the components above).</p>
    </blockquote>
</div>

---

## Access and Service Portfolio
{:.text-success}

<div class="my-3">
<strong id="how-can-i-access-cloud-services">
<a class="link-anchor" href="#how-can-i-access-cloud-services"><i class="fas fa-link"></i></a>
How can I access Cloud Services?
</strong><br>
    <p>Find all our Cloud Services, as well as any usage details and conditions at the
    <a href="https://helmholtz.cloud/services/"><strong>Helmholtz Cloud Portal</strong></a>.</p>
    <p>It is easier than ever before, since you do not need to create new accounts with new passwords!
    Just search for “Helmholtz ID” (or “Helmholtz AAI”) when logging in and use your home institute’s credentials.</p>
    <p>Please
    <a href="{% link aai/howto.md %}"><strong>refer to our illustrated tutorial on how to access Cloud Services via Helmholtz AAI</strong></a>
    for more details.</p>
</div>


---

<div class="my-3">
<strong id="my-helmholtz-center-is-not-directly-involved-into-hifis-do-you-still-help-me">
<a class="link-anchor" href="#my-helmholtz-center-is-not-directly-involved-into-hifis-do-you-still-help-me"><i class="fas fa-link"></i></a>
My Helmholtz center is not directly involved into HIFIS. Do you still help me?
</strong><br>
    <p>Yes, of course. HIFIS is a Helmholtz-wide platform that aims to
    provide offers for <strong>all</strong> Helmholtz centers.
    Please <a href="mailto:support@hifis.net">contact us</a> or any of the <a href="{% link team.md %}">HIFIS team members</a> to get assistance.</p>
</div>

---

<div class="my-3">
<strong id="i-am-not-helmholtz-do-you-still-help-me">
<a class="link-anchor" href="#i-am-not-helmholtz-do-you-still-help-me"><i class="fas fa-link"></i></a>
I am not even affiliated with Helmholtz. Do you still help me?
</strong><br>
    <p>Yes, we can help you if you are collaborating with someone from a Helmholtz centre. 
    HIFIS is designed to support everyone in Helmholtz <strong>and collaboration partners</strong>.
    If this applies to you, discuss HIFIS with your partners from <a href="https://www.helmholtz.de/en/about-us/helmholtz-centers/">a Helmholtz centre</a>. They will invite you to join a <a href="#aai-what-is-a-vo">“virtual organisation”</a> or create one for your collaboration.</p>
</div>

---

<div class="my-3">
<strong id="why-similar-services">
<a class="link-anchor" href="#why-similar-services"><i class="fas fa-link"></i></a>
Why are there several similar Cloud services like Jupyter, Nextcloud or Limesurvey?
</strong><br>
<p>Some services offered by different centres are indeed based on the same software (e.g. Jupyter) or offer different tools for the same function (e.g. <a href="https://helmholtz.cloud/services/?filterKeywordIDs=a7fceac6-1cc7-42b6-b277-7cbcad8f49c3&serviceID=5b9113f5-e3cf-4920-b081-822a58902b0f">SciFlow</a> and <a href="https://helmholtz.cloud/services/?filterKeywordIDs=a7fceac6-1cc7-42b6-b277-7cbcad8f49c3&serviceID=59a9d116-a897-4eac-a8f5-b810ab3838fe">Collabtex</a>). These alternatives are always welcome in the Helmholtz Cloud because they are good for <strong>scalability</strong>, <strong>resilience</strong>, and also for meeting individual needs as closely as possible.<br>
A consequence of this choice is that we try to <strong>avoid generic names</strong> like “GitLab” or “chat” to be fair to future alternative services. Providers are therefore encouraged to find unique names, as you may have noticed for example with "Codebase" or "Juchat".<br>
We know this can be confusing, especially if the names are not self-explanatory. If you have an idea for a more elegant solution, <a href="mailto:support@hifis.net">let us know</a>!</p>

</div>

---

## HIFIS for Scientists
{:.text-success}

<div class="my-3">
<strong id="how-can-hifis-help-me-as-a-researcher">
<a class="link-anchor" href="#how-can-hifis-help-me-as-a-researcher"><i class="fas fa-link"></i></a>
How can HIFIS help me as a researcher?
</strong><br>
    <p>The purpose of HIFIS is to support science and scientists. Within the HIFIS platform, we are building:</p>
    <ul>
    <li>The <a href="https://helmholtz.cloud"><strong>Helmholtz Cloud</strong></a>, providing a portfolio of seamlessly accessible IT services to simplify your daily work.</li>
    <li>A <strong>Helmholtz-wide login mechanism</strong>, the <a href="{% link aai/index.md %}">Helmholtz AAI</a>,</li>
    <li>A stable, high-bandwidth <strong>network infrastructure</strong> connecting all Helmholtz centers,</li>
    <li><a href="{{ "services/software-overview" | relative_url }}"><strong>Software services</strong></a> to provide you with a common platform,
    <a href="https://events.hifis.net/category/4/">training</a> and
    <a href="{% link services/software/consulting.html %}">support</a> for high-quality sustainable software development.</li>
    </ul>
    <p>All cloud services and software trainings and support are free of charge!</p>
</div>

---

## Workshops and Events
{:.text-success}

<div class="my-3">
<strong id="which-workshops-does-hifis-offer">
<a class="link-anchor" href="#which-workshops-does-hifis-offer"><i class="fas fa-link"></i></a>
Which workshops does HIFIS offer?
</strong><br>
    <p>
    You can find an overview of all ready-to-go workshops in the <a href="{% link services/software/training.md %}">List of published materials</a>.
    These are offered on a regular basis.
    For some topics, there is already some self-learning material, more is in preparation.
    </p>
    <p>
    We also may offer custom-tailored versions of these workshops on demand or create workshops on other RSE topics.
    </p>
</div>


---

<div class="my-3">
<strong id="how-to-request-workshops">
<a class="link-anchor" href="#how-to-request-workshops"><i class="fas fa-link"></i></a>
How do I request a workshop?
</strong><br>
    <p>
    Anybody within Helmholtz may request a workshop as long as they can bring around 15 to 20 participants.
    If you would like us to host a specific workshop for your team, please contact us:
    <a href="mailto:support@hifis.net">support@hifis.net</a>.
    </p>
    <p>
    You can either choose from the <a href="{% link services/software/training.md %}">List of Workshop Topics</a> or request a custom workshop.
    We will reach out to you to clarify the details based on your individual case.
    </p>
</div>

---

<div class="my-3">
<strong id="what-is-the-cost-of-workshops">
<a class="link-anchor" href="#what-is-the-cost-of-workshops"><i class="fas fa-link"></i></a>
What do workshops cost?
</strong><br>
    <div>
        For members of the Helmholtz Association, requesting and participating in HIFIS workshops is free of charge.
        We reserve the option to charge for participants who are not part of the Helmholtz Association (although we usually prefer not to).
    </div>
</div>

---

<div class="my-3">
<strong id="use-of-workshop-material">
<a class="link-anchor" href="#use-of-workshop-material"><i class="fas fa-link"></i></a>
Can I use your workshop materials for my own workshop?
</strong><br>
    <p>
    By default, our workshop materials are published under the Creatice Commons license
    <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.
    This means you can share (copy and redistribute) the material,
    but also adapt it to your needs under the provision that you attribute the authors with appropriate credits.
    </p>
    <p>
    Deviations from this policy may occur. Please check the footer of the workshop materials for information on the license that applies.
    </p>
</div>

---

<div class="my-3">
<strong id="train-the-trainer">
<a class="link-anchor" href="#train-the-trainer"><i class="fas fa-link"></i></a>
Do you help me with teaching my workshop, do you offer something like "train the trainer"?
</strong><br>
    <p>
    At the moment, we don't have the possibility to do any instructor trainings.
    This may change in the future.
    However, we will be happy to lend you advisory support as much as we can.
    </p>
</div>

---

## Helmholtz AAI
{:.text-success}

<div class="my-3">
<strong id="aai-what-is">
<a class="link-anchor" href="#aai-what-is"><i class="fas fa-link"></i></a>
What is Helmholtz AAI?
</strong><br>
    AAI is short for <i>Authentication and Authorisation Infrastructure</i> and comprises the central component in gaining access to a service shared by a Helmholtz centre different from your home centre.
    Once you log in to the Cloud Portal itself or the federated services available through it, it will ask for your home centre.
    When you choose it, you will get redirected to your home identity provider (IdP), enter your username and password there and get authenticated at the service.
    The Helmholtz AAI manages the communication between your home identity provider (IdP for short) and the federated service you want to access.
    No hassle for you.
</div>

---

<div class="my-3">
<strong id="aai-refused-transfer">
<a class="link-anchor" href="#aai-refused-transfer"><i class="fas fa-link"></i></a>
I accidentally refused to transfer my personal data when trying to log in to a service.
Now I cannot use it. How to revert that?
</strong><br>
    <ol>
    <li> Go to <a href="https://login.helmholtz.de/home">https://login.helmholtz.de/home</a> </li>
    <li> Log in, if needed </li>
    <li> On the account page, click on the <i>Trusted Application</i> tab. </li>
    <li> In the list, select the service of interest click on <i>Revoke access</i> and confirm the pop-up message. </li>
    </ol>
</div>

---

<div class="my-3">
<strong id="aai-authn-failed">
<a class="link-anchor" href="#aai-authn-failed"><i class="fas fa-link"></i></a>
While logging in using my home organisation, I got an <code>Authentication failed</code> error.
</strong><br>
    The error message says:
    <br/>
    <code>The remote authentication was successful, however the the server's policy requires more information then was provided to register your account.</code>
    <br/>
    Your home organisation did not release all mandatory information to create your Helmholtz AAI account.
    Beside the federations metadata, the mandatory information that your organisation needs to transfer to the Helmholtz AAI can be reviewed in the
    <a href="https://hifis.net/doc/helmholtz-aai/attributes/#consumed-attributes-from-identity-providers-idp">documentation</a>.
    Please contact your organisation's helpdesk and ask to release the mandatory information to the Helmholtz AAI.
    In case your home organisation does not want to release this information, you can use some social accounts like Google, GitHub and ORCID.
</div>

---

<div class="my-3">
<strong id="aai-no-oauth-context">
<a class="link-anchor" href="#aai-no-oauth-context"><i class="fas fa-link"></i></a>
While logging in, I got an <code>Authorization Server got an invalid request. No OAuth context</code> error.
</strong><br>
    Login via Helmholtz AAI requires the communication between multiple instances that must not be interrupted.
    If the login flow is paused or gets interrupted, the session might get lost or the exchanged information between the different services is not valid anymore.
    When the login is continued, the AAI service can not resume the session or use the received information if they are outdated.
    In these cases, it shows the message <code>Authorization server got an invalid request. No OAuth context.</code>
    Please restart the login from the service you want to use and do not pause or interrupt it.
    If the error appears although there was no pause, please contact us at <a href="mailto:support@hifis.net">support@hifis.net</a>.
</div>

---

<div class="my-3">
<strong id="aai-how-authn">
<a class="link-anchor" href="#aai-how-authn"><i class="fas fa-link"></i></a>
How does the authentication via Helmholtz AAI work?
</strong><br>
    In short: the central component of the Helmholtz AAI is a Unity instance that serves as a proxy between your home identity provider (IdP) and the service you want to access.
    Your home IdP knows who you are and is trusted by the Helmholtz AAI.
    You just need to log in with your username and password of your center at your home IdP and the rest is magic. &#x1F600;

    - Have a look at the <a href="{% post_url 2021/06/2021-06-23-how-to-helmholtz-aai %}">illustrated tutorial on how this works</a>!
    - For more details, you are invited to have a look at the <a href="https://hifis.net/doc/helmholtz-aai/concepts/">concept page of Helmholtz AAI</a>.
</div>


---

<div class="my-3">
<strong id="aai-how-authz">
<a class="link-anchor" href="#aai-how-authz"><i class="fas fa-link"></i></a>
How does the authorisation at the services work / how does a service know what I am allowed to do there?
</strong><br>
    Either your home identity provider or the Helmholtz AAI (via a virtual organisation) communicate your permissions (so-called entitlements) to the service.
</div>

---

<div class="my-3">
<strong id="aai-what-is-a-vo">
<a class="link-anchor" href="#aai-what-is-a-vo"><i class="fas fa-link"></i></a>
What is a virtual organisation?
</strong><br>
    A virtual organisation (VO) is a group of people led by a principal investigator (PI) that can gain special access to services if they have specialised needs for more features than a basic variant of a service offers.
    They can also negotiate and gain access to services with a restricted access policy. All this, however, is based on individual agreements.
</div>

---

<div class="my-3">
<strong id="aai-which-centres">
<a class="link-anchor" href="#aai-which-centres"><i class="fas fa-link"></i></a>
Which Helmholtz centres are connected to the Helmholtz AAI?
</strong><br>
    Have a look at the <a href="https://hifis.net/doc/helmholtz-aai/list-of-connected-organisations/"> list of connected centres</a>.
    At the moment of writing, all Helmholtz centres are connected.
</div>

---

<div class="my-3">
<strong id="aai-how-to-create-vo">
<a class="link-anchor" href="#aai-how-to-create-vo"><i class="fas fa-link"></i></a>
How do I create a new virtual organisation?
</strong><br>
    A new virtual organisation has to be requested by a principal investigator (PI) at the HIFIS team.
    Note that the PI has to be a member of a Helmholtz centre.
    The procedure for requesting a new virtual organisation is shown in the <a href="https://hifis.net/doc/helmholtz-aai/howto-vos/">Helmholtz AAI How-To</a>.
</div>

---

<div class="my-3">
<strong id="aai-further-infos">
<a class="link-anchor" href="#aai-further-infos"><i class="fas fa-link"></i></a>
Where do I find further information?
</strong><br>
    Have a look at the <a href="https://hifis.net/doc">HIFIS and AAI documentation page</a>, where you find more technical information on all our topics.
    If you don't find what you are looking for, contact us at <a href="mailto:support@hifis.net">support@hifis.net</a>.
</div>

---

## Contact us
{:.text-success}

<div class="my-3">
<strong>You have a question that isn't answered here? Or you do have feedback on the FAQs?</strong><br>
    Contact us anytime in English or German / Deutsch: <a href="mailto:support@hifis.net">support@hifis.net</a>
</div>
