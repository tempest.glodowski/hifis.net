/assign @user1
/label ~newsletter
/label ~Progress::ToDo
/milestone %milestone
/due <date>

# Contribution for the HIFIS Newsletter

## Authors

> Name the involved authors here, maybe suggesting contributors.
> If you know their GitLab aliases, use @username to keep them
> involved.

## Reviewers

> Who do you want as reviewers of your Newsletter contribution? 
> Default is @lisa.klaffki as the newsletter editor, add people who should review contentwise. 
> If you know their GitLab aliases, use @username to keep them involved.

## Topic and Content

> Outline what the Assignee should write about.
> Specify the style of the post (e.g. report, user story, guide, etc.).

## Deadline Date

> When is the deadline to hand in the FINAL text?
> <date>


