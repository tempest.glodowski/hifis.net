---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: Kadi4Mat

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2023-03-10

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: kadi4mat/kadi4mat_logo.png

# One or two sentences describing the software
excerpt: Kadi4Mat is an open source software for managing research data, which support a close cooperation between experimenters, theorists an simulators, especially in the field of materials science.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: Kadi4Mat - A Virtual Research Environment for Managing Research Data.

# Add at least one keyword
keywords:
    - Electronic Lab Notebook
    - Reproducible Workflows
    - Structured Data Storage
    - Web based data exchange
    - Materials Science
    - FAIR Data

# The Helmholtz research field
hgf_research_field: Information

# At least one responsible centre
hgf_centers:
    - Karlsruhe Institute of Technology

# List of scientific communities
scientific_community:
    - Materials Science

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: michael.selzer@kit.edu

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: gitlab
      link_as: https://gitlab.com/iam-cms/kadi
    - type: webpage
      link_as: https://kadi.iam-cms.kit.edu


# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: Apache-2.0

# Is the software pricey or free? (optional)
costs:

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Research Data Management

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    -

# List of programming languages (optional)
programming_languages:
    - Python
    - Vue
    - JavaScript

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5334/dsj-2021-008

# Funding of the software (optional)
funding:
    - shortname: BMBF
    - shortname: DFG
    - shortname: EU
    - shortname: KIT 
    - shortname: MWK
---

# Kadi4Mat: A Research Data Infrastructure for Materials Science

[Kadi4Mat](https://kadi.iam-cms.kit.edu) is **Karlsruhe Data Infrastructure for Materials Science**, an open source software for managing research data. It is being developed as part of several research projects at **Institute for Applied Materials (IAM)** and **Institute of Nano Technology (INT)** of the **Karlsruhe Institute of Technology (KIT)**. 

The web based application combines the possibility of structured storage of research data and corresponding metadata and data exchange, the <em>repository</em>, with the possibility to analyze, visualize and transform said data, the <em>electronic lab notebook (ELN)</em>. The ELN component is focused on the automated and documented execution of heterogeneous workflows, while the repository component focuses on unpublished data that is yet to be analysed further. The ELN can be described as **ELN 2.0**, which offers automation by providing API. This improved automation provides a more direct way to capture data provenance of the resources in Kadi4Mat. In this way, a virtual research environment is created which facilitates collaboration between researchers.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/kadi4mat/Homepage.png" alt="Web interface of Kadi4Mat">
<span>The screenshot shows an example of the Kadi4Mat web interface.</span>
</div>

The software was developed to support a close cooperation between experimenters, theorists and simulators especially in materials science but it has been kept as generic as possible. It also offers facility to directly publish data on external systems like **Zenodo**. Research data management with Kadi4Mat increases cooperation between researchers, taking into account the **FAIR data philosophy**.
The Kadi4Mat ecosystem offers its users multiple functionalites, illustrated in the figure below:

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/kadi4mat/Kadi4MatScheme.png" alt="Conceptual Overview of Kadi4Mat.">
<span>The screenshot shows the functionalities offered by Kadi4Mat. <cite><a href="https://datascience.codata.org/articles/10.5334/dsj-2022-016/" target="_blank"> (KadiStudio: FAIR Modelling of Scientific Research Processes) </a></cite></span>
</div>

* **KadiWeb**, a generally accessible web-based version of Kadi4Mat incorporating a classical ELN and a repository.
* **KadiStudio**, a desktop-based software version that allows the formulation and execution of workflows which can also be used offline.
* **KadiFS**, an open source software for mounting resources from Kadi4Mat into the file system.
* **KadiAI** and **CIDS**, are Kadi4Mat's interface and solution for integrated Machine learning (ML) and Artificial Intelligence (AI).

## KadiWeb

**KadiWeb** is a general accessible web-based version of Kadi4Mat which offers a corresponding web interface, thus even an inexperienced user can utilise all functions of Kadi4Mat. The most important type of resource that can be created in Kadi4Mat are the so-called ***records***, which can link data with descriptive metadata (includes both basic metadata and domain-specific metadata).

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/kadi4mat/Generic_Metadata_editor.png" alt="Screenshot of generic metadata editor.">
<span>The screenshot shows the currently possible different types of metadata entries can be created using generic metadata editor.</span>
</div>

## KadiStudio

**KadiStudio** is a desktop-based software version that allows to design and execute ***Workflows*** which can also be used offline, by running as an ordinary application on a local workstation. Different steps of a workflow can be defined using a graphical node editor to create reporducible workflows.

## KadiFS

**KadiFS** is an integration of Kadi4Mat into a desktop environment to combine the advantages of Kadi4Mat and the usual work in the filesystem.

## KadiAI

**KadiAI**, is to integrate and implement Artificial Intelligence (AI) and Machine Learning (ML) algorithms into Kadi4Mat using common interfaces and workflows. Leverage interactive dashboards to design, train, and tune data-driven models or enhance custom AI scripts with next-level research data management. ***CIDS (Computational Intelligence and Data Science)*** framework implements a wide range of AI models for statistical, active and deep learning. Through ***KadiAI***, ***CIDS*** integrates seamlessly with the Kadi4Mat platform. 


## Watch Video:
<a href="https://tu-darmstadt.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=e6eed9d2-fcd8-4c25-a306-aebd0146a59b" target="_blank" alt="A Research Data Infrastructure for Materials Science">A Research Data Infrastructure for Materials Science <i class="fas fa-external-link-alt"></i></a>

