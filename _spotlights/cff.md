---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: Citation File Format

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-06-23

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: cff/cff.png

# One or two sentences describing the software
excerpt: The Citation File Format (CFF) project provides a format and schema for plain text CITATION.cff files with human- and machine-readable citation information for software (and datasets). Developers/RSEs can include them in their repositories to let others know how to correctly cite their software. The project also provides tooling for working with CFF files.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: Citation File Format (CFF)

# Add at least one keyword
keywords:
    - software citation
    - Citation File Format
    - metadata
    - research software sustainability

# The Helmholtz research field
hgf_research_field: Information

# At least one responsible centre
hgf_centers:
    - German Aerospace Center (DLR)

# List of other contributing organisations (optional)
contributing_organisations:
    - name: "Netherlands eScience Center"
      link_as: https://www.esciencecenter.nl/
    - name: "University of Manchester"
      link_as: https://www.manchester.ac.uk/

# List of scientific communities
scientific_community:
    - Research Software Engineering
    - Library and Information Science
    - Computer Science (Software Engineering)
    - Scholarly Communications

# Impact on community (optional, not implemented yet)
impact_on_community: Over 9,000 CITATION.cff files on GitHub

# An e-mail address
contact: stephan.druskat@dlr.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://citation-file-format.github.io/
    - type: github
      link_as: https://github.com/citation-file-format/citation-file-format

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: CC-BY-4.0

# Is the software pricey or free? (optional)
costs: free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - software metadata

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - CLI

# List of programming languages (optional)
programming_languages:
    - YAML
    - JSON Schema
    - Go
    - Haskell
    - Java
    - JavaScript
    - Julia
    - PHP
    - Python
    - R
    - Ruby
    - TypeScript

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.1003149

# Funding of the software (optional)
funding:
  - shortname: Software Sustainability Insitute (SSI), UK
    funding_text: Fellowship (Stephan Druskat, since 2018), event fund
    link_as: https://software.ac.uk/
  - shortname: "Code for Science & Society, US"
    funding_text: Digital Infrastructure Incubator
    link_as: https://incubator.codeforscience.org/
  - shortname: "CampusSource e.V., DE"
    funding_text: CampusSource Award 2022
    link_as: https://ev.campussource.de/publikationen/csa2022/

---

# The Citation File Format - making software citation easi(er)

The citation of software is important to enable attribution and credit for developers
of research software and reproducibility.
In comparison to more traditional research outputs such as papers,
software does not inherently provide the necessary metadata to cite it correctly.
Therefore it is necessary for software authors to provide these metadata,
including software-specific metadata such as version identifiers,
source code repositories and license information.

The [Citation File Format](https://citation-file-format.github.io/) (CFF) is a community project that provides
a human- and machine-readable format and schema for software citation metadata files (`CITATION.cff`),
as well as tools for working with the format.
This way, software authors can supply the relevant metadata for their software,
and both human users and downstream services can use and reuse the respective metadata.

```yaml
  cff-version: 1.2.0
  message: "If you use this software, please cite it as below."
  authors:
    - family-names: Druskat
      given-names: Stephan
      orcid: https://orcid.org/0000-0003-4925-7248
  title: "My Research Software"
  version: 2.0.4
  doi: 10.5281/zenodo.1234
  date-released: 2021-08-11
```
<div class="spotlights-text-image">
<span>An example for a minimal <tt>CITATION.cff</tt> file.</span>
</div>

The Citation File Format project provides tooling for creating, editing, updating, validating, and converting CFF files
in a number of programming languages/technologies.
All tools as well as the schema and documentation are published under open licenses.

CFF is supported by several platforms in academia and software development;
GitHub uses `CITATION.cff` files to render citation information for repositories;
Zenodo - a universal open access repository for digital research artifacts - uses the information in
CFF files to populate record metadata in automated workflows;
Reference managers such as Zotero and JabRef support the import of software citation metadata from CFF files.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/cff/cff_screen.png" alt="The GitHub citation widget displaying citation information based on CITATION.cff.">
<span>The GitHub citation widget displaying citation information based on <tt>CITATION.cff</tt>.</span>
</div>

The Citation File Format schema is written in JSON Schema.
CFF files instances are implemented in YAML.
As of June 2022, [more than 9,000 `CITATION.cff`](https://github.com/sdruskat/cfftracker) files have been created on GitHub alone.
