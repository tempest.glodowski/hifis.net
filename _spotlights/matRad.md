---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: matRad

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-11-08

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: matRad/matrad.svg

# One or two sentences describing the software
excerpt: > 
    matRad is a dose calculation and inverse treatment planning toolkit for 
    radiotherapy research and education written in Matlab.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: spotlight_matRad_compliation.png

# Title at the top, inside the title-content-container
title: matRad

# Add at least one keyword
keywords:
    - Medical Physics
    - Radiotherapy
    - Particle Therapy
    - Treatment Planning


# The Helmholtz research field
hgf_research_field: Health

# At least one responsible centre
# Please use the full and official name of your centre
hgf_centers:
    - German Cancer Research Center (DKFZ)

# List of other contributing organisations (optional)
contributing_organisations:
    - name: Heidelberg Ion Beam Therapy Center (HIT)
      link_as: https://www.klinikum.uni-heidelberg.de/interdisziplinaere-zentren/heidelberger-ionenstrahl-therapiezentrum-hit
    - name: Heidelberg Institute for Radiation Oncology (HIRO)
      link_as: https://www.dkfz.de/en/hiro/index.html

# List of scientific communities
scientific_community:
    - Medical Physics

# Impact on community (optional, not implemented yet)
impact_on_community:
    - more than 130 forks and more than 160 citations

# An e-mail address
contact: n.wahl@dkfz.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: github
      link_as: https://github.com/e0404/matRad
    - type: webpage
      link_as: http://www.matRad.org

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-3.0-or-later

# Is the software pricey or free? (optional)
costs: free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - numerical simulation

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - Matlab
    - C/C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
# If you have one doi, please provide it as a variable:
doi: 10.5281/zenodo.3879615
# If you have multiple products, e.g. in a framework, and you want to reference each product with a DOI,
# then please provide a dictionary. Uncomment the following lines:
#     - name: # Product name for first DOI
#       doi: # DOI


# Funding of the software (optional)
funding:
    - shortname: DFG (Grant No. WA 4707/1-1)
      link_as: https://www.dfg.de/

---

# matRad: An open source dose calculation and treatment planning toolkit for research and education

matRad is an open source software for radiation treatment planning of intensity-modulated photon, proton, and carbon ion therapy started in 2015 in the research group "Radiotherapy Optimization" within the Department of Medical Physics in Radiation Oncology at the German Cancer Research Center - DKFZ. 

matRad targets education and research in radiotherapy treatment planning, where the software landscape is dominated by proprietary medical software. As of August 2022, matRad had more than 130 forks on GitHub and its [development paper](https://doi.org/10.1002/mp.12251) was cited more than 160 times (according to Google Scholar). matRad is entirely written in MATLAB and mostly compatible to GNU Octave.

matRad comprises
- MATLAB functions to model the entire treatment planning workflow
- Example patient data from the CORT dataset
- Physical and biological base data for all required computations

In particular we provide functionalities for

- Pencil-beam dose calculation for photon IMRT and proton/carbon IMPT
- Monte Carlo dose calculation for photon IMRT (with ompMC) and proton IMPT (with [MCsquare](http://www.openmcsquare.org))
- Non-linear constrained treatment plan optimization (based on physical dose, RBE-weighted dose and biological effect) using [IPOPT](https://coin-or.github.io/Ipopt/) or Matlab's fmincon
- Multileaf collimator sequencing
- Basic treatment plan visualization and evaluation
- Graphical User Interface
- Standalone Executable (using the Matlab Runtime)

matRad is constantly evolving. If you are interested in working with us or are looking for a special feature do not hesitate and get in touch.

## Philosophy

matRad provides a graphical user interface for educational purposes and basic treatment plan prameterization and visualization.
<div class="spotlights-text-image">
<img alt="matRad's graphical user interface (GUI)" src="{{ site.directory.images | relative_url}}spotlights/matRad/matRad_prostate_carbon.png">
<span>Screenshot of matRad's graphical user interface visualizing the biologically effective dose of a carbon ion treatment plan using opposing lateral beams for a prostate patient.</span>
</div>

matRad uses Matlab's dual scripting & visualization environment to allow a parallel workflow, alternating between scripting / command line input and triggering workflow steps in the graphical user interface.
A script to recreate the above treatment plan within matRad 2.10.1 is shown below:

```matlab
matRad_rc

% load patient data, i.e. ct, voi, cst
load PROSTATE.mat

% meta information for treatment plan
pln.radiationMode   = 'carbon';     % either photons / protons / carbon
pln.machine         = 'Generic';

pln.numOfFractions  = 30;

% beam geometry settings
pln.propStf.bixelWidth      = 5; % [mm] / lateral spot spacing for particles
pln.propStf.gantryAngles    = [90 270]; % [?]
pln.propStf.couchAngles     = [0 0]; % [?]
pln.propStf.numOfBeams      = numel(pln.propStf.gantryAngles);
pln.propStf.isoCenter       = ones(pln.propStf.numOfBeams,1) * matRad_getIsoCenter(cst,ct,0);

% dose calculation settings
pln.propDoseCalc.doseGrid.resolution.x = 5; % [mm]
pln.propDoseCalc.doseGrid.resolution.y = 5; % [mm]
pln.propDoseCalc.doseGrid.resolution.z = 5; % [mm]

% optimization settings
pln.propOpt.optimizer       = 'IPOPT';
pln.propOpt.bioOptimization = 'LEMIV_effect';   

%% generate steering file
stf = matRad_generateStf(ct,cst,pln);

%% dose calculation
dij = matRad_calcParticleDose(ct,stf,pln,cst);

%% inverse planning for imrt
resultGUI = matRad_fluenceOptimization(dij,cst,pln);

%% start gui for visualization of result
matRadGUI
```

For educational purposes, matRad is also available as a standalone application.
