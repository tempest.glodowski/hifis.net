---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: EnPT

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2021-11-18

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: enpt/logo.svg

# One or two sentences describing the software
excerpt: The Environmental Mapping and Analysis Program (EnMAP) is a German hyperspectral satellite mission that aims at monitoring and characterising Earth’s environment on a global scale. EnMAP measures and models key dynamic processes of Earth’s ecosystems by extracting geochemical, biochemical and biophysical parameters that provide information on the status and evolution of various terrestrial and aquatic ecosystems.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: EnPT - EnMAP Processing Tool

# Add at least one keyword
keywords:
    - Remote sensing
    - Hyperspectral
    - Satellite data
    - EnMAP

# The Helmholtz research field
hgf_research_field: Earth & Environment

# At least one responsible centre
hgf_centers:
    - Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
    - Alfred Wegener Institute for Polar and Marine Research (AWI)
    - German Aerospace Center (DLR)

# Other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - Remote sensing

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: daniel.scheffler@gfz-potsdam.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: gitlab
      link_as: https://git.gfz-potsdam.de/EnMAP/GFZ_Tools_EnMAP_BOX/EnPT
    - type: github
      link_as: https://github.com/GFZ/enpt
    - type: webpage
      link_as: https://helmholtz.software/software/enpt

# The software license (optional)
license: GPL-3.0-or-later

# Is the software pricey or free? (optional)
costs:

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    -

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    -

# List of programming languages (optional)
programming_languages:
    - Python

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.3742344

# Funding of the software (optional)
funding:
    - shortname: German Federal Ministry of Economic Affairs and Energy (50 EE 0850)
---

# EnPT - EnMAP Processing Tool

The Environmental Mapping and Analysis Program ([EnMAP](https://www.enmap.org/)) is a German hyperspectral satellite mission that aims at monitoring and characterising the Earth’s environment on a global scale. The EnPT Python package is an automated pre-processing pipeline for the new EnMAP hyperspectral satellite data. It provides free and open-source features to transform EnMAP Level-1B data to Level-2A. The package has been developed at the German Research Centre for Geosciences Potsdam (GFZ) as an alternative to the processing chain of the EnMAP Ground Segment.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/enpt/screenshot.png" alt="The graphical user interface of EnPT">
<span>The screenshot shows an exemplary EnMAP level 2A output image of EnPT (Arcachon test dataset), visualized within the EnMAP-Box.</span>
</div>

This software was developed within the context of the EnMAP project supported by the DLR Space Administration with funds of the German Federal Ministry of Economic Affairs and Energy and contributions from DLR, GFZ and OHB System AG.

EnPT is especially designed for the needs of the hyperspectral data community who want to have extensive control about the processes and algorithms included in the EnMAP level 2 processing chain. The package is freely available from the [Python package index](https://pypi.org/project/enpt/) or [conda-forge](https://anaconda.org/conda-forge/enpt).
