---
layout: spotlight

# The name of the software
name: MeshIt

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: meshit/meshit_logo.png

# One or two sentences describing the software
excerpt: The tool MeshIT generates quality tetrahedral meshes based on structural geological information. It has been developed at the GFZ Potsdam and some extensions were later added by PERFACCT. All procedures are fully automatized and require at least scattered data points as input.
# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: meshit_Pohang.png

# Title at the top, inside the title-content-container
title: MeshIt - Three dimensional volumetric meshing of complex faulted reservoirs

date_added: 2022-06-30

# Add at least one keyword
keywords:
    - Delaunay triangulation
    - Delaunay tetrahedralization
    - 3D meshing
    - Faulted and fractured reservoirs

# The Helmholtz research field
hgf_research_field: Earth & Environment

# At least one responsible centre
hgf_centers:
    - Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences

# List of other contributing organisations (optional)
contributing_organisations:
    - name: PERFACCT GmbH
      link_as: https://www.perfacct.eu/en/home/

# List of scientific communities
scientific_community:
    - Thermal-Hydraulic-Mechanical-Chemical reservoir modelling

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: guido.bloecher@gfz-potsdam.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://helmholtz.software/software/meshit
    - type: github
      link_as: https://github.com/bloech/MeshIt
    - type: gitlab
      link_as: https://git.gfz-potsdam.de/bloech/MeshIt

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: AGPL-3.0-or-later

# Is the software pricey or free? (optional)
costs: Free and Open

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Modelling

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.4327281

# Funding of the software (optional)
funding:
    - shortname: Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
      link_as: https://www.gfz-potsdam.de
    - shortname: Chair of Hydrogeology, Technical University of Munich
      link_as: https://www.tum.de

---

# MeshIt in a nutshell

MeshIt is a software for generating high quality, boundary conforming
Delaunay tetrahedral meshes for Finite Element (FE) or Finite Volume
(FV) Thermo-Hydraulic-Mechanical-Chemical (THMC) dynamic simulations
of complex faulted and fractured reservoir applications. The main goal
of MeshIt is to provide an open source, time efficient, robust and
“easy to handle” software tool to bridge the gap between geological
and dynamic forward models of flow and transport processes in
reservoir domains comprising fault zones, wells and induced and/or
natural fractures.

<div class="spotlights-text-image">
<img alt="Example of a complex structured reservoir from the Pohang reservoir site in South Korea." src="{{ site.directory.images | relative_url}}spotlights/meshit/meshit_Pohang.png">
<span>Example of a complex structured reservoir from the Pohang reservoir site in South Korea.</span>
</div>

MeshIt comes with an integrated graphical user interface (GUI) that
facilitates the user during the different stages of the meshing
workflows. All instructions from the user are provided either via the
GUI or in text files, which can be input to the software. Beside third
party C libraries, the complete code has been written in the object
oriented C++ programming language. In order to provide cross-platform
support (Windows, Mac OS X and Unix operating systems under 32 and 64
bit) the open source project [Qt](https://www.qt.io/?hsLang=en) has
been integrated in the supported library environment. The interactive
3D graphics Application Programming Interface (API) has been developed
by relying on the [OpenGL](https://www.opengl.org/) library. All
graphical processing stages have been interfaced to the open-source,
multiplatform visualization software
[Paraview](https://www.paraview.org/), the latter being based on the
Visualization ToolKit ([VTK](https://vtk.org/)). MeshIt consists of
different geometric and meshing routines, which have been combined in
an automated software framework. The core meshing libraries are
[Tetgen](http://tetgen.org/), a quality tetrahedral mesh generator
developed at the [WIAS](https://www.wias-berlin.de/) Institute in
Berlin and [Triangle](https://www.cs.cmu.edu/~quake/triangle.html), a
two-dimensional quality mesh generator. Both libraries are Delaunay
based. A nice feature of methods based on the Delaunay property is
that they guarantee high-quality meshes showing at the same time a
large flexibility when applied to generic geometries comprising
internal boundaries. Therefore, Delaunay-based meshes are particularly
suitable for the numerical solution of partial differential equations
on highly irregular domains. With the single exception of the final
constrained Delaunay tetrahedralization, all routines have been
written for parallel computation on symmetric multiprocessing computer
(SMP) architecture, as based on the Qt thread support environment via
platform independent threading classes. More information can be found
in the [relevant
publication](https://link.springer.com/article/10.1007/s12665-015-4537-x).
