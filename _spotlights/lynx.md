---
layout: spotlight

# The name of the software
name: LYNX - modelling lithosperic dynamics

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: lynx/lynx_logo.png

# One or two sentences describing the software
excerpt: LYNX (Lithosphere dYnamics Numerical toolboX) is a novel numerical simulator for modelling thermo-poromechanical coupled processes driving the deformation dynamics of the lithosphere.

date_added: 2022-06-30

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: lynx_preview.png

# Title at the top, inside the title-content-container
title: LYNX - modelling lithosperic dynamics

# Add at least one keyword
keywords:
    - Explicit visco-elasto-plastic rheology
    - semi-brittle/semi-ductile deformation of porous rocks
    - damage rheology and porosity feedback

# The Helmholtz research field
hgf_research_field: Earth & Environment

# At least one responsible centre
hgf_centers:
    - Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences

# List of other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - Computational geodynamic modelling

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: mauro.cacace@gfz-potsdam.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://helmholtz.software/software/lynx-modelling-lithosperic-dynamics
    - type: github
      link_as: https://github.com/ajacquey/lynx
    - type: gitlab
      link_as: https://git.gfz-potsdam.de/moose/lynx

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-3.0-or-later

# Is the software pricey or free? (optional)
costs: Free and Open

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Modelling

# The application type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.3355376

# Funding of the software (optional)
funding:
    - shortname: Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
      link_as: https://www.gfz-potsdam.de

---

# LYNX in a nutshell

LYNX (Lithosphere dYnamics Numerical toolboX) is a multiphysics modelling
solution developed within the group of Basin Modelling at GFZ. LYNX is
based on the flexible, object-oriented numerical framework
[MOOSE](https://mooseframework.inl.gov/), which provides a high-level
interface to state of the art nonlinear solver technology. LYNX is a
novel numerical simulator for modelling thermo-poromechanical coupled
processes driving the deformation dynamics of the lithosphere. The
formulation adopted in LYNX relies on an efficient implementation of a
thermodynamically consistent visco-elasto-plastic rheology with
anisotropic porous-visco-plastic damage feedback. The main target is
to capture the multiphysics coupling responsible for semi-brittle and
semi-ductile behaviour of porous rocks as also relevant to strain
localization and faulting processes. More information on the governing
equations, their derivation and their implementation together with a
list of synthetic and real case applications can be found in two
publications [here](http://doi.org/10.1029/2019jb018474) and
[here](http://doi.org/10.1029/2019jb018475).

<div class="spotlights-text-image">
<img alt="Modelling of the seismic cycle with a realistic elasto-visco-plastic off-fault rheology" src="{{ site.directory.images | relative_url}}spotlights/lynx/lynx_preview.png">
<span>Modelling of the seismic cycle with a realistic elasto-visco-plastic off-fault rheology.</span>
</div>

LYNX has been coded following the concept of Object-orientation thus
providing a flexible modular structure within easy to be extended
modules by the user. It also features geometric agnosticism and hybrid
parallelism with proven scalability on HPC architectures. LYNX is
based on a realistic physics-based rheological description of
lithosphere deformation dynamics based on an explicit incorporation of
the lithosphere visco-elasto-plastic rheology including nonlinear
feedback effects from the energetics of the system and its extension
to account for time-dependent brittle behavior via an overstress
(viscoplastic) formulation. It also includes a thermodynamically
consistent formulation of semi-brittle semi-ductile deformation
including brittle rock behaviour via damage mechanics and ductile
deformation via a rate-dependent viscoplastic formulation. Poro-damage
feedback is included via a dynamic porosity to simulate the full
volumetric mechanical response of the rock. In LYNX we adopt an
implicit and efficient numerical implementation of the material
consticutive behaviour within a limited amount of internal iterations
and we make use of the concept of Automatic Differentation (AD)
techniques to compute the full Jacobian contribution of the system
matrix.
