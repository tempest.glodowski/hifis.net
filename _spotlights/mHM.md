---
layout: spotlight

# Spotlight list attributes
name: mHM
date_added: 2022-02-21
preview_image: mHM/mHM_textlogo.png
excerpt: The mesoscale Hydrologic Model (mHM) is a well established hydrological model used by the scientific community. It is under active development with dozens of contributors and has an open community always interested in discussions.

# Title for individual page
title_image: mHM5_logo.png
title: The mesoscale Hydrologic Model - mHM
keywords:
    - mesoscale hydrologic model
    - multiscale parameter regionalization
    - seamless predicitions
    - mHM
hgf_research_field: Earth & Environment
hgf_centers:
    - Helmholtz Centre for Environmental Research (UFZ)
contributing_organisations:
scientific_community:
    - Hydrologic Modelling
impact_on_community:
contact: mhm-admin@ufz.de
platforms:
    - type: webpage
      link_as: https://mhm-ufz.org/
    - type: gitlab
      link_as: https://git.ufz.de/mhm/mhm
    - type: github
      link_as: https://github.com/mhm-ufz/mhm
license: GPL-3.0-or-later
costs: Free
software_type:
    - Modelling
application_type:
    - Desktop
programming_languages:
    - Fortran
doi: 10.5281/zenodo.5119952
funding:
    - shortname: UFZ
      link_as: https://www.ufz.de/
---

# mHM - mesoscale Hydrologic Model

The [mesoscale Hydrologic Model (mHM)](https://mhm-ufz.org/) developed by the Dept. Computational Hydrosystems at UFZ is a spatially explicit distributed hydrologic model.
It is implemented in the Fortran programming language and can be easily installed as software using the conda package manager.
Detailed installation guide can be found [here](https://mhm-ufz.org/guides/).
The model concept uses grid cells as a primary hydrologic unit, and accounts for the following processes: canopy interception, snow accumulation and melting, soil moisture dynamics, infiltration and surface runoff, evapotranspiration, subsurface storage and discharge generation, deep percolation and baseflow and discharge attenuation and flood routing.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/mHM/mHM5_logo.png" alt="Schematic of resolution levels, data, processes and states in mHM.">
<span>Schematic of resolution levels, data, processes and states in mHM.</span>
</div>

The model is driven by hourly or daily meteorological forcings (e.g., precipitation, temperature), and it utilizes observable basin physical characteristics (e.g., soil textural, vegetation, and geological properties) to infer the spatial variability of the required parameters.

The main feature of mHM is the approach to estimate parameters at the target resolution based on high resolution physiographic land surface descriptors (e.g., DEM, slope, aspect, root depth based on land cover class or plant functional types, leaf area index, soil texture, geological formation type).
The technique was proposed in [Samaniego et al., WRR 2010](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2008WR007327) and is called [multiscale parameter regionalization](https://gmd.copernicus.org/articles/15/859/2022/) (MPR).
The MPR technique is crucial to reach flux-matching across scales and to derive seamless parameter fields ([Samaniego et al., HESS 2017](https://www.hydrol-earth-syst-sci.net/21/4323/2017/)).
