---
layout: spotlight

# Spotlight list attributes
name: RCE
date_added: 2021-11-18
preview_image: rce/rce-logo.png
excerpt: RCE is a distributed integration environment for scientists and engineers to analyze, optimize, and design complex systems like aircraft, ships, or satellites.

# Title for individual page
title: RCE
keywords:
    - Multidisciplinary Analysis
    - Tool Integration
    - Workflow Execution
    - Collaboration
    - Distributed Execution
hgf_research_field: Aeronautics, Space and Transport
hgf_centers:
    - German Aerospace Center (DLR)
contributing_organisations:
scientific_community:
    - Engineering
impact_on_community:
contact: rce@dlr.de
platforms:
    - type: webpage
      link_as: https://rcenvironment.de
    - type: github
      link_as: https://github.com/rcenvironment
    - type: twitter
      link_as: https://twitter.com/RCEnvironment
license: EPL-1.0
costs: Free
software_type:
    - Distributed Integration Environment
application_type:
    - Desktop
    - Server
programming_languages:
    - Java
doi: 10.5281/zenodo.3691674
funding:
    - shortname: Several DLR, BMBF, BMWi, and EU projects
---

# RCE in a nutshell

Handling complex systems, such as aircraft, ships, or satellites, requires many experts and several tools for analysis, design, optimization, and simulation. RCE is a scientific integration framework that facilitates the design and distributed execution of multidisciplinary toolchains and workflows. It is especially suited for multidisciplinary collaboration, since tools for analyzing complex systems can be shared between team members. RCE is extensible and supports different scientific applications with a wide range of requirements.


<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/rce/rce-ui.jpg" alt="The RCE User Interface">
<span>The RCE User Interface.</span>
</div>
