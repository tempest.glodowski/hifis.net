---
layout: spotlight

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: TRIDEC Cloud
date_added: 2022-06-13

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: trideccloud/TRIDEC-Cloud-Logo-250x125.png

# One or two sentences describing the software
excerpt: TRIDEC Cloud merges several complementary external and in-house cloud-based services into one platform for automated background computation, for web-mapping of hazard specific geospatial data, and for serving relevant functionality to handle, share, and communicate threat specific information in a collaborative and distributed environment.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: spotlight_tridec.jpg

# Title at the top, inside the title-content-container
title: TRIDEC Cloud & GeoPeril

# Add at least one keyword
keywords:
    - tsunami
    - early warning
    - simulation system
    - scenario computation
    - impact analysis
    - GPU
    - SaaS
    - virtual scenario training

# The Helmholtz research field
hgf_research_field: Earth & Environment

# At least one responsible centre
hgf_centers:
    - Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences

# List of other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - Simulation and Modelling

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: tridec-cloud-support@gfz-potsdam.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://helmholtz.software/software/tridec-cloud
    - type: github
      link_as: https://github.com/locationtech/geoperil
    - type: webpage
      link_as: https://trideccloud.gfz-potsdam.de

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: Apache-2.0

# Is the software pricey or free? (optional)
costs: Access available on request

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    -

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Web

# List of programming languages (optional)
programming_languages:
    - Python, JavaScript/TypeScript

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi:

# Funding of the software (optional)
funding:
    - shortname: GFZ in-house
    - shortname: Helmholtz Enterprise
---

# TRIDEC Cloud & GeoPeril

In times of cloud computing and ubiquitous computing the use of state of the art technology concepts and paradigms have to be considered even for early warning and fast response systems. Based on the experiences and the knowledge gained in three research projects new technologies are exploited to implement a cloudbased and web-based platform to open up new prospects for tsunami early warning and mitigation systems (TEWS).

**TRIDEC Cloud** merges several complementary external and in-house cloud-based services into one platform for automated background computation with graphics processing units (GPU), for web-mapping of hazard specific geospatial data, and for serving relevant functionality to handle, share, and communicate threat specific information in a collaborative and distributed environment.

**GeoPeril** is the framework of TRIDEC Cloud. This implementation of an early warning system for tsunamis includes, among other things, the harvesting of earthquake events from catalogs or APIs, the automatic execution of simulations for events with given thresholds, remote execution of simulations with [EasyWave](https://git.gfz-potsdam.de/id2/geoperil/easyWave) or HySEA and the modifying of earthquake parameters or creating fictional earthquakes to simulate a scenario.

## User group

The platform is meant for researchers and authorities around the world to make use of cloud-based GPU computation, to analyze tsunamigenic events, and react upon the computed situation picture with a web-based GUI in a web browser at remote sites.
Features

- Global coverage, and applicable for any region in the world
- Workflow implemented following operators’ or users’ needs, even in stressful situation
- Integrated threat management & tracing of information, activities, and sequences related to an event
- Integration of different observational networks and services
- Integration of different computation engines, i.e. simulation software and hardware (CPU, GPU)
- Provision of different input data for computations, e.g. different resolutions
- Generation of pre-defined, internationally agreed messages with dynamic content provided by observations and computations
- Dissemination via E-mail, Fax, FTP/GTS, SMS, Cloud Messages, Shared Maps

<div class="spotlights-text-image">
    <img width="500" alt="Screenshot of the TRIDEC Cloud website" src="{{ site.directory.images | relative_url}}spotlights/trideccloud/TRIDEC-1.jpg">
    <span>Using TRIDEC Cloud to monitor possible tsunamis (early warning).</span>
</div>

In monitoring mode a list displays earthquake events with information on magnitude, location, time, and depth, as well as dip, strike, and rake. An earthquake classification is provided based on a simplified decision matrix and a tsunami prediction is available for critically classified earthquakes based on an attached simulation either computed automatically or manually in the background. Information on earthquakes is complemented by historic and real-time sea level data. The map displays the position of the sea level stations and small diagrams below the map display the measurements for each sea level station.

<div class="spotlights-text-image">
    <img width="500" alt="Screenshot about sending information about a tsunami with TRIDEC Cloud" src="{{ site.directory.images | relative_url}}spotlights/trideccloud/TRIDEC-2.jpg">
    <span>Sending information about a tsunami with TRIDEC Cloud.</span>
</div>

Interactive diagrams allow a detailed analysis and picking of values that are available for automated integration in warning messages. The generation of warning messages is based on internationally agreed message structures and includes static and dynamic information based on earthquake information, instant computations of tsunami simulations, and actual measurements. Generated messages are served for review, modification, and addressing in one simple form for dissemination.

The exercise and training mode enables training and exercises with virtual scenarios. This mode disconnects real world systems and connects with a virtual environment that receives virtual earthquake information and virtual sea level data re-played by a scenario player. The exercise and training mode has been used in an opportunity provided by the international NEAMWave14 tsunami exercise to test the TRIDEC Cloud as a collaborative activity based on previous partnerships and commitments at the European scale (Hammitzsch et al. 2014). For the NEAMWave21 exercise the modelling of the KOERI-NOA scenario had been done by KOERI through the easyWave tsunami modelling tool embedded in the TRIDEC Cloud. During the conduct of the NEAMWave21 exercise, TRIDEC Cloud was also utilized by KOERI for the real-time simulation of the sea-level readings.

<div class="spotlights-text-image">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/CaRXYR4PE8s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
