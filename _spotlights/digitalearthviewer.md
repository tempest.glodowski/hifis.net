---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: Digital Earth Viewer

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-04-05

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: digitalearthviewer/dev_logo.png

# One or two sentences describing the software
excerpt: The Digital Earth Viewer is a tool for the visualisation and exploration of geospatial data in true 3D over time. It runs on Windows, MacOS and Linux and only requires a modern webbrowser to use. Common file formats such as CSV, netCDF, GeoTIFF and many more are natively supported.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: Digital Earth Viewer

# Add at least one keyword
keywords:
    - Data Visualization
    - Environmental Data
    - GIS

# The Helmholtz research field
hgf_research_field: Earth & Environment

# At least one responsible centre
hgf_centers:
    - Helmholtz Centre For Ocean Research Kiel (GEOMAR)

# List of other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - Oceanography
    - Meteorology
    - Climate Research

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: digitalearthviewer@geomar.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://digitalearth-hgf.de/results/software-components/data-visualisation/
    - type: gitlab
      link_as: https://git.geomar.de/digital-earth/digital-earth-viewer

# The software license (optional)
license: EUPL-1.2

# Is the software pricey or free? (optional)
costs: Free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    -

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    -

# List of programming languages (optional)
programming_languages:
    - Rust
    - Typescript

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi:

# Funding of the software (optional)
funding:
    - shortname: Digital Earth (Helmholtz)
---

# Digital Earth Viewer

The Digital Earth Viewer is a web-based tool for the visualization and exploration of geospatial timeseries data. It can load data from files such as CSV, netCDF, GeoTIFF, shapefiles or SQLite databases, but also from online sources like OpenStreetMap or WMS servers. It provides a Google-Earth-style 3D globe visualization in which the user can simultaneosly view data from different sources. This contextualization can help with aiding understanding complex datasets. Users can scroll through timeseries, navigate on the globe and query or graph selected data. As the viewer is provided as a single 25MB executable with no dependencies besides a modern web browser, it can be quickly deployed and used both in the field as well as in the lab or even in hosted contexts. Scenes can be viewed either on a regular monitor, in anaglyph 3D or on projection domes. As data is extracted, reprojected and pyramidized at runtime, no lengthy preprocessing is needed. The application is licensed under the EUPL v1.2 and thus can be freely used and extended.

Several online showcases are hosted:

- [The GLODAP project](https://digitalearthviewer-glodap.geomar.de/) shows measurements over terrain from GEBCO.net. It also contains early gridded versions of the GLODAP data as part of a quality control study.
- [The Mining Impact project](https://digitalearthviewer-plume.geomar.de/) shows a simulated dust cloud over a sonar scan of terrain. This data is fused with sensor measurements from a real dust cloud with similar parameters to the simulation.
- [The Digital Earth Showcase B: Methane](https://digitalearthviewer-methane.geomar.de/) projects data of methane outflow and an animated wind-indicator. This includes location of methane-releasing wells.

## Watch video

A short introduction to the viewer was given at EUROVIS2021:

<a href="https://www.youtube.com/watch?t=123&v=iIRBpXOWuPg&feature=youtu.be" target="_blank" alt="Digital Earth Viewer: a 4D visualisation platform for geoscience datasets">Digital Earth Viewer: a 4D visualisation platform for geoscience datasets <i class="fas fa-external-link-alt"></i></a> (*link to Youtube*)

## Examples

<div class="spotlights-text-image" style="display: block">
<img src="{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/glodap.png"
    onclick="window.open('{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/glodap.png', '_blank')"
    width="16.5%" alt="dev images missing" style="cursor:pointer;">
<img src="{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/glodap-ctds-norway.png"
    onclick="window.open('{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/glodap-ctds-norway.png', '_blank')"
    width="16.5%" alt="dev images missing" style="cursor:pointer;">
<img src="{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/Pointcloud.png"
    onclick="window.open('{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/Pointcloud.png', '_blank')"
    width="16.5%" alt="dev images missing" style="cursor:pointer;">
<img src="{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/StereoAnaglyph.png"
    onclick="window.open('{{ site.directory.images | relative_url}}spotlights/digitalearthviewer/StereoAnaglyph.png', '_blank')"
    width="16.5%" alt="dev images missing" style="cursor:pointer;">
<br>
<span><center>Examples of the Digital Earth Viewer.</center></span>
</div>

<center>
<video src="{{ site.directory.videos | relative_url }}spotlights/digitalearthviewer/dev_quick.mp4" width="40%" controls>
</center>
