---
layout: spotlight

# The name of the software
name: Golem - a MOOSE based application

# Small preview image shown at the spotlights list page.
preview_image: golem/golem_preview.png

excerpt: Golem is a modelling platform for thermal-hydraulic-mechanical and non-reactive chemical processes in fractured and faulted porous media.

# The path is relative to /assets/img/jumbotrons/
title_image: golem_title.png

title: Golem - a MOOSE based application

date_added: 2022-06-30

keywords:
    - Multiphysics of porous fractured rocks
    - THMC coupled processes
    - reservoir behaviour and induced seismicity

# The Helmholtz research field
hgf_research_field: Earth & Environment

# At least one responsible centre
hgf_centers:
    - Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences

# List of other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - computational hydrologeology
    - computational rock physics and mechanics

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: "mauro.cacace@gfz-potsdam.de"

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://helmholtz.software/software/golem-a-moose-based-application
    - type: gitub
      link_as: https://github.com/ajacquey/golem
    - type: gitlab
      link_as: https://git.gfz-potsdam.de/moose/golem

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-3.0-or-later

# Is the software pricey or free? (optional)
costs: Free and Open

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Modelling

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.5281/zenodo.999401

# Funding of the software (optional)
funding:
    - shortname: Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
      link_as: https://www.gfz-potsdam.de
---

# Golem in a nutshell

GOLEM is a modelling platform for thermal-hydraulic-mechanical and
non-reactive chemical processes in fractured and faulted porous media.
GOLEM makes use of the flexible, object-oriented numerical framework
[MOOSE](https://mooseframework.inl.gov/), which provides a high-level
interface to state of the art nonlinear solver technology. In GOLEM,
the governing equations of groundwater flow, heat and mass transport,
and rock deformation are solved in a weak sense (by classical
Newton–Raphson or by free Jacobian inexact Newton-Krylow schemes) on
an underlying unstructured mesh. Non-linear feedback among the active
processes are enforced by considering evolving fluid and rock
properties depending on the thermo-hydro-mechanical state of the
system and the local structure, i.e. degree of connectivity, of the
fracture system. More information on the governing equations, their
derivation and implementation together with a list of synthetic and
real case applications can be found in [Cacace and Jacquey](
https://doi.org/10.5194/se-8-921-2017) - also available from a
dedicated github repository.

<div class="spotlights-text-image">
<img alt="Proof of concept for EGS analysis" src="{{ site.directory.images | relative_url}}spotlights/golem/golem_title.png">
<span>Proof of concept for EGS analysis - sustainability of induced fracture and exploitability of geothermal reservoirs.</span>
</div>

Golem is base on the concept of Object-orientation and provides a
flexible modular structure within easy to be extended modules by the
user. It also features geometric agnosticism and hybrid parallelism,
with proved scalability on HPC architectures. The physics targeted in
Golem includes saturated single phase fluid flow in fractured Porous
Media (FPM), heat transfer (conduction and advection with or without
internal buoyant flow) in FPM, non reactive chemical transport
(diffusion and dispersion), coupled to rock mechanics for the porous
matrix (linear and non-linear elastic, plastic, visco-elastic,
isotropic and anisotropic damage rheology) and fracture mechanics
(elasto-plastic, frictional). Ongoing activities are towards
integration of reactive chemistry via a dedicated interface to
existing open source software.
