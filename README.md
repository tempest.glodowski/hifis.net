# HIFIS Software web page

The HIFIS Software web page will be the home for gathering information about
the activities and services offered by the HIFIS Software team. This website
is built using Jekyll.

## Build the project locally

### Manually

1. Clone the project locally
   ```bash
   git clone --recursive git@codebase.helmholtz.cloud:hifis/overall/hifis.net.git
   cd hifis.net
   ```
2. [Install](https://jekyllrb.com/docs/installation/) Jekyll.
3. Install dependencies.
   ```bash
   bundle install
   ```
4. Install submodules (currently `bootstrap`, `Font-Awesome`, `jquery`, `MathJax`).
   ``` bash
   git submodule update --init --recursive
   ```
5. Optionally, on _Linux_ systems install `FFmpeg` to be used to scale
   Jumbotron images.
   On systems with `Apt`
   ```bash
   apt install ffmpeg
   ```
   _or_ on systems with `Yum`
   ```bash
   yum install ffmpeg
   ```
6. Optionally, on _Linux_ systems create scaled variants of Jumbotron images to
   be used on different devices.
   ```bash
   bash scripts/create_jumbotrons.sh assets/img/jumbotrons/
   ```
   _Note: Be aware that missing scaled variants of Jumbotron images may cause
   issues in your local installation when rendering the page in the next step._
7. Build and preview the web page.
   ```bash
   bundle exec jekyll serve --future
   ```
8. Add content.

Execute the commands above from the root of your repository.

More information is available in Jekyll's
[documentation](https://jekyllrb.com/docs/)

### Using the Makefile

If you are using Linux, you may also use the provided `Makefile`. After having
cloned the repository (Step 1 above), make sure to have Jekyll and ffmpeg installed.
Now you can start the local development server by running

```shell
make all
```

Individual steps can also be called:

```shell
make install     # install dependencies and update submodule
make jumbotrons  # build the jumbotrons using ffmpeg
make serve       # start the dev server
make clean       # Remove all generated/downloaded files
```

## Fetch the latest builds from gitlab

The latest master branch (production) build artifacts can be downloaded using
* https://codebase.helmholtz.cloud/hifis/hifis.net/-/jobs/artifacts/master/download?job=build:production
