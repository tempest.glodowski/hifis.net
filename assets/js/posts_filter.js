var selectedTags = [];

var posts = document.querySelectorAll(".list-entry");
var searchInput = document.getElementById("postsFilterInput");

function filterPosts() {
    var searchQuery = searchInput.value.toLowerCase();

    posts.forEach(function (post) {
        // fetch title
        var postTitle = post.querySelector(".preview h1 a").innerText.toLowerCase();
        // fetch content
        var excerptContent = post.querySelector(".excerpt-content");
        var noExcerptContent = post.querySelector(".no-excerpt-content");
        var postContent = excerptContent ? excerptContent.innerText.toLowerCase() : noExcerptContent.innerText.toLowerCase();
        // fetch tags
        var postTags = [];
        var tagElements = post.querySelectorAll(".badge-pill");
        tagElements.forEach(function(tag) {
            postTags.push(tag.innerText.toLowerCase());
        });

        // define individual matching criteria
        var titleMatch = postTitle.includes(searchQuery);
        var contentMatch = postContent.includes(searchQuery);
        var tagMatch = postTags.some(function(tag) {
            return tag.includes(searchQuery);
        });
        var selectedTagMatch = selectedTags.length !== 0 && selectedTags.every(function(tag) {
            return postTags.includes(tag);
        });

        // define global matching criteria depending on combined use of search bar and tag selection
        var matchesSearch;

        if (selectedTags.length > 0) {
            matchesSearch = (titleMatch || contentMatch || tagMatch) && selectedTagMatch;
        } else {
            matchesSearch = titleMatch || contentMatch || tagMatch || selectedTagMatch;
        }

        if (matchesSearch) {
            post.style.display = "flex";
        } else {
            post.style.display = "none";
        }
    });

    updateTagStyles();
}

// toggle class for selected tags
function updateTagStyles() {
    var tagElements = document.querySelectorAll(".badge-pill");

    tagElements.forEach(function(tag) {
        var clickedTag = tag.innerText.toLowerCase();
        var isTagSelected = selectedTags.includes(clickedTag);
        tag.classList.toggle("selected", isTagSelected);
    });
}

// add the clickable-badge class to badges (in js because specific to this page)
document.addEventListener('DOMContentLoaded', function () {
    var tagElements = document.querySelectorAll(".badge-pill");

    tagElements.forEach(function (tag) {
        tag.classList.add('clickable-badge');
    });
});

// Event listener for clicks on tags
var tagElements = document.querySelectorAll(".badge-pill");
tagElements.forEach(function(tag) {
    tag.addEventListener("click", function() {
        // Get clicked tag text
        var clickedTag = tag.innerText.toLowerCase();
        var index = selectedTags.indexOf(clickedTag);

        if (index === -1) {
            // tag is not already selected: add tag to selectedTags
            selectedTags.push(clickedTag);
        } else {
            // tag is already selected: remove tag from selectedTags
            selectedTags.splice(index, 1);
        }

        filterPosts();
    });
});
